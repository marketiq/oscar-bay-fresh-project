import React, { Component } from "react";
import { Provider, connect } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { RootRouteContainer } from "./navigation/RootRoute";
import { Root } from "native-base";
import reducers from "./rootReducer";
import ReduxThunk from "redux-thunk";
import { StatusBar, View, Platform } from "react-native";
import axios from "axios";
import { getURL, BASE_URL } from "./constants/constants";
import { LoadingModal } from "./components/common";
import { setCustomTextInput, setCustomText } from "react-native-global-props";

const customTextInputProps = {
  style: {
    fontFamily:
      Platform.OS === "ios"
        ? "HelveticaNeue"
        : "Roboto:100,300,400,500,500i,700"
  }
};
const customTextProps = {
  style: {
    fontSize: 16,
    fontFamily: Platform.OS === "ios" ? "HelveticaNeue" : "Roboto",
    color: "black"
  }
};

setCustomTextInput(customTextInputProps);
setCustomText(customTextProps);

//For debug purposes
GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

//Config
axios.defaults.baseURL = BASE_URL;
const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

class App extends Component {
  constructor() {
    super();
    StatusBar.setBarStyle("light-content", true);
  }
  render() {
    return (
      <Root>
        <Provider store={store}>
          <View style={{ flex: 1 }}>
            <LoadingModal />
            <RootRouteContainer />
          </View>
        </Provider>
      </Root>
    );
  }
}

export default App;
