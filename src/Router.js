
import React from 'react';
import { RootRouteParams } from './navigation/RootRoute';
import { NavigationActions } from 'react-navigation';

import { AnalyticsTabs } from './navigation/MainRoute';
	
const initialState = RootRouteParams.router.getStateForAction(RootRouteParams.router.getActionForPathAndParams('Splash'))

// const initialState = RootRouteParams.router.getStateForAction(
//     NavigationActions.init()
// );

export const NavReducer = (state = initialState, action) => {
	const newState = RootRouteParams.router.getStateForAction(action, state)

	// AnalyticsTabs.router.getStateForAction(AnalyticsTabs.router.getActionForPathAndParams('Overview'))

  	return newState || state;
};
