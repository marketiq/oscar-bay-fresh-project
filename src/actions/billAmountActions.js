import { BILL_AMOUNT } from './actionTypes';

export const addTotal = (amount)=>{
    return (dispatch,getState) =>{
        dispatch({  
            type : BILL_AMOUNT.ADD_PAYMENT_FIELD,
            amount
        })
    }
}