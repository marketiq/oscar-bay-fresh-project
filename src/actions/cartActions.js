import {CART} from './actionTypes';

export const onCartAddProduct = (product) => {
    return (dispatch,getState) => {
        let cartProduct = getState().cart_reducer.find(item =>{
            return item.product_id === product.product_id;
        });
        if(cartProduct){
            dispatch({
                type:CART.ON_INC_COUNT,
                product
            })
        }else {
            dispatch({
                type:CART.ON_ADD_PRODUCT,
                product,
            })
        }
    }
}

export const onCartRemoveProduct=(id, hardDelete)=>{
    return (dispatch,getState)=>{
        let cartProduct = getState().cart_reducer.find(item => {
            return item.product_id === id;
        });
        if(cartProduct.qty > 1 && !hardDelete){
            dispatch({
                type:CART.ON_DEC_COUNT,
                id,
            })
        }else {
            dispatch({
                type:CART.ON_REMOVE_PRODUCT,
                id,
            })

        }
    }
}

export const emptyCart = () => {
    return (dispatch)=>{
        dispatch({
            type:CART.ON_EMPTY_CART,
        })
    }
}