import { CUSTOMERS } from "./actionTypes";
import initalState from "../reducers/initialState";
import axios from "axios";
import { getBaseUrl } from "../constants/constants";
import { NavigationActions } from "react-navigation";

export const createCustomer = data => {
  return (dispatch, getState) => {
    getBaseUrl(BASE_URL => {
        axios
          .post(`${BASE_URL}api/pos/customers/`, {
            city: false,
            create_date: "2018-02-08 13:28:58",
            display_name: data.name,
            email: "admin@coffeelucio.pk",
            is_company: false,
            mobile: false,
            street: false,
            vat: false,
            zip: false,
            name: data.name,
            phone: data.phone
          })
          .then(response => {
            dispatch({
              type: CUSTOMERS.CREATE_CUSTOMER,
              data
            });
          })
          .catch(function(error) {});
    });
  };
};

export const customersOnLoad = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      getBaseUrl(BASE_URL => {
          axios
            .get(
              `${BASE_URL}api/pos/customers/`
              // { 'Authorization': 'Token {token}' }
            )
            .then(response => {
              if (response.status === 200) {
                dispatch({
                  type: CUSTOMERS.SHOW_CUSTOMERS,
                  payload: response.data
                });

                resolve(response);
              } else {
                reject(response);
              }
            })
            .catch(err => {
              reject(err);
            });
      });
    });
  };
};
export const updateCustomer = data => {
  return dispatch => {
    getBaseUrl(BASE_URL => {
        axios
          .put(`${BASE_URL}api/pos/customers/${data.id}/`, {
            name: data.name,
            phone: data.phone,
            vehicle_number:data.vehicle_number
          })
          .then(response => {
            dispatch({
              type: CUSTOMERS.UPDATE_CUSTOMER,
              data
            });
          })
          .catch(function(error) {
          });
    });
  };
};

export const searchCustomer = text => {
  return dispatch => {
    dispatch({
      type: CUSTOMERS.FILTER_CUSTOMERS,
      text
    });
  };
};
