import { ORDERS } from "./actionTypes";
import initalState from "../reducers/initialState";
import { getBaseUrl } from "../constants/constants";
import axios from "axios";

export const ordersOnLoad = () => {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      getBaseUrl((BASE_URL) => {
        axios
          .get(`${BASE_URL}api/pos/manage_orders/`)
          .then(response => {
            if (response.status === 200) {
              dispatch({
                type: ORDERS.GET_ALL_ORDERS,
                data: response.data
              });
              resolve(response);
            } else {
              reject(response);
            }
          })
          .catch(function(error) {
            reject(error);
          });
      });
    });
  };
};

export const submitOrder = data => {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      getBaseUrl((BASE_URL)=>{
        let params = data;
        axios
          .post(`${BASE_URL}api/pos/manage_orders/`, data)
          .then(response => {
            if (response.status === 201) {
              resolve(response);
            } else {
              reject(response);
            }
          })
          .catch(function(error) {
            reject(error);
          });
      })
    });
  };
};

export const ordersRecord = val => {
  return dispatch => {
    dispatch({
      type: ORDERS.ALL_ORDERS,
      payload: val
    });
  };
};
