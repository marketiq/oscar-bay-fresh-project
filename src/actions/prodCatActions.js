import axios from 'axios';
import {getBaseUrl} from '../constants/constants'
import {CATEGORIES} from './actionTypes';

export const onLoadCategories =()=>{
    return (dispatch,getState) =>{
        return new Promise ((resolve,reject)=>{
            getBaseUrl((BASE_URL)=>{
                axios.get(`${BASE_URL}api/pos/categories/`).then(res =>{
                    if(res.status===200){
                        dispatch({
                            type:CATEGORIES.GET_CATEGORIES,
                            data: res.data,
                        })
                        resolve(res);
                    }else {
                        reject(res);
                    }
                }).catch(err =>{
                    reject(err)
                })
            })
        })
    }
}