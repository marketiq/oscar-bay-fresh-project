import { PRODUCTS } from "./actionTypes";
import initalState from "../reducers/initialState";
import { getBaseUrl } from "../constants/constants";
import axios from "axios";

export const productsOnLoad = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      getBaseUrl((BASE_URL) => {
        axios
          .get(`${BASE_URL}api/pos/products/`)
          .then(response => {
            if (response.status === 200) {
              dispatch({
                type: PRODUCTS.SHOW_PRODUCTS,
                payload: response.data
              });

              resolve(response);
            } else {
              reject(response);
            }
          })
          .catch(err => {
            reject(err);
          });
      });
    });
  };
};

export const searchProduct = inputText => {
  return dispatch => {
    const tempProducts = initalState.products.filter(product => {
      return product.title.toUpperCase().indexOf(inputText.toUpperCase()) > -1;
    });
    dispatch({
      type: PRODUCTS.SHOW_FILTER_PRODUCTS,
      payload: tempProducts
    });
  };
};
