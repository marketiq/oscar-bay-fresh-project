import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  Text,
  Image,
  Platform,
  TouchableOpacity,
  KeyboardAvoidingView,
  AsyncStorage,
  Picker,
  Keyboard,
  Dimensions,
  Switch,
  StatusBar
} from "react-native";
import { CheckBox } from "react-native-elements";
import { Input, ImageBackgroundContainer } from "../components/common";
import { AuthActions, AuthSelectors } from "../modules/auth";
import { Button } from "react-native-elements";
import { WHITE_COLOR ,testBaseUrl,multiGet,testDBUrl} from "../constants/constants";

const { authOnChange, authOnLogin } = AuthActions;
const { getSelectorAuth } = AuthSelectors;

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailFocus: false,
      passwordFocus: false,
      phaseTwo: false
    };
    // this.setFocus = this.setFocus.bind(this);
    this.onChange = this.onChange.bind(this);
    this.emptyField = this.emptyField.bind(this);
    this.setBaseUrl = this.setBaseUrl.bind(this);
  }
  componentWillMount(){
    let self =this;
    multiGet((values)=>{
      if(values[0][1] && values[1][1]){
        let base_url= values[0][1].split(':')[1].split('/')
        base_url=testDBUrl(base_url[2]);
        let url = base_url.split(':')[1].split('/')
        self.onChange(url[2],'url')
        self.onChange(values[1][1],'db')
      }
    })
  }
  setFocus(key, value) {
    this.setState({
      [key]: value
    });
  }

  onChange(text, key) {
    this.props.authOnChange(key, text);
  }

  emptyField(key) {
    this.props.authOnChange(key, "");
  }

  _getFilter(callback) {
    try {
      AsyncStorage.multiGet(["base_url", "db"])
        .then(values => {
          callback(values);
        })
        .catch(err => {
        });
    } catch (error) {
    }
  }

  async setBaseUrl() {
    this.setState({ phaseTwo: true });

    try {
      await AsyncStorage.multiSet([
        ["base_url", testBaseUrl(this.props.url)],
        ["db", this.props.db]
      ]);
    } catch (error) {}
  }

  onLogin() {
    this.setState({phaseTwo:false});
    const { email, password, url, db, authOnLogin } = this.props;
    authOnLogin({ email, password, url, db });
  }

  dismissKeyboard() {
    Keyboard.dismiss();
  }
  render() {
    let self = this;
    return (
      <ImageBackgroundContainer>
        <TouchableOpacity activeOpacity={1} onPress={this.dismissKeyboard}>
          <View style={styles.loginContainer}>
            <Image
              resizeMode="contain"
              source={require("../../src/static/images/loginlogo.png")}
            />
          </View>

          <Text style={styles.loginText}>Sign In To Oscar</Text>
          <View style={styles.erroMessageContainer}>
            {this.props.errors.map((item, index) => (
              <Text key={index} style={styles.erroMessage}>
                {item}
                {index != this.props.errors.length - 1 ? ", " : null}
              </Text>
            ))}
          </View>
    
          {!this.state.phaseTwo ? (
            <View style={styles.formContainer}>
              <Input
                emptyField={() => this.emptyField("url")}
                icon={this.props.url ? "md-close" : null}
                value={this.props.url}
                onChangeText={text => this.onChange(text, "url")}
                placeholder="Domain/IP"
              />
              <Input
                emptyField={() => this.emptyField("db")}
                icon={this.props.db ? "md-close" : null}
                value={this.props.db}
                onChangeText={text => this.onChange(text, "db")}
                placeholder="DB"
              />
              <Button
                disabled={!(this.props.url && this.props.db)}
                onPress={this.setBaseUrl}
                style={styles.button}
                color={this.props.url && this.props.db ? "#946fd8" : "black"}
                fontWeight="bold"
                backgroundColor={
                  this.props.url && this.props.db ? WHITE_COLOR : "dimgray"
                }
                title="Next"
                borderRadius={20}
                fontSize={15}
                containerViewStyle={styles.containerButton}
              />
            </View>
          ) : (
            <View style={styles.formContainer}>
              <Input
                emptyField={() => this.emptyField("email")}
                icon={this.props.email ? "md-close" : null}
                value={this.props.email}
                onChangeText={text => this.onChange(text, "email")}
                keyboardType="email-address"
                placeholder="Email"
              />
              <Input
                emptyField={() => this.emptyField("password")}
                icon={this.props.password ? "md-close" : null}
                value={this.props.password}
                onChangeText={text => this.onChange(text, "password")}
                placeholder="Password"
                secureTextEntry={true}
              />
              <Button
                onPress={this.onLogin.bind(this)}
                style={styles.button}
                color="#946fd8"
                fontWeight="bold"
                backgroundColor={WHITE_COLOR}
                title="SUBMIT"
                borderRadius={20}
                fontSize={15}
                containerViewStyle={styles.containerButton}
              />
            </View>
          )}
        </TouchableOpacity>
      </ImageBackgroundContainer>
    );
  }
}

const styles = {
  formContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  loginContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  loginText: {
    fontSize: 24,
    backgroundColor: "transparent",
    color: WHITE_COLOR,
    fontWeight: "200",
    textAlign: "center",
    paddingTop: 30,
    paddingBottom: 10
  },
  button: {
    width: "100%"
  },
  containerButton: {
    marginTop: 10,
    width: "30%",
    marginLeft: 0,
    marginRight: 0,
    ...Platform.select({
      android: {
        borderRadius: 30
      }
    })
  },
  erroMessageContainer: {
    justifyContent: "center",
    flexDirection: "row",
    height: 50,
    marginBottom: 10,
    borderRadius: 0
  },
  erroMessage: {
    color: "#F4B006",
    backgroundColor: "transparent",
    textAlign: "center"
  }
};

const mapStateToProps = state => {
  return getSelectorAuth(state);
};

export default connect(
  mapStateToProps,
  { authOnChange, authOnLogin }
)(LoginPage);
