import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Platform,
  Text,
  ScrollView,
  View,
  Image,
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableHighlight,
  KeyboardAvoidingView
} from "react-native";
import { Card } from "react-native-elements";
import {
  Container,
  Item,
  Input,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Icon
} from "native-base";
// import { APP_BACKGROUND_COLOR } from '../../constants/constants';
import { Grid, Row, Col } from "react-native-easy-grid";
// import Icon from "react-native-vector-icons/dist/FontAwesome";
import { NavigationActions } from "react-navigation";

const Touchable =
  (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;

class OrderHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchInput: ""
    };
  }
  close() {
    this.setState({ searchInput: "" });
  }
  searchOrder(event) {
    if (event) {
      this.setState({
        searchInput: event.nativeEvent.text
      });
      this.props.onChangeInput(event.nativeEvent.text);
    } else {
      this.setState({ searchInput: "" }, () => {
        this.props.onChangeInput("");
      });
    }
  }
  render() {
    return (
      <Container style={styles.mainCont} noShadow>
        <Touchable
        underlayColor="#000"
        activeOpacity={0.2}
          onPress={() => {
            this.props.dispatch(
              NavigationActions.navigate({ routeName: "Dashboard" })
            );
          }}
        >
          <View transparent style={styles.backBtn}>
            <Icon style={styles.backiconFirst} name="ios-arrow-back" />
            <Text style={styles.backiconText}>Back</Text>
          </View>
        </Touchable>
        <Body style={styles.headerBody}>
          <Title style={styles.headerBodytext}>ORDERS HISTORY</Title>
        </Body>
        <View style={styles.searchMain}>
          <Icon style={styles.searchiconStyle} name="ios-search" />
          <Input
            style={styles.srchInput}
            type="text"
            value={this.state.searchInput}
            onChange={this.searchOrder.bind(this)}
            placeholder="Search Order"
            placeholderTextColor="#b968ee"
          />
          {this.state.searchInput ? (
            <Button transparent light style={{height:'100%'}}
            onPress={() => this.searchOrder()}            
            >
              <Icon
                name="md-close"
                style={{ color: "#b968ee",fontSize:18}}
              />
            </Button>
          ) : null}
        </View>
      </Container>
    );
  }
}

const styles = {
  mainCont: {
    height: "100%",
    backgroundColor: "#fff",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  headerMain: {
    backgroundColor: "#fff",
    paddingTop: 8,
    paddingBottom: 10,
    paddingRight: 20,
    paddingLeft: 20,
    height: "100%",
    borderBottomWidth: 2,
    borderBottomColor: "#f9f9fb",
    marginLeft: 20,
    marginRight: 20
  },
  headerLeft: {
    width: "20%",
    marginLeft: "-9%"
  },
  headerBody: {
    width: "33%"
  },
  headerBodytext: {
    textAlign: "center",
    color: "#262626",
    fontSize: 22,
    fontFamily: "Roboto",
    fontWeight: "500"
  },
  headerRight: {
    width: "30%"
  },
  backiconFirst: {
    color: "#555556",
    fontSize: 22,
    marginRight: 10,
    paddingTop: 3
  },
  backiconText: {
    color: "#555556",
    marginRight: 10,
    marginLeft: 5,
    fontFamily: "Roboto",
    fontSize: 22,
    borderRadius: 3
  },
  backBtn: {
    width: 150,
    height: "100%",
    borderColor: "#bcbcbc",
    flexDirection: "row",
    paddingLeft: 20,
    alignItems: "center"
  },

  searchMain: {
    flexDirection: "row",
    alignItems:'center',  
    justifyContent:'center',
    width: "33%",
    backgroundColor: "transparent",
    borderRadius: 15,
    marginRight: 10,
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth:1,
    borderColor: "#b968ee"
  },
  srchInput: {
    color: "#b968ee",
    borderBottomWidth: 0,
    padding: 15,
    fontSize: 14
  },
  searchiconStyle: {
    color: "#b968ee",
    fontSize: 18
  }
};
export default OrderHeader;
