import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  ScrollView,
  View,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { Card } from "react-native-elements";
import {
  Container,
  Item,
  Input,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Icon,
  List,
  ListItem
} from "native-base";
// import { APP_BACKGROUND_COLOR } from '../../constants/constants';
import { Grid, Row, Col } from "react-native-easy-grid";
// import Icon from "react-native-vector-icons/dist/FontAwesome";
import { NavigationActions } from "react-navigation";

class OrdersList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    var renderColors = status => {
      switch (status.toLowerCase()) {
        case "pending":
          return {
            ...styles.PendingColor
          };

        case "delivered":
          return {
            ...styles.DeliveredColor
          };
        default:
          return {};
      }
    };
    return (
      <Container>
        <View style={styles.orderHistoryinner}>
          <View style={{...styles.headingCont,justifyContent:'flex-start'}}>
            <Text style={styles.colHeading}>Name </Text>
          </View>

          <View style={styles.headingCont}>
            <Text style={styles.colHeading}>Phone Number</Text>
          </View>

          <View style={styles.headingCont}>
            <Text style={styles.colHeading}>Vehicle Number</Text>
          </View>

          <View style={{...styles.headingCont,justifyContent:'flex-end'}}>
            <Text style={styles.colHeading}>Status</Text>
          </View>
        </View>

        <ScrollView style={{ backgroundColor: "#fff" }}>
            {this.props.listOrders.map((item, index) => {
              return (
                <View key={index} style={styles.orderHistoryinner}>
                  <View style={{...styles.contentMain,justifyContent:'flex-start'}}>
                   
                    <Text style={styles.customerNme}>{item.pos_reference}</Text>
                  </View>

                  <View style={styles.contentMain}>
                    <Text style={styles.customerNme}>{item.phoneNum}</Text>
                  </View>

                  <View style={styles.contentMain}>
                    <Text style={styles.vechilenum}>{item.vehicleNum}</Text>
                  </View>

                  <View style={{...styles.contentMain,justifyContent:'flex-end'}}>
                    <Text
                      style={{ ...styles.statusprogress }}
                      // style={renderColors(item.orderStatus)}
                    >
                      {item.orderStatus}
                    </Text>
                  </View>
                </View>
              );
            })}
        </ScrollView>
      </Container>
    );
  }
}

const styles = {
  orderHistoryinner: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderColor: "#f3f3f7",
    borderBottomWidth: 1,
    marginLeft: 20,
    marginRight: 20
  },
  headingCont:{
    display: "flex",
    flexDirection: "row",
    justifyContent:'center',
    width: "25%",
    paddingTop: 20,
    paddingBottom: 20
  },
  colHeading: {
    fontWeight: "900",
    display: "flex",
    color: "#222"
  },
  contentMain: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    width: "25%",
    paddingTop: 20,
    paddingBottom: 20
  },
  customerTag: {
    color: "#222222",
    display: "flex",
    width: "40%",
    textAlign: "right"
  },
  customerNme: {
    color: "#bcbec0",
    display: "flex",
    color: "#222"
  },
  vechileTag: {
    color: "#222222",
    width: "10%",
    textAlign: "left"
  },
  vehicleNum: {
    color: "#bcbec0",
    textAlign: "left"
  },
  statusTag: {
    color: "#222222",
    textAlign: "right"
  },
  statusprogress: {
    color: "#bcbec0",
  },
  DeliveredColor: {
    color: "#91d625"
  },
  PendingColor: {
    color: "#ffde17"
  }
};
export default OrdersList;
