import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  ScrollView,
  View,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { Card } from "react-native-elements";
import { Icon, Item, Input, Button } from "native-base";
import { Grid, Row, Col } from "react-native-easy-grid";
import OrderHeader from "./OrderHeader";
import OrdersList from "./OrdersList";
import { ordersOnLoad, ordersRecord } from "../../actions/";

class OrderHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      vehicleNum: "",
      allRecords: null,
      searchInput: ""
    };
  }
  onChangeInput(input) {
    if (input) {
      const searchedOrder = this.props.listOrders.filter(order => {
        return order.pos_reference.toUpperCase().startsWith(input.toUpperCase());
      });
      this.setState({ orders: searchedOrder });
    } else {
      this.setState({
        orders: JSON.parse(JSON.stringify(this.props.listOrders))
      });
    }
  }

  componentWillMount() {
    this.props.dispatch(ordersOnLoad());
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      orders: JSON.parse(JSON.stringify(nextProps.listOrders))
    });
  }

  render() {
    console.ignoredYellowBox = ["Remote debugger"];
    return (
      <KeyboardAvoidingView style={{ height: "100%" }} behavior="padding">
        <Grid>
          <Row size={10} style={{ minHeight: 65, maxHeight: 65 }}>
            <OrderHeader
              dispatch={this.props.dispatch}
              onChangeInput={this.onChangeInput.bind(this)}
            />
          </Row>
          <Row size={90}>
            <OrdersList listOrders={this.state.orders} />
          </Row>
        </Grid>
      </KeyboardAvoidingView>
    );
  }
}

const styles = {};
const mapStateToProps = state => {
  // return getSelectorProducts(state)
  return {
    listOrders: state.order_reducer,
    allRecords: state.orderHistoryReducer
  };
  // return getSelectorProducts(state)
};
// const mapStateToProps = state => {     return getSelectorAnalytics(state); }

export default connect(mapStateToProps)(OrderHistory);
