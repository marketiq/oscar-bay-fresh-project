import React from "react";
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  Image,
  ImageBackground,
  TouchableHighlight,
  TextInput,
  Platform,
  ScrollView,
  ActivityIndicator
} from "react-native";
import { Button, Icon, Item, Input } from "native-base";
import AutoComplete from "react-native-autocomplete";
import moment from "moment";

const Touchable =
  (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;
class AddCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchInput: "",
      customers: []
    };
    this.onChange = this.onChange.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    this.setState({ customers: nextProps.customers });
  }
  onChange(text) {
    this.setState({ searchInput: text });
    // this.props.searchCustomer(text);
    let customers = this.props.customers.filter(customer => {
      // return (customer.name.toLowerCase().indexOf(text.toLowerCase()) > -1) || (customer.phone.indexOf(text) > -1);
      return (
        customer.name.toUpperCase().substring(0, text.length) ===
        text.toUpperCase()
      );
      // return customer.name.toLowerCase().indexOf(text.toLowerCase()) > -1;
    });
    this.setState({ customers });
  }
  render() {
    let { propStyles } = this.props;
    let { customers } = this.state;
    return (
      <View>
        <View style={propStyles.modalHeader}>
          <Touchable onPress={() => this.props.onHideModal("addCustomer")}>
            <View style={propStyles.closeButton}>
              <Icon style={propStyles.closeButtonText} name="md-close" />
            </View>
          </Touchable>
          <Text style={propStyles.headerText}>Add Customer to Sale</Text>
        </View>

        <View>
          <View style={styles.cutomerViewmain}>
            <Item style={styles.productSearch}>
              <Icon style={styles.productSearchIcon} name="ios-search" />
              <Input
                placeholder="Search Customer"
                type="text"
                onChangeText={text => this.onChange(text)}
                value={this.state.searchInput}
              />
              {this.state.searchInput ? (
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      searchInput: "",
                      customers: this.props.customers
                    })
                  }
                >
                  <Icon style={styles.productCloseIcon} name="md-close" />
                </TouchableOpacity>
              ) : null}
            </Item>
          </View>
          <Button
            transparent
            style={styles.createNewbtn}
            onPress={() => {
              this.props.changeView("CREATE");
            }}
          >
            <Text style={styles.createbtn}>Create New Customer</Text>
          </Button>

          {!this.props.loader ? (
            this.props.errors ? (
              <View style={[styles.container]}>
                <Text>{this.props.errors}</Text>
              </View>
            ) : (
              <ScrollView
                style={{ height: "75%" }}
                keyboardShouldPersistTaps="always"
              >
                <View style={styles.recentlyCreated}>
                  <Text>Recently Created</Text>
                </View>
                {customers.map((customer, index) => {
                  let time = moment(customer.create_date, "YYYYMMDD").fromNow();
                  return (
                    <Touchable
                      // underlayColor="white"
                      activeOpacity={0}
                      onPress={() => {
                        this.props.changeView("VIEW");
                        this.props.setCustomer(
                          JSON.parse(JSON.stringify(customer))
                        );
                      }}
                      key={index}
                    >
                      <View style={styles.contentRow}>
                        <Text style={styles.customerNamecol}>
                          {customer.name}
                        </Text>
                        <Text style={styles.customerDays}>{time}</Text>
                      </View>
                    </Touchable>
                  );
                })}
              </ScrollView>
            )
          ) : (
            <View style={[styles.container]}>
              <ActivityIndicator size={50} color="grey" />
              <Text style={styles.loaderText}>Loading...</Text>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styles = {
  mainContainer: {},
  productSearch: {
    backgroundColor: "#fff",
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 1,
    width: "100%",
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 5
  },
  loaderText: {
    marginTop: 15
  },
  container: {
    height: "60%",
    // flex: 1,
    display: "flex",
    // flexDirection:'column',
    alignItems: "center",
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  productSearchIcon: {
    color: "#c858f7",
    fontSize: 24,
    fontWeight: "600"
  },
  productCloseIcon: {
    color: "#c858f7",
    fontSize: 24,
    fontWeight: "600"
  },
  createNewbtn: {
    backgroundColor: "#fafbfc",
    borderWidth: 1,
    borderColor: "#c2c7cc",
    width: "95%",
    marginRight: 22,
    marginLeft: 22,
    borderRadius: 5,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    paddingTop: 20,
    paddingBottom: 20,
    height: 50,
    marginTop: 5
  },
  createbtn: {
    paddingTop: 40,
    paddingBottom: 40
  },
  contentRow: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    borderTopWidth: 1,
    borderTopColor: "#f3f3f7",
    paddingTop: 20,
    paddingBottom: 20,
    marginRight: 22,
    marginLeft: 22
  },
  recentlyCreated: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 20,
    paddingBottom: 20,
    marginRight: 22,
    marginLeft: 22,
    marginTop: 10
  },
  customerNamecol: {
    color: "#555557"
  },

  customerDays: {
    color: "#939598"
  },
  cutomerViewmain: {}
};
export default AddCustomer;
