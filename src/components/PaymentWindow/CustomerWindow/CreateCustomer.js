import React from "react";
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  Platform,
  TouchableNativeFeedback,
  TouchableHighlight,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import { Button, Icon } from "native-base";

const Touchable =
  (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;
class CreateCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.customers.length + 1,
      name: "",
      lastName: "",
      phone: "",
      vehicle_number: "",
      errors: [],
      phoneNumError: "",
      nameError: ""
    };
    this.addCustomerData = this.addCustomerData.bind(this);
    this.isValidate = this.isValidate.bind(this);
  }
  isValidate() {
    let { phone, name } = this.state;
    // if (!firstName || !phone) {
    if (name.length <= 0) {
      this.setState({
        errors: ["Name can't be blank"]
      });
      return false;
    }
    if (phone.length <= 0) {
      this.setState({
        errors: ["Phone number can't be blank"]
      });
      return false;
    }
    if (phone.length > 11 || phone.length < 11) {
      this.setState({
        errors: ["Please enter a valid phone number"]
      });
      return false;
    }

    this.setState({
      errors: []
    });
    return true;
  }
  addCustomerData() {
    const { name, phone, vehicle_number } = this.state;
    let data = {
      name,
      phone,
      vehicle_number:vehicle_number,
    };
    if (this.isValidate()) {
      this.props.createCustomer(data);
      this.props.addCustomer(data);
      this.props.onHideModal("addCustomer");
    }
  }
  render() {
    let { propStyles } = this.props;
    let { phoneNumError, nameError, errors } = this.state;
    return (
      <KeyboardAvoidingView behaviour="padding">
        <View style={{ ...propStyles.modalHeader, ...styles.headercont }}>
          <Touchable
            onPress={() => this.props.onHideModal("addCustomer")}
          >
          <View
            style={{ ...propStyles.closeButton, width: 40 }}          
          >
            <Icon style={propStyles.closeButtonText} name="md-close" />
          </View>
          </Touchable>
          <Text style={{ ...propStyles.headerText, ...styles.headercustm }}>
            Create New Customer
          </Text>
          <Button
            style={styles.addSalebtn}
            onPress={this.addCustomerData}
            disabled={this.state.edit}
          >
            <Text style={styles.addSalebtntxt}>Save</Text>
          </Button>
        </View>

        <View style={styles.formMain}>
          {errors.length > 0 ? (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{errors[0]}</Text>
            </View>
          ) : null}

          <View style={styles.forminner}>
            <View>
              <TextInput
                style={styles.formInput}
                placeholderTextColor="#999999"
                onChangeText={name => {
                  this.setState({ name });
                }}
                vlaue={this.state.name}
                placeholder="Name *"
              />
              {/* {this.isValidate ? (
                <Text style={styles.errorText}>
                  {nameError ? nameError : null}
                </Text>
              ) : null} */}
            </View>
            {/* <View>
              <TextInput
                style={styles.formInput}
                placeholderTextColor="#999999"
                onChangeText={lastName => {
                  this.setState({ lastName });
                }}
                value={this.state.lastName}
                placeholder="Last name"
              />
            </View> */}

            <View>
              <TextInput
                style={styles.formInput}
                keyboardType="numeric"
                placeholderTextColor="#999999"
                onChangeText={phone => {
                  this.setState({ phone });
                }}
                value={this.state.phone}
                placeholder="Phone number *"
              />

              {/* {this.isValidate ? (
                <Text style={styles.errorText}>
                  {phoneNumError ? phoneNumError : null}
                </Text>
              ) : null} */}
            </View>

            <View>
              <TextInput
                style={styles.formInput}
                placeholderTextColor="#999999"
                onChangeText={vehicle_number => {
                  this.setState({ vehicle_number });
                }}
                value={this.state.vehicle_number}
                placeholder="vehicle_number number"
              />
            </View>
          </View>
          {/* <View style={styles.crtuserbtnMain}>
            <Button
              style={styles.crtuserbtn}
              transparent
              onPress={this.addCustomerData}
            >
              <Text>Save</Text>
            </Button>
          </View> */}
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = {
  contentRow: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  headercont: {
    paddingBottom: 0,
    paddingTop: 0
  },
  addSalebtntxt: {
    color: "#fff"
  },
  addSalebtn: {
    width: "18%",
    height: "100%",
    marginLeft: "-5%",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "#746dc8"
  },
  headercustm: {
    width: "80%",
    textAlign: "left"
  },
  placetextcolor: {
    borderWidth: 5
  },
  formMain: {},
  forminner: {
    height: "75%",
    marginRight: 20,
    marginLeft: 20
  },
  formInput: {
    color: "#555557",
    paddingTop: 20,
    paddingBottom: 20
  },
  crtuserbtnMain: {
    height: "20%"
  },
  crtuserbtn: {
    backgroundColor: "#fafbfc",
    width: "92%",
    borderWidth: 1,
    borderColor: "#c2c7cc",
    borderRadius: 5,
    paddingTop: 30,
    paddingBottom: 30,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginLeft: "4%"
  },
  errorContainer: {
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "lightgray",
    paddingLeft: 20
  },
  errorText: {
    color: "red",
    marginRight: 25
  }
};
export default CreateCustomer;
