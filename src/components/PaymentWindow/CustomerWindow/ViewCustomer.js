import React from "react";
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  Image,
  ImageBackground,
  TouchableHighlight,
  Platform,
  TextInput,
  Dimensions,
  Keyboard
} from "react-native";
import { Button, Icon } from "native-base";
import { updateCustomer } from "../../../actions";

const Touchable =
  (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;

class ViewCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      name: this.props.info.name,
      phone: this.props.info.phone ? this.props.info.phone : '',
      vehicle_number: this.props.info.vehicle_number ? this.props.info.vehicle_number : '',
      errors: []
    };
    this.isValidate = this.isValidate.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.selectCustomer = this.selectCustomer.bind(this);
  }

  onEdit() {
    var { name, phone, vehicle_number } = this.state;
    let data = {
      id: this.props.info.id,
      name,
      phone,
      vehicle_number:vehicle_number
    };
    if (this.isValidate()) {
      this.props.updateCustomer(data);
      this.setState({ edit: false });
    }
  }

  isValidate() {
    let { phone, name } = this.state;
    // if (!firstName || !phone) {
    if (name.length <= 0) {
      this.setState({
        errors: ["First name can't be blank"]
      });
      return false;
    }
    if (phone.length <= 0) {
      this.setState({
        errors: ["Phone number can't be blank"]
      });
      return false;
    }
    if (phone.length > 11 || phone.length < 11) {
      this.setState({
        errors: ["Please enter a valid phone number"]
      });
      return false;
    }

    this.setState({
      errors: []
    });
    return true;
  }

  selectCustomer() {
    var { name, phone, vehicle_number } = this.state;
    let data = {
      id: this.props.info.id,
      name,
      phone,
      vehicle_number
    };
    this.props.addCustomer(data);
    this.props.onHideModal("addCustomer");
  }
  render() {
    let { propStyles, customers } = this.props;
    var { name, phone, edit, errors, vehicle_number } = this.state;
    return (
      <View>
        <View style={{ ...propStyles.modalHeader, ...styles.headercont }}>
          <Touchable
            onPress={() => this.props.onHideModal("addCustomer")}
          >
          <View
            style={{ ...propStyles.closeButton, width: 40 }}          
          >
            <Icon style={propStyles.closeButtonText} name="md-close" />
          </View>
          </Touchable>
          <Text style={{ ...propStyles.headerText, ...styles.headercustm }}>
            {this.state.name}
          </Text>
          <Button
            style={styles.addSalebtn}
            onPress={() => this.selectCustomer()}
            disabled={this.state.edit}
          >
            <Text style={styles.addSalebtntxt}>Add to sale</Text>
          </Button>
        </View>

        <View>
          {errors.length > 0 ? (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{errors[0]}</Text>
              {/* <Text>dsadsad</Text> */}
            </View>
          ) : null}
          <View>
            <View style={styles.personalInfo}>
              <Text>Personal Information</Text>
            </View>

            <View
              style={{
                ...styles.contentRow,
                ...styles.borderBotm,
                borderBottomColor: this.state.edit ? "#8881e2" : "#f3f3f7",
                borderBottomWidth: this.state.edit ? 2 : 1
              }}
            >
              <Text style={styles.Rightside}>Name</Text>
              <TextInput
                underlineColorAndroid="transparent"
                onChangeText={name => {
                  this.setState({ name });
                }}
                value={name}
                editable={edit}
                style={{
                  ...styles.Leftside,
                  color: this.state.edit ? "#8881e2" : "#939598"
                }}
              />
            </View>


            <View
              style={{
                ...styles.contentRow,
                ...styles.borderBotm,
                borderBottomColor: this.state.edit ? "#8881e2" : "#f3f3f7",
                borderBottomWidth: this.state.edit ? 2 : 1
              }}
            >
              <Text style={styles.Rightside}>Phone</Text>
              <TextInput
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                onChangeText={phone => {
                  this.setState({ phone });
                }}
                value={phone}
                editable={edit}
                style={{
                  ...styles.Leftside,
                  color: this.state.edit ? "#8881e2" : "#939598"
                }}
              />
            </View>
            <View
              style={{
                ...styles.contentRow,
                ...styles.borderBotm,
                borderBottomColor: this.state.edit ? "#8881e2" : "#f3f3f7",
                borderBottomWidth: this.state.edit ? 2 : 1
              }}
            >
              <Text style={styles.Rightside}>Vehicle Number</Text>
              <TextInput
                underlineColorAndroid="transparent"
                onChangeText={vehicle_number => {
                  this.setState({ vehicle_number });
                }}
                value={vehicle_number}
                editable={edit}
                style={{
                  ...styles.Leftside,
                  color: this.state.edit ? "#8881e2" : "#939598"
                }}
              />
            </View>
          </View>
          {this.state.edit ? (
            <Button
              transparent
              style={styles.editInformation}
              onPress={() => this.onEdit()}
            >
              <Text>Save</Text>
            </Button>
          ) : (
              <Button
                transparent
                style={styles.editInformation}
                onPress={() => this.setState({ edit: true })}
              >
                <Text>Edit Personal Information</Text>
              </Button>
            )}
          <View style={styles.seprator} />
          <Button
            transparent
            style={styles.removeCustomer}
            onPress={() => {
              this.props.addCustomer({});
              this.props.onHideModal("addCustomer");
            }}
          >
            <Text style={styles.removetxt}>Remove Customer from Sale</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = {
  headercont: {
    paddingBottom: 0,
    paddingTop: 0
  },
  addSalebtn: {
    width: "18%",
    height: "100%",
    marginLeft: "-5%",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "#746dc8"
  },
  addSalebtntxt: {
    color: "#fff"
  },
  personalInfo: {
    display: "flex",
    flexDirection: "row",
    marginLeft: "4%",
    marginRight: "4%",
    paddingTop: 40,
    paddingBottom: 0
  },
  contentRow: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: "4%",
    marginRight: "4%",
    paddingTop: 10,
    paddingBottom: 10
  },
  headercustm: {
    width: "80%",
    textAlign: "left"
  },
  seprator: {
    height: 2,
    borderBottomWidth: 2,
    borderBottomColor: "#f3f3f7",
    marginTop: "3%",
    width: "92%",
    marginLeft: "4%"
  },
  borderBotm: {
    borderBottomWidth: 1,
    borderBottomColor: "#f3f3f7"
  },
  Rightside: {
    textAlign: "right",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    alignSelf: "center",
    borderBottomWidth: 0,
    borderColor: "#af2d2d"
  },
  Leftside: {
    textAlign: "right",
    display: "flex",
    width: '50%',
    flexDirection: "column",
    alignItems: "center",
    alignSelf: "center"
  },
  editInformation: {
    backgroundColor: "#fafbfc",
    width: "92%",
    borderWidth: 1,
    borderColor: "#c2c7cc",
    borderRadius: 5,
    paddingTop: 30,
    paddingBottom: 30,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginLeft: "4%",
    marginTop: "4%",
    marginBottom: 10
  },
  removeCustomer: {
    backgroundColor: "#fafbfc",
    width: "92%",
    borderWidth: 1,
    borderColor: "#c2c7cc",
    borderRadius: 5,
    paddingTop: 30,
    paddingBottom: 30,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginLeft: "4%",
    marginTop: "4%",
    marginBottom: "4%"
  },
  removetxt: {
    color: "#ef4136"
  },
  errorContainer: {
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "lightgray",
    paddingLeft: 20
  },
  errorText: {
    color: "red",
    marginRight: 25
  }
};
export default ViewCustomer;
