import React from "react";
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  TouchableHighlight,
  TextInput,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard
} from "react-native";
import { Button, Icon } from "native-base";
import AddCustomer from "./AddCustomer";
import CreateCustomer from "./CreateCustomer";
import ViewCustomer from "./ViewCustomer";
import { connect } from "react-redux";
import {
  createCustomer,
  updateCustomer,
  searchCustomer,
  customersOnLoad
} from "../../../actions";

class CustomerWindow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeView:
        JSON.stringify(this.props.currentCustomer) !== "{}" ? "VIEW" : "ADD",
      currentCustomer: this.props.currentCustomer,
      customersList: null,
      loader:false,
      errors:''
    };
    this.activeView = this.activeView.bind(this);
    this.changeView = this.changeView.bind(this);
    this.setCustomer = this.setCustomer.bind(this);
  }
  componentDidMount() {
    this.setState({loader:true})
    this.props.customersOnLoad().then(res => {
      this.setState({
        customersList: this.props.customers,
        loader: false
      })
    }).catch(err =>{
      this.setState({
        loader:false,
        errors: 'Sorry try again!'
      })
    })

  }
  changeView(view) {
    this.setState({
      activeView: view
    });
  }
  setCustomer(data) {
    this.setState({ currentCustomer: data });
  }
  activeView() {
    switch (this.state.activeView) {
      case "ADD":
        return (
          <AddCustomer
            loader={this.state.loader}
            errors={this.state.errors}
            searchCustomer={this.props.searchCustomer}
            setCustomer={this.setCustomer}
            changeView={this.changeView}
            customers={this.state.customersList}
            propStyles={styles}
            onHideModal={this.props.onHideModal}
          />
        );
      case "CREATE":
        return (
          <CreateCustomer
            customers={this.state.customersList}
            addCustomer={this.props.addCustomer}
            createCustomer={this.props.createCustomer}
            changeView={this.changeView}
            propStyles={styles}
            onHideModal={this.props.onHideModal}
          />
        );
      case "VIEW":
        return (
          <ViewCustomer
            updateCustomer={this.props.updateCustomer}
            info={this.state.currentCustomer}
            addCustomer={this.props.addCustomer}
            propStyles={styles}
            onHideModal={this.props.onHideModal}
          />
        );
      default:
        return (
          <AddCustomer
            searchCustomer={this.props.searchCustomer}
            setCustomer={this.setCustomer}
            changeView={this.changeView}
            customers={this.state.customersList}
            propStyles={styles}
            onHideModal={this.props.onHideModal}
          />
        );
    }
  }
  render() {
    return (
      <KeyboardAvoidingView
        behaviour="padding">
        <Modal
          transparent={true}
          animationType="fade"
          visible={this.props.isOpen}
          onRequestClose={() => this.props.onHideModal("addCustomer")}
          onPress={() => this.props.onHideModal()}
        >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
              <View style={styles.modalInner}>{this.activeView()}</View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </KeyboardAvoidingView>
    );
  }
}

const styles = {
  container: {
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: "center",
    flexDirection: "row",
    alignContent: "space-between",
    backgroundColor: "#00000098",
    width: "100%",
    height: "100%"
  },
  addQuantity: {
    justifyContent: "center",
    flexDirection: "row",
    marginBottom: 10,
    width: "75%",
    paddingTop: "10%"
  },
  addQuantityinner: {
    justifyContent: "center",
    flexDirection: "row",
    width: "100%",
    paddingLeft: "15%",
    fontFamily: "Roboto",
    fontWeight: "700",
    fontSize: 18,
    color: "#555556"
  },
  modalInner: {
    backgroundColor: "#fff",
    height: "100%",
    width: "70%"
  },
  modalHeader: {
    height: "10%",
    backgroundColor: "#978add",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 70,
    width: "100%"
  },
  modalContent: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: "center",
    flexDirection: "row"
  },
  image: {
    height: 140
  },
  closeButton: {
    width: 60,
    height:'100%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
  },
  closeButtonText: {
    color: "#fff",
    fontSize: 25,
    // paddingLeft: 15,
    // paddingTop: 5,
    // marginLeft: -10,
    fontWeight: "900"
  },
  headerText: {
    color: "#fff",
    width: "96%",
    fontSize: 20,
    marginLeft: 5,
    fontFamily: "Roboto",
    fontWeight: "100",
    fontSize: 20
  },
  headerPriceText: {
    color: "#fff",
    fontSize: 20,
    textAlign: "right",
    position: "relative",
    right: 10,
    width: "30%",
    fontFamily: "Roboto",
    fontWeight: "100",
    fontSize: 18
  }
};

function mapStateToProps(state) {
  return {
    customers: state.customer_reducer
  };
}
export default connect(mapStateToProps, {
  createCustomer,
  updateCustomer,
  searchCustomer,
  customersOnLoad
})(CustomerWindow);
