import React from 'react';
import {View,Text} from 'react-native';

class DisplayWindow extends React.Component {
    render(){
        var {tenderAmount,billAmount} = this.props;
        return (
                // !tenderAmount?
                <View style={styles.BillAmountmain}>
                    <Text style={styles.billAmountHeading}>Billed Amount</Text>                                                                
                    <Text style={styles.billAmounttext}>Rs. {billAmount}</Text>                                
                </View>
                
        );
    }
}

const styles = {
    BillAmountmain: {
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
    },
    billAmountHeading:{
        fontSize:15,
        fontFamily: 'Roboto',
        color:'gray'
    },
    billAmounttext: {
        fontSize: 56,
        color: '#555556',
        fontFamily: 'Roboto',
        fontWeight: '400'
    },
    billButtonbx: {
        flexDirection:'row',
        width:'100%',
        justifyContent:'center',
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 20,
    },
    displayRow:{
        width:'24%',
        margin:0,
        marginTop:10,
        marginBottom:10,
    },
    displayRowText:{
        fontSize: 15,        
        fontFamily: 'Roboto',
        color:'white',
        fontWeight: '300',
    },
    displayRowTendered: {
        color:'gray',
        fontSize: 15,
        fontFamily: 'Roboto',
    },
    displayRowContent:{
        width:'100%',
        marginTop:5,
        marginRight:0,
        marginLeft:0,
        height:60,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 15,
        paddingRight: 15,
        justifyContent:'center'
    }
}

export default DisplayWindow;