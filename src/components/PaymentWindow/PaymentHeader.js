import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, ScrollView, View, Image, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import { Container, Item, Input, Header, Left, Body, Right, Button, Title, Icon } from 'native-base';
import { Grid, Row, Col } from "react-native-easy-grid";
import { NavigationActions } from 'react-navigation';

class PaymentHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartData: [],
            query: '',
            data: [],
            inputText: '',
            finalizeModal: false
        }
        this.hideModal = this.hideModal.bind(this)
    }
    hideModal() {
        this.setState({
            finalizeModal: false,
        })
    }
    showModal() {
        this.setState({
            finalizeModal: true,
        })
    }
    validate() {
        if (this.state.inputText !== '' && (this.props.tenderAmount >= this.props.billAmount)) {
            this.showModal()
        }
    }
    inputText(ev) {
        this.setState({
            inputText: ev.nativeEvent.text
        })
    }

    render() {
        return (
            <Container style={styles.mainCont} noShadow>
                <Header style={styles.headerMain} noShadow>
                    <Left style={styles.headerLeft}>
                        <Button transparent style={styles.backBtn} onPress={() => {
                            this.props.dispatch(NavigationActions.back())
                        }}>
                            <Icon style={styles.backiconFirst} name='ios-arrow-back' />
                            <Text style={styles.backiconText}>Back to Sale</Text>
                        </Button>
                    </Left>
                    <Body style={styles.headerBody}>
                        <Title style={styles.headerBodytext}>PAYMENT</Title>
                    </Body>
                    
                </Header>
            </Container>
        )
    }
}

const styles = {
    mainCont: {
        height: '100%',
        padding:0
    },
    headerMain: {
        backgroundColor: '#fff',
        // paddingTop: 8,
        // paddingBottom: 10,
        // paddingRight: 20,
        // paddingLeft: 20,
        padding:0,
        height: '100%',

    },
    headerLeft: {
        width: '20%',
        display: 'flex',
        flexDirection: 'row',
    },
    headerBody: {
        width: '40%',
        display: 'flex',
        marginLeft: '-12%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerBodytext: {
        textAlign: 'center',
        color: '#262626',
        fontSize: 22,
        fontFamily: 'Roboto',
        fontWeight: '500',
        marginLeft: '-9%',
    },
    
    backiconFirst: {
        color: '#575757',
        fontWeight: '500',
        marginRight: 8
    },
    backiconText: {
        color: '#575757',
        marginRight: 20,
        marginLeft: 5,
        fontFamily: 'Roboto',
        fontSize: 18,
        fontWeight: '400',
        borderRadius: 3,
    },
    backBtn: {
        width: '80%',
        margin:0,
        height:100,
        marginLeft: '-8%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    carNumbermain: {
        width: '65%',
        marginRight: 20,
        borderColor: '#bcbcbc',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        height: 45,
        borderRadius: 3,
        paddingLeft: 10,
        paddingRight: 10,
    },
    frwrdBtn: {
        width: '35%',
        borderColor: '#bcbcbc',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
        marginRight: -2,
    },
    frwrdiconFirst: {
        color: '#555556',
    },
    frwrdiconText: {
        color: '#555556',
        marginRight: 5,
        marginLeft: 20,
        fontFamily: 'Roboto',
        fontSize: 18,
    },
    carIcon: {
        color: '#b270eb',
    },
}
export default PaymentHeader;