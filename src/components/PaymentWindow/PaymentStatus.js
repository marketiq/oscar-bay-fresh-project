import React from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
  Image,
  ActivityIndicator,
  ToastAndroid
} from "react-native";
import { Icon, Button, Item,Toast } from "native-base";
import { PayCashModal, PayCardModal } from "../common";
import CustomerWindow from "./CustomerWindow";
import { NavigationActions } from "react-navigation";
import { emptyCart } from "../../actions/";

class PaymentStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cashPayment: false,
      cardPayment: false,
      addCustomer: false,
      carNum: "",
      customer: {},
      activeView: "",
    };
    this.onHideModal = this.onHideModal.bind(this);
    this.addCustomer = this.addCustomer.bind(this);
    this.setCarNum = this.setCarNum.bind(this);
  }
  addCustomer(data) {
    this.setState({
      carNum: "",
      customer: JSON.parse(JSON.stringify(data))
    });
  }
  setCarNum(carNum) {
    this.setState({ carNum });
  }
  onHideModal(key) {
    this.setState(
      {
        [key]: false
      },
      () => {
        if (key === "cashPayment") {
          this.props.dispatch(emptyCart());
          this.props.dispatch(
            NavigationActions.navigate({ routeName: "Dashboard" })
          );
        }
      }
    );
  }
 
  render() {
    var { tenderAmount, billAmount } = this.props;
    var { customer } = this.state;
    tenderAmount = tenderAmount ? parseInt(tenderAmount) : 0;
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.vehicleNumView}>
            <Icon name="ios-car" style={styles.carIcon} />
            {customer.vehicle_number ? (
              <Text>{customer.vehicle_number}</Text>
            ) : (
              <TextInput
                style={{ width: "100%" }}
                underlineColorAndroid="rgba(0,0,0,0)"
                value={this.state.carNum}
                onChangeText={this.setCarNum}
                placeholder="Vehicle number here"
              />
            )}
          </View>

          <View style={styles.addCustomerView}>
            {JSON.stringify(customer) !== "{}" ? (
              <TouchableHighlight
                style={styles.addCustomerBtn}
                onPress={() => {
                  this.setState({ addCustomer: true });
                }}
                underlayColor="lightgray"
                activeOpacity={0.2}
              >
                <Text>{customer.name}</Text>
              </TouchableHighlight>
            ) : (
              <TouchableHighlight
                style={styles.addCustomerBtn}
                onPress={() => {
                  this.setState({ addCustomer: true });
                }}
                underlayColor="lightgray"
                activeOpacity={0.2}
              >
                <Icon name="md-person-add" />
              </TouchableHighlight>
            )}
          </View>
        </View>
        <View
          style={{
            height: "91.7%"
          }}
        >
          <View style={styles.contentContainer}>
            <View style={styles.contentViewRow}>
              <Text style={styles.Duetext}>Due</Text>
              <Text style={styles.Dueamount}>Rs. {billAmount}</Text>
            </View>
            <View style={styles.contentViewRow}>
              <Text style={styles.tendertExt}>Tendered</Text>
              <Text style={styles.tenderAmount}>Rs. {tenderAmount}</Text>
            </View>
            <View style={styles.contentViewRow}>
              <Text style={styles.changetExt}>Change</Text>
              <Text style={styles.changeAmount}>
                Rs.{" "}
                {tenderAmount - billAmount < 0 ? 0 : tenderAmount - billAmount}
              </Text>
            </View>
          </View>

          <View style={styles.footer}>
            <View style={styles.footerTxt}>
              <View style={styles.listItmMain}>
                <Text style={styles.footerInertext}>
                  {/* How would you like to payment? */}
                </Text>
              </View>
            </View>
            <View style={styles.orderButton}>
              <Button
                style={{
                  ...styles.footerBtn,
                  backgroundColor:
                    tenderAmount - billAmount >= 0 && billAmount > 0
                      ? "#d7df23"
                      : "lightgray"
                }}
                disabled={
                  tenderAmount - billAmount >= 0 && billAmount > 0
                    ? false
                    : true
                }
                onPress={this.props.onSubmitOrder(
                      customer.vehicle_number || this.state.carNum
                    )}
              >
                {this.props.loader ? (
                  <ActivityIndicator size="large" color="white" />
                ) : (
                  <View style={{ ...styles.footerBtn, marginTop: 0 }}>
                    <Icon
                      name="ios-cash-outline"
                      style={styles.footerBtnicon}
                    />
                    <Text style={styles.footerBtnText}>Cash</Text>
                  </View>
                )}
              </Button>
            </View>
          </View>
        </View>
        {this.props.cashPayment ? (
          <PayCashModal
            dispatch={this.props.dispatch}
            orderNum={this.props.orderNum}
            tenderAmount={this.props.tenderAmount}
            billAmount={this.props.billAmount}
            isOpen={this.props.cashPayment}
            onHideModal={this.onHideModal}
          />
        ) : null}
        {this.state.addCustomer ? (
          <CustomerWindow
            currentCustomer={this.state.customer}
            addCustomer={this.addCustomer}
            isOpen={this.state.addCustomer}
            onHideModal={this.onHideModal}
          />
        ) : null}
      </View>
    );
  }
}

const styles = {
  container: {
    width: "100%",
    display: "flex"
  },

  searchMain: {
    flexDirection: "row",
    width: 300,
    backgroundColor: "transparent",
    borderRadius: 15,
    marginRight: 10,
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    marginTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: "#b968ee"
  },
  headerContainer: {
    flexDirection: "row",
    height: "10%"
  },
  vehicleNumView: {
    height: "100%",
    width: "85%",
    alignSelf: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    borderWidth: 1,
    borderLeftWidth: 0,
    borderColor: "#e7e7ef",
    paddingRight: 15
  },
  addCustomerView: {
    borderWidth: 1,
    borderLeftWidth: 0,
    borderColor: "#e7e7ef",
    width: "15%"
  },
  contentViewRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 18,
    paddingBottom: 18,
    marginRight: 15,
    marginLeft: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#f3f3f7"
  },
  addCustomerBtn: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  contentContainer: {
    height: "65%"
  },
  Duetext: {
    color: "#222222"
  },
  Dueamount: {
    color: "#222222"
  },
  tendertExt: {
    color: "#00aeef"
  },
  tenderAmount: {
    color: "#00aeef"
  },
  changetExt: {
    color: "#91d625"
  },
  changeAmount: {
    color: "#91d625"
  },
  orderButton: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  orderTxt: {
    color: "#ffffff",
    fontSize: 22,
    fontFamily: "Roboto",
    fontWeight: "600"
  },
  listItmMain: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderTopColor: "#e7e7ef",
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: -10
  },
  carIcon: {
    color: "#b270eb",
    marginLeft: 10,
    marginRight: 20
  },
  footer: {
    paddingTop: 20,
    height: "32%"
  },
  footerTxt: {
    height: "40%",
    paddingTop: 10
  },
  footerInertext: {
    color: "#555556",
    fontSize: 20
  },
  footerBtn: {
    width: "100%",
    justifyContent: "center",
    height: "50%",
    marginTop: 10,
    borderRadius: 0,
    display: "flex",
    flexDirection: "row"
  },
  footerBtnText: {
    color: "white",
    fontSize: 30,
    fontWeight: "400",
    fontFamily: "Roboto"
  },
  footerBtnicon: {
    color: "#fff",
    fontSize: 45,
    marginTop: 5
  },
  cashBtn: {
    backgroundColor: "#d7df23"
  }
};

export default PaymentStatus;
