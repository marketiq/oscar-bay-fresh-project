import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, ScrollView, View, Image, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import { Icon, Item, Input, Button } from 'native-base';
// import { APP_BACKGROUND_COLOR } from '../../constants/constants';
import { Grid, Row, Col } from "react-native-easy-grid";

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartData: [],
            query: '',
            data: [],
            searchInput: ''
        }

    }
    render() {
        return (
            <View style={styles.siderbarMain}>
                <Button bordered dark style={styles.buttonActive}>
                    <Icon style={styles.sidebarIconactive} name="ios-cash-outline"></Icon>
                    <Text style={styles.sidebarButtonactive}>Cash (PKR)</Text>
                </Button>
                
                <Button bordered dark style={{...styles.button, ...styles.marTopnone}}>
                    <Icon style={styles.sidebarIcon} name="ios-card"></Icon>
                    <Text style={styles.sidebarButton}>Credit Card (PKR)</Text>
                </Button>

                <View style={styles.borderDivider}></View>
                
                <Button bordered dark style={styles.button}>
                    <Icon style={styles.sidebarIcon} name="md-person"></Icon>
                    <Text style={styles.sidebarButton}>Customer</Text>
                </Button>
                
                <Button bordered dark style={styles.button}>
                    <Icon style={styles.sidebarIcon} name="ios-star-outline"></Icon>
                    <Text style={styles.sidebarButton}>Loyality</Text>
                </Button>
                
                <Button bordered dark style={styles.button}>
                    <Icon style={styles.sidebarIcon} name="md-text"></Icon>
                    <Text style={styles.sidebarButton}>Feedback</Text>
                </Button>
                
                <Button bordered dark style={styles.button}>
                    <Icon style={styles.sidebarIcon} name="ios-document"></Icon>
                    <Text style={styles.sidebarButton}>Digital Receipt</Text>
                </Button>
            </View>
        )
    }
}

const styles = {
    siderbarMain: {
        marginTop: 20,
    },
    button: {
        borderColor: '#bcbcbc', 
        borderRadius: 3,
        width: '94%', 
        height: '12.8%', 
        marginLeft: '3%', 
        marginRight: '10%', 
        marginTop: '3%', 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'flex-start'
    },
    buttonActive: {
        borderColor: '#978add', 
        backgroundColor: '#ce52fa',
        borderRadius: 3,
        width: '94%', 
        height: '12.8%', 
        marginLeft: '3%', 
        marginRight: '10%', 
        marginTop: '3%', 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'flex-start'
    },
    borderDivider: {
        height:2,
        backgroundColor: '#f3f3f7',
        width: '94%', 
        marginLeft: '3%', 
        marginRight: '10%', 
        marginTop: '8%',
        marginBottom: '5%',
    },
    marTopnone :{
        marginTop: 0,
        borderTopWidth: 0,
    },
    sidebarButton:{
        color : '#555556',
        fontSize: 20,
        fontWeight: '400',
    },
    sidebarIcon: {
        color: '#b270eb',
        fontSize: 30,
        paddingRight: 20,
        paddingLeft: 30,
    },
    sidebarButtonactive:{
        color: '#fff',
        fontWeight: '300',
    },
    sidebarIconactive: {
        color: '#fff',
        fontSize: 30,
        paddingRight: 20,
        paddingLeft: 30,
    },
}
export default Sidebar;