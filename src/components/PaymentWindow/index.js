import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  ScrollView,
  View,
  Keyboard,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Dimensions,
  TouchableWithoutFeedback,
} from "react-native";
import { Card } from "react-native-elements";
import { Icon, Item, Input, Button, Toast } from "native-base";
import { Grid, Row, Col } from "react-native-easy-grid";
import { NavigationActions } from "react-navigation";

import PaymentHeader from "./PaymentHeader";
import Sidebar from "./Sidebar";
import { APP_BACKGROUND_COLOR } from "../../constants/constants";
import CalculatorContainer from "./CalculatorContainer";
import DisplayWindow from "./DisplayWindow";
import PaymentStatus from "./PaymentStatus";
import { submitOrder } from "../../actions";

var { height, width } = Dimensions.get("window");
let tempHeight = height;

class PaymentWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: [],
      query: "",
      data: [],
      vehicleNum: "",
      orderNum: null,
      cashPayment: false,
      tenderAmount: "",
      cashBtnLoader: false,
      loader: false,
      error: ""
    };
    this.onChangeTenderAmount = this.onChangeTenderAmount.bind(this);
    this.onSubmitOrder = this.onSubmitOrder.bind(this);
    this.formatCartData = this.formatCartData.bind(this);
  }
  onChangeTenderAmount(value) {
    let { tenderAmount } = this.state;
    if (!tenderAmount && (value === 0 || value === "00")) {
      return;
    }
    if (value > 9) {
      let temp = tenderAmount ? parseInt(tenderAmount) : 0;
      temp += value;
      this.setState({
        tenderAmount: temp.toString()
      });
      return;
    }
    switch (value) {
      case "c":
        this.setState({ tenderAmount: "" });
        break;

      case "e":
        let temp = tenderAmount.slice(0, tenderAmount.length - 1);
        this.setState({ tenderAmount: temp });
        break;
      default:
        this.setState({
          tenderAmount: tenderAmount + value
        });
        break;
    }
  }
  formatCartData() {
    let cartData = [];
    for (var i = 0; i < this.props.cart.length; i++) {
      cartData.push([0, 0, this.props.cart[i]]);
    }
    return cartData;
  }
  onSubmitOrder = carNum => () => {
    this.setState({
      loader: true
    });
    let bill = Math.ceil(this.props.billAmount);
    let data = [
      {
        data: {
          amount_total: bill,
          amount_paid: this.state.tenderAmount,
          amount_tax: 0,
          amount_return: this.state.tenderAmount - bill,
          wk_loyalty_points: null,
          lines: this.formatCartData(),
          partner_id: false,
          sequence_number: 1,
          fiscal_position_id: false,
          customer_count: 1,
          vehicle_number: carNum
        },
        to_invoice: false
      }
    ];
    this.props
      .dispatch(submitOrder(data))
      .then(res => {
        this.setState({
          cashPayment: true,
          orderNum: res.data.sequence_number,
          loader: false
        });
      })
      .catch(err => {
        this.setState(
          {
            loader: false,
            error: "Order is not being processed. Please contact the manager."
          },
          () => {
            Toast.show({
              text: this.state.error,
              textStyle: {
                color: "white"
              },
              duration:5000,
              type: "danger",
              buttonText: "Okay"
            });
          }
        );
      });
  };
  render() {
    let bill = this.props.billAmount;
    let billAmount = bill ? Math.ceil(bill) : 0;
    console.ignoredYellowBox = ["Remote debugger"];
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Grid style={styles.mainGrid}>
          <Row size={100}>
            <Col size={5}>
              <Row size={10} style={styles.headerBorder}>
                <PaymentHeader
                  dispatch={this.props.dispatch}
                  tenderAmount={this.state.tenderAmount}
                  billAmount={billAmount}
                />
              </Row>
              <Row size={90} style={{}}>
                <Col size={8} style={styles.rightSide}>
                  <Row size={35} style={styles.billAmountmain}>
                    <DisplayWindow
                      tenderAmount={this.state.tenderAmount}
                      billAmount={billAmount}
                    />
                  </Row>
                  <Row size={65} style={styles.calculationMain}>
                    {billAmount > 0 ? (
                      <CalculatorContainer
                        onChange={this.onChangeTenderAmount}
                      />
                    ) : null}
                  </Row>
                </Col>
              </Row>
            </Col>

            <Col size={3} style={styles.leftSide}>
              <PaymentStatus
                loader={this.state.loader}
                orderNum={this.state.orderNum}
                cashBtnLoader={this.state.cashBtnLoader}
                onSubmitOrder={this.onSubmitOrder}
                cashPayment={this.state.cashPayment}
                dispatch={this.props.dispatch}
                tenderAmount={this.state.tenderAmount}
                billAmount={billAmount}
              />
            </Col>
          </Row>
        </Grid>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  mainGrid: {
    minHeight: tempHeight - 90,
    maxHeight: "100%"
  },
  mainRow: {
    flexDirection: "row",
    flex: 1
  },
  button: {
    width: "90%",
    marginLeft: "3%",
    marginLeft: "3%"
  },
  headerBorder: {
    borderBottomWidth: 1,
    borderBottomColor: "#e7e7ef"
  },
  billAmountmain: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#e7e7ef",
    borderStyle: "dotted"
  },

  calculationMain: {
    borderWidth: 0,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderColor: "#e7e7ef",
    height: "100%"
  },
  leftSide: {
    // backgroundColor: '#fafbfb', paddingTop: 15, paddingBottom: 15,
    backgroundColor: "#fff",
    borderLeftWidth: 1,
    borderLeftColor: "#e7e7ef"
  },
  rightSide: {
    backgroundColor: "#fff",
    borderLeftWidth: 1,
    borderLeftColor: "#e7e7ef"
  },
  productSearch: {
    backgroundColor: "#fff",
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 0,
    marginLeft: 15,
    marginRight: 15
  },
  tabSty: {
    color: "#af2d2d"
  }
};
const mapStateToProps = state => {
  return {
    billAmount: state.billAmount,
    cart: state.cart_reducer
  };
};

export default connect(mapStateToProps)(PaymentWindow);
