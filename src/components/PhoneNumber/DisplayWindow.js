import React from "react";
import { View, Text, Image } from "react-native";
import { Icon } from "native-base";
class DisplayWindow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.renderHeading = this.renderHeading.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
    this.renderPlaceHolder = this.renderPlaceHolder.bind(this);
  }
  renderPlaceHolder() {
    var { inputNum, activeView, phoneNum } = this.props;
    switch (activeView) {
      case "rechargeView":
        return (
          <Text style={styles.billAmounttext}>
            {inputNum ? "Rs." + inputNum : "Enter Recharge Amount"}
          </Text>
        );
      case "phoneView":
        return (
          <Text style={styles.billAmounttext}>
            {inputNum ? inputNum : "Enter your phone #"}
          </Text>
        );
      case "tenderView":
        return (
          <Text style={styles.billAmounttext}>
            {inputNum ? "Rs." + inputNum : "Enter Tender Amount"}
          </Text>
        );
    }
  }
  renderIcon() {
    var { activeView } = this.props;
    switch (activeView) {
      case "rechargeView":
        return (
          <Text style={styles.billicon}>
            <Image
              style={styles.icon}
              source={require("../../static/images/rehcarger_amount.png")}
            />
          </Text>
        );
      case "phoneView":
        return (
          <Text style={styles.billicon}>
            <Image 
              style={styles.icon}
              source={require("../../static/images/phone_number.png")} />
          </Text>
        );
      case "tenderView":
        return (
          <Text style={styles.billicon}>
            <Image 
              style={styles.icon}
              source={require("../../static/images/tender_icon.png")} />
          </Text>
        );
    }
  }
  renderHeading() {
    var { activeView } = this.props;
    switch (activeView) {
      case "rechargeView":
        return <Text style={styles.billAmountHeading}>Recharge Amount</Text>;
      case "phoneView":
        return <Text style={styles.billAmountHeading}>Phone Number</Text>;
      case "tenderView":
        return <Text style={styles.billAmountHeading}>Tender Amount</Text>;
    }
  }
  render() {
    var { tenderAmount, billAmount, inputNum, rechargeView } = this.props;
    return (
      <View style={styles.BillAmountmain}>
        {inputNum ? this.renderHeading() : null}
        <View style={styles.contMain}>
          {this.renderIcon()}
          {this.renderPlaceHolder()}
        </View>
      </View>
    );
  }
}

const styles = {
  contMain: {
    display: "flex",
    flexDirection: "row"
  },
  billicon: {
    paddingRight: 20,
    paddingTop: 8,
    display: "flex",
    flexDirection: "row"
  },
  billAmounttext: {
    fontSize: 50,
    color: "#555556",
    fontFamily: "Roboto",
    fontWeight: "400",
    display: "flex",
    flexDirection: "row"
  },
  BillAmountmain: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  billAmountHeading: {
    fontSize: 15,
    fontFamily: "Roboto",
    color: "gray"
  },
  billButtonbx: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 20
  },
  displayRow: {
    width: "24%",
    margin: 0,
    marginTop: 10,
    marginBottom: 10
  },
  displayRowText: {
    fontSize: 15,
    fontFamily: "Roboto",
    color: "white",
    fontWeight: "300"
  },
  displayRowTendered: {
    color: "gray",
    fontSize: 15,
    fontFamily: "Roboto"
  },
  displayRowContent: {
    width: "100%",
    marginTop: 5,
    marginRight: 0,
    marginLeft: 0,
    height: 60,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: "center"
  },
  icon: { 
    height: 80, 
    width: 80 
  }
};

export default DisplayWindow;
