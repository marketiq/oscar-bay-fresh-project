import React from "react";
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  Image,
  Platform
} from "react-native";
import { Button, Icon } from "native-base";
import { emptyCart } from "../../actions/";
import { NavigationActions } from "react-navigation";

const Touchable =
  (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;
class MobileRechargeModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      network: ""
    };
  }
  setNetwork(label) {
    this.setState({
      network: label
    });
    this.props.onHideRechargeModal();
  }
  render() {
    return (
      <Modal
        transparent={true}
        animationType="fade"
        visible={this.props.mobileRechargeModalOpen}
        onRequestClose={() => this.props.onHideRechargeModal()}
        onPress={() => this.props.onHideRechargeModal()}
      >
        <View style={styles.container}>
          <View style={styles.modalInner}>
            <View style={styles.modalHeader}>
              <Touchable
                onPress={() => this.props.onHideRechargeModal()}
              >
              <View style={styles.closeButton}>
                <Icon style={styles.closeButtonText} name="md-close" />
              </View>
              </Touchable>
              <Text style={styles.headerText}>Mobile Recharge</Text>
            </View>
            <Text style={styles.text}>PLEASE CHOOSE YOUR NETWORK</Text>
            <View style={styles.operatorsMain}>
              <Touchable
                underlayColor="lightgray"
                activeOpacity={0.5}
                onPress={() => {
                  this.props.setNetwork("zong");
                }}
              >
                <View style={styles.operatorsImage}>
                  <Image
                    style={styles.productImage}
                    source={require(`../../static/images/Zong_Logo.png`)}
                  />
                </View>
              </Touchable>
              <Touchable
                underlayColor="lightgray"
                activeOpacity={0.5}
                onPress={() => {
                  this.props.setNetwork("telenor");
                }}
              >
                <View style={styles.operatorsImage}>
                  <Image
                    style={styles.productImage}
                    source={require(`../../static/images/Telenor_Logo.png`)}
                  />
                </View>
              </Touchable>
              <Touchable
                style={styles.operatorsImage}
                underlayColor="lightgray"
                activeOpacity={0.5}
                onPress={() => {
                  this.props.setNetwork("ufone");
                }}
              >
                <View style={styles.operatorsImage}>
                  <Image
                    style={styles.productImage}
                    source={require(`../../static/images/Ufone_Logo.png`)}
                  />
                </View>
              </Touchable>
              <Touchable
                underlayColor="lightgray"
                activeOpacity={0.5}
                onPress={() => {
                  this.props.setNetwork("jazz");
                }}
              >
                <View style={styles.operatorsImage}>
                  <Image
                    style={styles.productImage}
                    source={require(`../../static/images/Jazz_Logo.png`)}
                  />
                </View>
              </Touchable>
              <Touchable
                underlayColor="lightgray"
                activeOpacity={0.5}
                onPress={() => {
                  this.props.setNetwork("warid");
                }}
              >
                <View style={styles.operatorsImage}>
                  <Image
                    style={styles.productImage}
                    source={require(`../../static/images/Warid_Logo.png`)}
                  />
                </View>
              </Touchable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  container: {
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: "center",
    flexDirection: "row",
    alignContent: "space-between",
    backgroundColor: "#00000098",
    width: "100%",
    height: "100%"
  },
  operatorsMain: {
    height: "70%",
    flexDirection: "row",
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center"
  },
  operatorsImage: {
    width: 200,
    height: 180,
    margin: 10,
    borderWidth: 2,
    borderColor: "#e8e9ea",
    borderRadius: 5
  },
  addQuantity: {
    justifyContent: "center",
    flexDirection: "row",
    marginBottom: 10,
    width: "75%",
    paddingTop: "10%"
  },
  productImage: {
    // width: 100,
    marginBottom: 13,
    marginTop: 10,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  addQuantityinner: {
    justifyContent: "center",
    flexDirection: "row",
    width: "100%",
    paddingLeft: "15%",
    fontFamily: "Roboto",
    fontWeight: "700",
    fontSize: 18,
    color: "#555556"
  },
  modalInner: {
    backgroundColor: "#fff",
    height: "100%",
    width: "70%"
  },
  modalHeader: {
    height: "10%",
    backgroundColor: "#978add",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    // paddingTop: 20,
    // paddingBottom: 20,
    // paddingLeft: 10,
    // paddingRight: 10,
    height: 70,
    width: "100%"
  },
  modalContent: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: "center",
    flexDirection: "row"
  },
  image: {
    height: 140
  },
  closeButton: {
    width: 50,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    height:'100%'
  },
  closeButtonText: {
    color: "#fff",
    fontSize: 25,
    fontWeight: "900"
  },
  headerText: {
    color: "#fff",
    width: "96%",
    fontSize: 20,
    marginLeft: 5,
    fontFamily: "Roboto",
    fontWeight: "100",
    fontSize: 20
  },
  headerPriceText: {
    color: "#fff",
    fontSize: 20,
    textAlign: "right",
    position: "relative",
    right: 10,
    width: "30%",
    fontFamily: "Roboto",
    fontWeight: "100",
    fontSize: 18
  },
  text: {
    color: "#555556",
    textAlign: "center",
    marginTop: 80,
    marginBottom: 40,
    fontSize: 20,
    fontWeight: "300",
    backgroundColor: "transparent"
  },
  removeText: {
    color: "#555556",
    fontFamily: "HelveticaNeue"
  },
  removeButton: {
    alignSelf: "center",
    backgroundColor: "#fafbfc",
    marginRight: 5,
    marginLeft: 5,
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "#c2c7cc",
    width: "40%",
    height: 60,
    paddingTop: 30,
    paddingBottom: 30,
    elevation: 0,
    justifyContent: "center",
    borderRadius: 5
  }
};

export default MobileRechargeModal;
