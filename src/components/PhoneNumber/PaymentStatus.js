import React from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Input, Image, Dimensions
} from "react-native";
import { Icon, Button, Item } from "native-base";
import { NavigationActions } from "react-navigation";
import { emptyCart } from "../../actions/";
import {PayCashModal} from '../../components/common';
import PhoneViewContainer from "./PhoneViewContainer";
import RechargeViewContainer from "./RechargeViewContainer";
import TenderViewContainer from "./TenderViewContainer";
import PhoneViewFooter from "./PhoneViewFooter";
import RechargeViewFooter from "./RechargeViewFooter";
import TenderViewFooter from "./TenderViewFooter";

class PaymentStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cashPayment: false,
      cardPayment: false,
      activeView: "",
      cashPayment: false,
    };

    this.renderImageOfNetwork = this.renderImageOfNetwork.bind(this);
    this.renderContainer = this.renderContainer.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onHideModal = this.onHideModal.bind(this);
    this.showModal = this.showModal.bind(this);
  }
  showModal() {
    this.setState({ cashPayment: true })
  }
  onHideModal(key) {
    this.setState({
      [key]: false
    }, () => {
      if (key === 'cashPayment') {
        this.props.dispatch(emptyCart());
        this.props.dispatch(NavigationActions.navigate({ routeName: 'Dashboard' }))
      }
    })
  }
  renderFooter() {
    var { network, activeView, phoneNum, inputNum, rechargeAmount } = this.props;
    switch (activeView) {
      case "rechargeView":
        return (

          <RechargeViewFooter styles={styles}
            onChangeView={this.props.onChangeView}
            onAddRechargeToCart={this.props.onAddRechargeToCart}
            toggleRecharge={this.props.toggleRecharge}
            inputNum={inputNum}
            cart={this.props.cart.length}
          />
        )
      case "phoneView":
        return (

          <PhoneViewFooter styles={styles}
            network= {this.props.network}
            onChangeView={this.props.onChangeView}
            toggleRecharge={this.props.toggleRecharge}
            inputNum={inputNum}
          />
        )
      case "tenderView":
        return (

          <TenderViewFooter styles={styles}
            onChangeView={this.props.onChangeView}
            toggleRecharge={this.props.toggleRecharge}
            inputNum={inputNum}
            showModal={this.showModal}
            rechargeAmount={rechargeAmount}
          />
        )
    }
  }
  renderContainer() {
    var { network, activeView, phoneNum, rechargeAmount, inputNum, cart } = this.props;

    switch (activeView) {
      case "rechargeView":
        return (
          <RechargeViewContainer styles={styles} phoneNum={phoneNum} network={network} renderImageOfNetwork={this.renderImageOfNetwork} />
        )
      case "phoneView":
        return (
          <PhoneViewContainer styles={styles} network={network} renderImageOfNetwork={this.renderImageOfNetwork} />
        )
      case "tenderView":
        return (
          <TenderViewContainer styles={styles} rechargeAmount={rechargeAmount} phoneNum={phoneNum} inputNum={inputNum} network={network} renderImageOfNetwork={this.renderImageOfNetwork} />
        )
    }
  }
  renderImageOfNetwork(network) {
    switch (network) {
      case 'ufone':
        return (<Image style={styles.networkLogo}
          source={require(`../../static/images/Ufone_Logo.png`)}
        />
        )
      case 'zong':
        return (<Image style={styles.networkLogo}
          source={require(`../../static/images/Zong_Logo.png`)}
        />
        )
      case 'telenor':
        return (<Image style={styles.networkLogo}
          source={require(`../../static/images/Telenor_Logo.png`)}
        />
        )
      case 'warid':
        return (<Image style={styles.networkLogo}
          source={require(`../../static/images/Warid_Logo.png`)}
        />
        )
      case 'jazz':
        return (<Image style={styles.networkLogo}
          source={require(`../../static/images/Jazz_Logo.png`)}
        />)
    }
  }

  render() {
    var { tenderAmount, billAmount, inputNum, network, phoneNum } = this.props;
    var { customer } = this.state;
    tenderAmount = tenderAmount ? parseInt(tenderAmount) : 0;
    return (
      <View style={styles.container}>
        <View
          style={{
            marginTop: 0,
            paddingBottom: 0,
            marginBottom: 0,
            height: "100%",
            flexDirection: "column",
            justifyContent: "flex-end"
          }}
        >
          {this.renderContainer()}
          {this.renderFooter()}
        </View>
        {this.state.cashPayment ? <PayCashModal dispatch={this.props.dispatch} tenderAmount={inputNum}
          billAmount={this.props.rechargeAmount}
          isOpen={this.state.cashPayment} onHideModal={this.onHideModal} />
          : null}
      </View>
    );
  }
}

const styles = {
  container: {
    width: "100%",
    display: "flex",
  },
  contentViewRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 18,
    paddingBottom: 18,
    marginRight: 15,
    marginLeft: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#f3f3f7"
  },
  contentViewNetwork: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: -20,
    // borderWidth:1,
    height:50,  
    // paddingBottom: 10,
    marginRight: 15,
    marginLeft: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#f3f3f7"
  },

  contentContainer: {
    height: "65%",
  },
  Duetext: {
    color: "#222222"
  },
  Dueamount: {
    color: "#222222"
  },
  tendertExt: {
    color: "#00aeef"
  },
  tenderAmount: {
    color: "#00aeef"
  },
  changetExt: {
    color: "#91d625"
  },
  changeAmount: {
    color: "#91d625"
  },
  orderButton: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",

  },

  listItmMain: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderTopColor: "#e7e7ef",
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: -10
  },

  footer: {
    paddingTop: 0,
    height: "32%"
  },
  footerContinueShopping: {
    paddingTop: 0,
    height: "32%"
  },
  footerTxt: {
    height: "40%",
    // paddingTop: 10
  },
  footerInertext: {
    color: "#555556",
    fontSize: 20
  },
  footerBtn: {
    width: "100%",
    justifyContent: "center",
    height: "50%",
    marginTop: 20,
    borderRadius: 0
  },
  footerBtnText: {
    color: "white",
    fontSize: 30,
    fontWeight: "400",
    fontFamily: "Roboto"
  },
  cntshpngMain: {
    marginTop: -70,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  continueBtn: {
    width: "65%",
    alignItems: 'center',
    justifyContent: "center",
    height: "80%",
    paddingTop: 15,
    paddingBottom: 15,
    borderRadius: 30,
    borderWidth: 0,
  },
  continueShoppingtxt: {
    color: "white",
    fontSize: 24,
    fontWeight: "400",
    fontFamily: "Roboto",
    marginLeft: 10,
    marginTop: -5,
  },
  footerBtnicon: {
    color: "#fff",
    fontSize: 45,
    marginTop: 5
  },
  networkLogo:{
    width:120,
    height:120
  }
};


export default PaymentStatus;