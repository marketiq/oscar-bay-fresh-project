import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  ScrollView,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight
} from "react-native";
import { Card } from "react-native-elements";
import {
  Container,
  Item,
  Input,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Icon
} from "native-base";
// import { APP_BACKGROUND_COLOR } from '../../constants/constants';
import { Grid, Row, Col } from "react-native-easy-grid";
// import Icon from "react-native-vector-icons/dist/FontAwesome";
import { NavigationActions } from "react-navigation";
// import PayCashModal from './PayCashModal';
import CustomerWindow from '../PaymentWindow/CustomerWindow'

class PhoneNumHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: [],
      query: "",
      data: [],
      inputText: "",
      finalizeModal: false,
      addCustomer: false,
      customer: {},
    };
    // this.hideModal = this.hideModal.bind(this);
    this.onHideModal = this.onHideModal.bind(this);
    this.addCustomer = this.addCustomer.bind(this);
    this.renderHeading = this.renderHeading.bind(this);
    this.back = this.back.bind(this);


  }
  // hideModal() {
  //   this.setState({
  //     finalizeModal: false
  //   });
  // }
  renderHeading() {
    switch (this.props.activeView) {

      case "rechargeView":
        return (
          <Title style={styles.headerBodytext}>ENTER YOUR RECHARGE AMOUNT</Title>
        );
      case "phoneView":
        return (
          <Title style={styles.headerBodytext}>ADD PHONE NUMBER</Title>
        );
      case "tenderView":
        return (
          <Title style={styles.headerBodytext}>ENTER TENDER AMOUNT</Title>
        );



    }
  }
  addCustomer(data) {
    this.setState({
      customer: JSON.parse(JSON.stringify(data))
    });
  }
  onHideModal(key) {
    this.setState(
      {
        [key]: false
      },
      () => {
        // this.props.dispatch(emptyCart());
        // this.props.dispatch(
        //   NavigationActions.navigate({ routeName: "Dashboard" })
        // );
      }
    );
  }
  // showModal() {
  //   this.setState({
  //     finalizeModal: true
  //   });
  // }
  validate() {
    if (
      this.state.inputText !== "" &&
      this.props.tenderAmount >= this.props.billAmount
    ) {
      // this.showModal();
    }
  }
  inputText(ev) {
    this.setState({
      inputText: ev.nativeEvent.text
    });
  }
  back() {
    this.props.back()
    // switch (this.props.activeView) {

    //   case "rechargeView":
    //     return (
    //       <Title style={styles.headerBodytext}>ENTER YOUR RECHARGE AMOUNT</Title>
    //     );
    //   case "phoneView":
    //     return (
    //       <Title style={styles.headerBodytext}>ADD PHONE NUMBER</Title>
    //     );
    //   case "tenderView":
    //     return (
    //       <Title style={styles.headerBodytext}>ENTER TENDER AMOUNT</Title>
    //     );



    // }
  }
  render() {
    let { customer } = this.state;
    return (
      <Container style={styles.mainCont} noShadow>
        <Header style={styles.headerMain} noShadow>
          <Left style={styles.headerLeft}>
            <Button
              transparent
              style={styles.backBtn}
              onPress={() => {
                this.back()
                // if (this.props.rechargeView) {
                //   this.props.toggleRecharge(false);

                // } else {
                //   this.props.dispatch(NavigationActions.back());
                // }
              }}
            >
              <Icon style={styles.backiconFirst} name="ios-arrow-back" />
              <Text style={styles.backiconText}>Back</Text>
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.headerBodytext}>
              {this.renderHeading()}
            </Title>

            <View style={styles.addCustomerView}>
              {
                (JSON.stringify(customer) !== '{}') ?
                  <TouchableHighlight style={styles.addCustomerBtn}
                    onPress={() => { this.setState({ addCustomer: true }) }}
                    underlayColor="lightgray" activeOpacity={0.2}>
                    <Text>
                      {customer.name}
                    </Text>
                  </TouchableHighlight>
                  :
                  <TouchableHighlight
                    style={styles.addCustomerBtn}
                    onPress={() => {
                      this.setState({ addCustomer: true });
                    }}
                    underlayColor="lightgray"
                    activeOpacity={0.2}
                  >
                    <Icon name="md-person-add" />
                  </TouchableHighlight>
              }
            </View>
          </Body>
        </Header>

        {this.state.addCustomer ?
          <CustomerWindow
            currentCustomer={this.state.customer}
            addCustomer={this.addCustomer}
            isOpen={this.state.addCustomer}
            onHideModal={this.onHideModal} />
          : null}
      </Container>
    );
  }
}

const styles = {
  mainCont: {
    height: "100%"
  },
  headerContainer: {
    height: "100%",
    borderLeftWidth: 1,
    borderColor: "#e7e7ef"
  },
  addCustomerView: {
    borderLeftWidth: 1,
    width: 90,
    borderColor: "#e7e7ef",
    // flexDirection: "row",
    // width:'100%'
    // height: "10%"
  },
  addCustomerBtn: {
    width: 90,
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  headerMain: {
    backgroundColor: "#fff",
    // paddingTop: 8,
    // paddingBottom: 10,
    paddingRight: 0,
    paddingLeft: 20,
    height: "100%"
  },
  headerLeft: {
    width: "20%",
    marginLeft: "-6%",
    display: "flex",
    flexDirection: "row"
  },
  headerBody: {
    width: "40%",
    display: "flex",
    marginLeft: "12%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  headerBodytext: {
    textAlign: "center",
    color: "#262626",
    fontSize: 22,
    fontFamily: "Roboto",
    fontWeight: "500",
    marginLeft: "-15%"
  },

  backiconFirst: {
    color: "#575757",
    fontWeight: "500",
    marginRight: 8
  },
  backiconText: {
    color: "#575757",
    marginRight: 20,
    marginLeft: 5,
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "400",
    borderRadius: 3
  },
  backBtn: {
    width: "80%",
    borderWidth: 0,
    marginLeft: "-8%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  carNumbermain: {
    width: "65%",
    marginRight: 20,
    borderColor: "#bcbcbc",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    height: 45,
    borderRadius: 3,
    paddingLeft: 10,
    paddingRight: 10
  },
  frwrdBtn: {
    width: "35%",
    borderColor: "#bcbcbc",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 3,
    marginRight: -2
  },
  frwrdiconFirst: {
    color: "#555556"
  },
  frwrdiconText: {
    color: "#555556",
    marginRight: 5,
    marginLeft: 20,
    fontFamily: "Roboto",
    fontSize: 18
  },
  carIcon: {
    color: "#b270eb"
  }
};
export default PhoneNumHeader;
