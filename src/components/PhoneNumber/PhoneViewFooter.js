import React from "react";
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Input, Image, Dimensions
} from "react-native";
import { Icon, Button, Item } from "native-base";
var { height, width } = Dimensions.get('window')

class PhoneViewFooter extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        var { styles, onChangeView, toggleRecharge, inputNum } = this.props;
        return (
            <View style={styles.footer}>
                <View style={styles.footerTxt}>
                    <View style={styles.listItmMain}>
                        <Text style={styles.footerInertext}>
                            Please proceed to recharge
            </Text>
                    </View>
                </View>
                <View style={styles.orderButton}>
                    <Button
                        style={{
                            ...styles.footerBtn,
                            backgroundColor:
                            (inputNum.length === 11 && this.props.network) ? "#d7df23" : "lightgray"
                        }}
                        disabled={(inputNum.length === 11 && this.props.network) ? false : true}

                        onPress={() => onChangeView('rechargeView')}
                    >
                        <Text style={styles.footerBtnText}>Next</Text>
                        <Icon name="ios-arrow-forward" style={styles.footerBtnicon} />
                    </Button>
                </View>
            </View>
        )
    }

}


export default PhoneViewFooter;