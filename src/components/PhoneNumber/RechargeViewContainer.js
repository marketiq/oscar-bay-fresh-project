import React from "react";
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Input, Image, Dimensions
} from "react-native";
import { Icon, Button, Item } from "native-base";

// import PayCashModal from './PayCashModal';
// import PayCardModal from './PayCardModal';
// import { NavigationActions } from "react-navigation";
// import { emptyCart } from "../../actions/";
// import PayCashModal from './PayCashModal';

var { height, width } = Dimensions.get('window')
class RechargeViewContainer extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        var { styles, renderImageOfNetwork,network,phoneNum } = this.props;
        return (
            <View style={styles.contentContainer}>
            <View style={styles.contentViewNetwork}>
              <Text style={styles.Duetext}>Network</Text>
              <Text style={styles.Dueamount}>
                {renderImageOfNetwork(network)}
              </Text>
            </View>
            <View style={styles.contentViewRow}>
              <Text style={styles.Duetext}>Phone</Text>
              <Text style={styles.Dueamount}>
                {phoneNum}
              </Text>
            </View>
          </View>
        )
    }

}


export default RechargeViewContainer;