import React from "react";
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Input, Image, Dimensions
} from "react-native";
import { Icon, Button, Item } from "native-base";

var { height, width } = Dimensions.get('window')

class RechargeViewFooter extends React.Component {
    constructor(props) {
        super(props);
        this.changeView = this.changeView.bind(this);
    }
    changeView(){
        this.props.onChangeView('tenderView'); 
    }
    render() {
        var { styles, onChangeView, toggleRecharge, inputNum, onAddRechargeToCart, cart } = this.props;
        return (
            <View style={styles.footer}>
                {cart === 0 ?
                    <View style={styles.cntshpngMain}>
                        <Button transparent style={{
                            ...styles.continueBtn,
                            backgroundColor:parseInt(inputNum)>=20  ? "#b968ee" : "lightgray"
                        }}
                        disabled={parseInt(inputNum)>=20 ? false : true}
                        onPress={onAddRechargeToCart}

                        >
                            <Image source={require(`../../static/images/shopping_icon.png`)} />
                            <Text style={styles.continueShoppingtxt}>Continue Shopping</Text>

                        </Button>
                    </View>
                    : null}

                <View style={styles.footerTxt}>
                    <View style={styles.listItmMain}>
                        <Text style={styles.footerInertext}>
                            Please proceed to recharge
              </Text>
                    </View>
                </View>
                <View style={styles.orderButton}>
                    {cart === 0 ?

                        <Button
                            style={{
                                ...styles.footerBtn,

                                backgroundColor:
                                parseInt(inputNum)>=20 ? "#d7df23" : "lightgray"
                            }}
                            disabled={parseInt(inputNum)>=20 ? false : true}

                            onPress={this.changeView}
                        >
                            <Text style={styles.footerBtnText}>Next</Text>
                            <Icon name="ios-arrow-forward" style={styles.footerBtnicon} />
                        </Button>
                        :
                        <Button
                            style={{
                                ...styles.footerBtn,

                                backgroundColor:
                                parseInt(inputNum)>=20  ? "#d7df23" : "lightgray"
                            }}
                            disabled={parseInt(inputNum)>=20  ? false : true}

                            onPress={onAddRechargeToCart}
                        >
                            <Text style={styles.footerBtnText}>Continue to Shopping</Text>
                            <Icon name="ios-arrow-forward" style={styles.footerBtnicon} />
                        </Button>
                    }
                </View>
            </View>
        )
    }

}


export default RechargeViewFooter;