import React from 'react';
import { View, Text } from 'react-native';
import { Button, Icon } from 'native-base';

class TenderCalculator extends React.Component {
    constructor(props) {
        super(props);
        this.onPressButton = this.onPressButton.bind(this);
        

    }
    onPressButton(value) {
        this.props.onChange(value)
    }
   
    render() {
        return (
            <View style={styles.rowStylemain}>
                <View style={styles.rowStyle}>
                    <Button transparent style={styles.inputButton} onPress={() => this.onPressButton(1)}>
                        <Text style={styles.btnText}>1</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(2)} transparent style={styles.inputButton}>
                        <Text style={styles.btnText}>2</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(3)} transparent style={{ ...styles.inputButton, ...styles.borderRight }}>
                        <Text style={styles.btnText}>3</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(50)} transparent style={{ ...styles.inputButton, ...styles.borderRight, ...styles.calcuRight }}>
                        <Text style={styles.btnTextlast}>Rs. 50</Text>
                    </Button>
                </View>

                <View style={styles.rowStyle}>
                    <Button onPress={() => this.onPressButton(4)} transparent style={styles.inputButton}>
                        <Text style={styles.btnText}>4</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(5)} transparent style={styles.inputButton}>
                        <Text style={styles.btnText}>5</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(6)} transparent style={{ ...styles.inputButton, ...styles.borderRight }}>
                        <Text style={styles.btnText}>6</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(100)} transparent style={{ ...styles.inputButton, ...styles.borderRight, ...styles.calcuRight }}>
                        <Text style={styles.btnTextlast}>Rs. 100</Text>
                    </Button>
                </View>
                <View style={styles.rowStyle}>
                    <Button onPress={() => this.onPressButton(7)} transparent style={styles.inputButton}>
                        <Text style={styles.btnText}>7</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(8)} transparent style={styles.inputButton}>
                        <Text style={styles.btnText}>8</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(9)} transparent style={{ ...styles.inputButton, ...styles.borderRight }}>
                        <Text style={styles.btnText}>9</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(500)} transparent style={{ ...styles.inputButton, ...styles.borderRight, ...styles.calcuRight }}>
                        <Text style={styles.btnTextlast}>Rs. 500</Text>
                    </Button>
                </View>
                <View style={styles.rowStyle}>
                    <Button onPress={() => this.onPressButton('c')} transparent style={{ ...styles.inputButton, ...styles.borderBottom }}>
                        <Text style={styles.btnText}>C</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton(0)} transparent style={{ ...styles.inputButton, ...styles.borderBottom }}>
                        <Text style={styles.btnText}>0</Text>
                    </Button>
                    <Button onPress={() => this.onPressButton('e')} transparent style={{ ...styles.inputButton, ...styles.borderBottom, ...styles.borderRight }}>
                        <Icon style={styles.iconText} name="ios-backspace-outline" />

                    </Button>
                    <Button
                        // onPress={() => this.setRechargeAmt(true)}
                        onPress={() => this.onPressButton(1000)}

                        // disabled={!this.props.inputNum || this.props.rechargeAmt ? true : false}
                        transparent style={{ ...styles.inputButton, ...styles.borderRight, ...styles.borderBottom, ...styles.calcuRight }}>
                        <Text style={styles.btnTextlast}>Rs. 1000</Text>

                        {/* <Icon style={{ ...styles.iconText, color: !this.props.inputNum || this.props.rechargeAmt ? 'lightgray' : null }} name="md-checkmark" /> */}
                    </Button>
                </View>
            </View>
        );
    }
}

const styles = {
    rowStylemain: {
        height: '100%',
        paddingTop: 30,
        paddingBottom: 30,
        paddingLeft: 30,
        paddingRight: 30
    },
    rowStyle: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        height: '25%'
    },
    inputButton: {
        width: '24%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        borderWidth: 1,
        borderRightWidth: 0,
        borderBottomWidth: 0,
        borderColor: '#e7e7ef'
    },
    btnText: {
        fontSize: 20,
        color: '#555556',
        fontFamily: 'Roboto',
        fontWeight: '500'
    },
    btnTextlast: {
        fontSize: 20,
        color: '#555556',
        fontFamily: 'Roboto',
        fontWeight: '400'
    },
    iconText: {
        fontSize: 35,
        color: '#555556',
        fontFamily: 'Roboto',
        fontWeight: '400'
    },
    calcuRight: {
        marginLeft: '4%',
        backgroundColor: '#faf9fd',
    },
    borderRight: {
        borderRightWidth: 1,
        borderColor: '#e7e7ef'
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderColor: '#e7e7ef'
    }
}

export default TenderCalculator;