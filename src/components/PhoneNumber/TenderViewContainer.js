import React from "react";
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Input, Image, Dimensions
} from "react-native";
import { Icon, Button, Item } from "native-base";

var { height, width } = Dimensions.get('window')
class TenderViewContainer extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        var { styles, renderImageOfNetwork, network, phoneNum, rechargeAmount,inputNum } = this.props;
        return (
            <View style={styles.contentContainer}>
                <View style={styles.contentViewNetwork}>
                    <Text style={styles.Duetext}>Network</Text>
                    <Text style={styles.Dueamount}>
                        {renderImageOfNetwork(network)}
                    </Text>
                </View>
                <View style={styles.contentViewRow}>
                    <Text style={styles.Duetext}>Phone</Text>
                    <Text style={styles.Dueamount}>
                        {phoneNum}
                    </Text>
                </View>
                <View style={styles.contentViewRow}>
                    <Text style={styles.Duetext}>Amount Recharged</Text>
                    <Text style={styles.Dueamount}>
                        Rs. {rechargeAmount}
                    </Text>
                </View>
                {inputNum ?
                    <View>
                        <View style={styles.contentViewRow}>
                            <Text style={styles.tendertExt}>Tendered</Text>
                            <Text style={styles.tenderAmount}>Rs. {inputNum}</Text>
                        </View>
                        <View style={styles.contentViewRow}>
                            <Text style={styles.changetExt}>Change</Text>
                            <Text style={styles.changeAmount}>Rs. {(parseInt(inputNum) - parseInt(rechargeAmount) < 0) ? 0 : parseInt(inputNum) - parseInt(rechargeAmount)}</Text>
                        </View>

                    </View> : null}
            </View>
        )
    }

}


export default TenderViewContainer;