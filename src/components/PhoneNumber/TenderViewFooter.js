import React from "react";
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Input, Image, Dimensions
} from "react-native";
import { Icon, Button, Item } from "native-base";

// import PayCashModal from './PayCashModal';
// import PayCardModal from './PayCardModal';
// import { NavigationActions } from "react-navigation";
// import { emptyCart } from "../../actions/";
// import PayCashModal from './PayCashModal';

var { height, width } = Dimensions.get('window')
class TenderViewFooter extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        var { styles, onChangeView, toggleRecharge, inputNum, onAddRechargeToCart, showModal,cart, rechargeAmount } = this.props;
        return (
            <View style={styles.footer}>
                <View style={styles.footerTxt}>
                    <View style={styles.listItmMain}>
                        <Text style={styles.footerInertext}>
                            Please proceed to recharge
          </Text>
                    </View>
                </View>
                <View style={styles.orderButton}>
                    <Button
                        style={{
                            ...styles.footerBtn,
                            backgroundColor:
                                parseInt(inputNum) >= parseInt(rechargeAmount) ? "#d7df23" : "lightgray"
                        }}
                        disabled={parseInt(inputNum) >= parseInt(rechargeAmount) ? false : true}

                        onPress={() => showModal()}
                    >
                        <Text style={styles.footerBtnText}>Cash</Text>
                        <Icon name="md-cash" style={styles.footerBtnicon} />
                    </Button>
                </View>
            </View>
        )
    }

}


export default TenderViewFooter;