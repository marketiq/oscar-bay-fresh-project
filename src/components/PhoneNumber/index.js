import React, { Component } from "react";
import { connect } from "react-redux";
import { Text, ScrollView, View, Image, TouchableOpacity,Dimensions } from "react-native";
import { Card } from "react-native-elements";
import { Icon, Item, Input, Button } from "native-base";
import { APP_BACKGROUND_COLOR } from "../../constants/constants";
import { Grid, Row, Col } from "react-native-easy-grid";
import PhoneNumHeader from "./PhoneNumHeader";
import CalculatorContainer from "./CalculatorContainer";
import DisplayWindow from "./DisplayWindow";
import PaymentStatus from "./PaymentStatus";
import RechargeCalculator from "./RechargeCalculator";
import TenderCalculator from "./TenderCalculator";
import { onCartAddProduct } from "../../actions";
import { NavigationActions } from "react-navigation";
import MobileRechargeModal from './MobileRechargeModal';

const {height,width} = Dimensions.get('window');
class PhoneNumber extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: [],
      query: "",
      data: [],
      vehicleNum: "",
      tenderAmount: "",
      inputNum: "",
      network:'',
      phoneNum: "",
      rechargeView: false,
      rechargeAmt: false,
      tenderView: false,
      amount: "",
      rechargeAmount: "",
      mobileRechargeModalOpen:true,
      activeView: "phoneView"
    };

    this.onChangeNum = this.onChangeNum.bind(this);
    this.toggleRechargeView = this.toggleRechargeView.bind(this);
    this.onAddRechargeToCart = this.onAddRechargeToCart.bind(this);
    this.setRechargeAmt = this.setRechargeAmt.bind(this);
    this.toggleTenderView = this.toggleTenderView.bind(this);
    this.onChangeView = this.onChangeView.bind(this);
    this.renderCalculatorView = this.renderCalculatorView.bind(this);
    this.onHideRechargeModal = this.onHideRechargeModal.bind(this);
    this.setNetwork = this.setNetwork.bind(this);
    this.back = this.back.bind(this);
  }

  onHideRechargeModal() {
    this.setState({ mobileRechargeModalOpen: false })
}
setNetwork(network) {
  this.setState({
      network
  }, () => {
      this.onHideRechargeModal();
  })
}
  back() {
    switch (this.state.activeView) {
      case "phoneView":
        this.props.dispatch(
          NavigationActions.navigate({ routeName: "Dashboard" })
        );
        break;

      case "rechargeView":
        this.setState({
          activeView: "phoneView",

          inputNum: this.state.phoneNum
        });
        break;
      case "tenderView":
        this.setState({
          activeView: "rechargeView",

          inputNum: this.state.rechargeAmount
        });
        break;
    }
  }

  renderCalculatorView() {
    switch (this.state.activeView) {
      case "phoneView":
        return <CalculatorContainer onChange={this.onChangeNum} />;

      case "rechargeView":
        return <RechargeCalculator onChange={this.onChangeNum} />;

      case "tenderView":
        return <TenderCalculator onChange={this.onChangeNum} />;
        break;
    }
  }
  onChangeNum(value) {
    let { inputNum, tenderAmount, rechargeAmount, activeView } = this.state;
    if (activeView !== "phoneView") {
      if (!inputNum && (value === 0 || value === "00")) {
        return;
      }
    }
    if(activeView ==='phoneView'){
      if(!inputNum && value!==0){
        return;
      }else if(inputNum.length === 1){
        if(!isNaN(value) && value!==3 )
        return;
      }
    }
    if (value > 9) {
      let temp = inputNum ? parseInt(inputNum) : 0;
      temp += value;
      this.setState({
        inputNum: temp.toString()
      });
      return;
    }
    switch (value) {
      case "c":
        this.setState({ inputNum: "" });
        break;

      case "e":
        let temp = inputNum.slice(0, inputNum.length - 1);
        this.setState({ inputNum: temp });
        break;
      default:
        if (inputNum.length >= 11) return;
        this.setState({
          inputNum: "" + inputNum + value
        });
        break;
    }
  }
  onChangeView(activeView) {
    switch (activeView) {
      case "rechargeView":
        this.setState({
          activeView,
          phoneNum: this.state.inputNum,
          inputNum: ""
        });
        break;
      case "tenderView":
        this.setState({
          activeView,
          rechargeAmount: this.state.inputNum,
          inputNum: ""
        });
        break;
    }
  }

  onAddRechargeToCart() {
    let { dispatch } = this.props;
    let network = this.state.network
    // this.props.navigation.state.params.network || "";
    let data = {
      product_id: Math.floor(Math.random()*100) + 1,
      price_unit: parseInt(this.state.inputNum),
      display_name: network
        ? network.charAt(0).toUpperCase() + network.slice(1) + " " + "Recharge"
        : "",
      qty: 1
    };
    dispatch(onCartAddProduct(data));
    dispatch(NavigationActions.navigate({ routeName: "Dashboard" }));
  }

  toggleRechargeView(rechargeView) {
    this.setState({
      rechargeView,
      phoneNum: this.state.inputNum,
      inputNum: ""
    });
  }
  toggleTenderView(tenderView) {
    this.setState({
      tenderView,
      amount: this.state.inputNum,
      rechargeView: false,
      inputNum: ""
    });
  }

  setRechargeAmt(value) {
    this.setState({ rechargeAmt: value });
  }
  render() {
    billAmount = 100;
    console.ignoredYellowBox = ["Remote debugger"];
    return (
      <View style={{height:height-95}}>
      <Grid>
        <Row size={10} style={styles.headerBorder}>
          <PhoneNumHeader
            activeView={this.state.activeView}
            toggleRecharge={this.toggleRechargeView}
            rechargeView={this.state.rechargeView}
            back={this.back}
            dispatch={this.props.dispatch}
          />
        </Row>

        <Row size={90} style={{}}>
          <Col size={5} style={styles.rightSide}>
            <Row size={35} style={styles.billAmountmain}>
              <DisplayWindow
                network ={this.state.network}
                activeView={this.state.activeView}
                rechargeView={this.state.rechargeView}
                inputNum={this.state.inputNum}
                tenderAmount={this.state.tenderAmount}
                billAmount={billAmount}
                phoneNum={this.state.phoneNum}
              />
            </Row>
            <Row size={65} style={styles.calculationMain}>
            {
              this.renderCalculatorView()              
            }
            </Row>
          </Col>

          <Col size={3} style={styles.leftSide}>
            <PaymentStatus
              activeView={this.state.activeView}
              setView={this.setView}
              network={this.state.network}
              // network={this.props.navigation.state.params.network}
              rechargeView={this.state.rechargeView}
              onAddRechargeToCart={this.onAddRechargeToCart}
              dispatch={this.props.dispatch}
              tenderAmount={this.state.tenderAmount}
              toggleRecharge={this.toggleRechargeView}
              toggleTenderView={this.toggleTenderView}
              billAmount={billAmount}
              inputNum={this.state.inputNum}
              phoneNum={this.state.phoneNum}
              rechargeAmt={this.state.rechargeAmt}
              onChangeView={this.onChangeView}
              rechargeAmount={this.state.rechargeAmount}
              cart={this.props.cart}
            />
          </Col>
        </Row>
        {this.state.mobileRechargeModalOpen ? (
          <MobileRechargeModal
            setNetwork = {this.setNetwork}
            mobileRechargeModalOpen={this.state.mobileRechargeModalOpen}
            onHideRechargeModal={this.onHideRechargeModal}
            dispatch={this.props.dispatch}
          />
        ) : null}
      </Grid>
      </View>
    );
  }
}

const styles = {
  mainRow: {
    flexDirection: "row",
    flex: 1
  },
  button: {
    width: "90%",
    marginLeft: "3%",
    marginLeft: "3%"
  },
  headerBorder: {
    borderBottomWidth: 1,
    borderBottomColor: "#e7e7ef"
  },
  billAmountmain: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    borderBottomColor: "#e7e7ef",
    borderStyle: "dotted"
  },

  calculationMain: {
    borderWidth: 0,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderColor: "#e7e7ef",
    height: "100%"
  },
  leftSide: {
    // backgroundColor: '#fafbfb', paddingTop: 15, paddingBottom: 15,
    backgroundColor: "#fff",
    borderLeftWidth: 1,
    borderLeftColor: "#e7e7ef"
  },
  rightSide: {
    backgroundColor: "#fff",
    borderLeftWidth: 1,
    borderLeftColor: "#e7e7ef"
  },
  productSearch: {
    backgroundColor: "#fff",
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 0,
    marginLeft: 15,
    marginRight: 15
  },
  tabSty: {
    color: "#af2d2d"
  }
};
const mapStateToProps = state => {
  // return getSelectorProducts(state)
  return { listProducts: state.product_reducer, cart: state.cart_reducer };
  // return getSelectorProducts(state)
};
// const mapStateToProps = state => {     return getSelectorAnalytics(state); }

export default connect(mapStateToProps)(PhoneNumber);
