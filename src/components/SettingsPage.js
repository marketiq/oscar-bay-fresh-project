import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { AuthActions, AuthSelectors } from '../modules/auth';
import { Button } from 'react-native-elements';

const { authOnLogout } = AuthActions;

class SettingsPage extends Component {

    componentWillMount() {
        this.props.authOnLogout();
    }

    onLogout() {
        this.props.authOnLogout();
    }

    render() {
        return (
            <View>
                <Button
                    onPress={this.onLogout.bind(this)}
                    title='LOGOUT'
                />
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {};
}

export default connect(mapStateToProps, { authOnLogout })(SettingsPage);
