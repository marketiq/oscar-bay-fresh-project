import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, Animated, Easing } from 'react-native';
import { WHITE_COLOR } from '../constants/constants';
import { ProfileActions } from '../modules/profile';
import { ConfigActions } from '../modules/config';
import { ImageBackgroundContainer } from './common'

const { loadInitialProfileState } = ProfileActions;
const { loadInitialConfigState } = ConfigActions;

class Splash extends Component {

	constructor(){
        super();
        this.state = { spinValue: new Animated.Value(0) };
    }

	componentWillMount() {
		// this.props.loadInitialConfigState();
		this.props.loadInitialProfileState();
	}

	componentDidMount() {

        Animated.loop(
		    Animated.timing(
		        this.state.spinValue,
		        {
		            toValue: 1,
		            duration: 3000,
		            easing: Easing.linear,
		            useNativeDriver: true
		        }
            ) 
		).start();
	}



                //     <Animated.View style={{transform: [{rotate: spin}] }}>
                //     <Image 
                //         style={styles.image}
                //         source={require('../static/images/loader.png')}
                //         resizeMode={Image.resizeMode.contain}
                //     />
                // </Animated.View>
                // <Text style={styles.text}>loading...</Text>

	render() {
        const spin = this.state.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })

		return (
			<ImageBackgroundContainer customStyle={{alignItems: 'center'}}>
            </ImageBackgroundContainer>
		)
	}
}


const styles = {
	image: {
		height: 140,

	},
	text: {
        color: WHITE_COLOR,
        fontSize: 25,
        fontWeight: '300',
        backgroundColor: 'transparent',
        paddingTop: 20
	}
};

const mapStateToProps = state => {
	return {};
};

export default connect(mapStateToProps, { loadInitialProfileState, loadInitialConfigState })(Splash);
