import React from 'react';
import {View,Text  } from 'react-native';

class CartListItem extends React.Component {
    render(){
      
        let {item} = this.props;
        return (
            <View style={{height: item.count<=1?55:75 }} style={styles.mainContainer}>
            <View style={styles.totalView}>
                <Text style={styles.totalleft}>
                    {item.display_name}
                </Text>
                <Text style={styles.totalright}>
                   Rs. {(item.price_unit*item.qty).toFixed(2)}
                </Text>
            </View>
            <View style={styles.priceView}>
                <Text style={{...styles.priceText}}>
                    {item.qty>1?item.qty + ' x ' + 'Rs.'+item.price_unit:null}
                </Text>
            </View>
        </View>
        );
    }
}


const styles = {
    mainContainer:{
        paddingBottom:5,
        borderBottomWidth:1,
        borderColor:'#e7e7ef',
        marginRight: 15,
        marginLeft: 15, 
    },
    totalView:{
        flexDirection: 'row',
        justifyContent:'space-between',
        paddingTop: 15,
        paddingBottom: 10,
        marginBottom: -10,
    },
    totalright: {
        textAlign: 'right',
        flex: 1,
        fontSize: 16,
        fontFamily: 'Roboto',
       fontWeight: '300',
       color: '#222',
    },
    totalleft: {
        textAlign: 'left',
        flex: 1,
        fontFamily: 'Roboto',
       fontWeight: '300',
       color: '#222',
       fontSize: 16,
    },
    priceView:{
        flexDirection: 'row',
    },
    priceText: {
        textAlign: 'left',
        fontFamily: 'Roboto',
       fontWeight: '300',
       color: '#888787',
       paddingBottom: 5,
       fontSize: 14,
    },
}

export default CartListItem;
