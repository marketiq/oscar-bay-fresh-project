import React from "react";
import { Modal, View, Text, TouchableOpacity,TouchableNativeFeedback,Platform } from "react-native";
import { Button, Icon } from "native-base";
import { onCartAddProduct, onCartRemoveProduct } from "../../../actions/";
import { connect } from "react-redux";

const Touchable = (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;

class QuantityUpdateModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: this.props.product
    };
    this.onProductInc = this.onProductInc.bind(this);
    this.onProductDec = this.onProductDec.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
  }

  handleRemove(id) {
    this.props.onRemoveProduct(id);
    this.props.onHideModal();
  }
  onProductInc() {
    this.setState({
      product: {
      qty:this.state.product.qty++,
        ...this.state.product
      }})
    var prodInc=this.props.onAddProductToCart(this.state.product);
    prodInc();
  }
  onProductDec(id) {
    if (this.state.product.qty > 1) {
      this.props.onCartRemoveProduct(id, false);
    } else {
      this.props.onRemoveProduct(id);
    }
    this.setState({
      product: {
        qty: this.state.product.qty--,
        ...this.state.product
      }
    });
  }

  render() {
    let { product } = this.state;
    return (
      <Modal
        transparent={true}
        animationType="fade"
        visible={this.props.isOpen}
        onRequestClose={() => this.props.onHideModal()}
        onPress={() => this.props.onHideModal()}
      >
        <View style={styles.container}>
          <View style={styles.modalInner}>
            <View style={styles.modalHeader}>
              <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>
              <Touchable
                onPress={() => this.props.onHideModal()}>
                <View style={styles.closeButton}> 
                    <Icon style={styles.closeButtonText} name="md-close" />
                </View>
              </Touchable>
              <Text style={styles.headerText}>{product.display_name}</Text>
              </View>
              <Text style={styles.headerPriceText}>
                Rs. {(product.price_unit * product.qty).toFixed(2)}
              </Text>
            </View>
            <View style={styles.addQuantity}>
              <Text style={styles.addQuantityinner}>UPDATE QUANTITY</Text>
            </View>
            <View style={styles.modalContent}>
              <View
                style={{
                  width: "10%",
                  height: 50,
                  backgroundColor: "lightgray",
                  borderTopWidth: 1,
                  borderBottomWidth: 1,
                  borderLeftWidth: 1,
                  borderColor: "#919191"
                }}
              >
                <TouchableOpacity
                  disabled={product.qty <= 0 ? true : false}
                  onPress={() => {
                    this.onProductDec(product.product_id);
                  }}
                >
                  <Text style={styles.text}>-</Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: "60%",
                  height: 50,
                  backgroundColor: "white",
                  borderWidth: 1,
                  borderColor: "#919191"
                }}
              >
                <Text style={styles.text}>{product.qty}</Text>
              </View>
              <View
                style={{
                  width: "10%",
                  height: 50,
                  backgroundColor: "lightgray",
                  borderTopWidth: 1,
                  borderBottomWidth: 1,
                  borderRightWidth: 1,
                  borderColor: "#919191"
                }}
              >
                <TouchableOpacity onPress={this.onProductInc}>
                  <Text style={styles.text}>+</Text>
                </TouchableOpacity>
              </View>
            </View>
            <Button
              onPress={() => this.props.onHideModal()}
              style={styles.removeButton}
            >
              <Text style={styles.removeText}>Okay</Text>
            </Button>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  container: {
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#00000098",
    width: "100%",
    height: "100%"
  },
  addQuantity: {
    justifyContent: "center",
    flexDirection: "row",
    marginBottom: 10,
    width: "75%",
    paddingTop: "10%"
  },
  addQuantityinner: {
    justifyContent: "center",
    flexDirection: "row",
    width: "100%",
    paddingLeft: "15%",
    fontFamily: "Roboto",
    fontWeight: "700",
    fontSize: 18,
    color: "#555556"
  },
  modalInner: {
    backgroundColor: "#fff",
    height: "100%",
    width: "70%"
  },
  modalHeader: {
    backgroundColor: "#978add",
    flexDirection: "row",
    alignItems:'center',
    justifyContent: "space-between",
    height: "10%",
    width: "100%"
  },
  modalContent: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: "center",
    flexDirection: "row",
    height: "50%"
  },
  image: {
    height: 140
  },
  closeButton: {
    width: 50,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    height:100
  },
  closeButtonText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold"
  },
  headerText: {
    color: "#fff",
    fontSize: 20,
    marginRight: 50,
    fontFamily: "Roboto",
    fontWeight: "100",
    fontSize: 20
  },
  headerPriceText: {
    color: "#fff",
    fontSize: 20,
    textAlign: "right",
    position: "relative",
    right: 10,
    width: "30%",
    fontFamily: "Roboto",
    fontWeight: "100",
    fontSize: 18
  },
  text: {
    color: "#555556",
    fontSize: 30,
    textAlign: "center",
    fontWeight: "300",
    backgroundColor: "transparent",
    alignItems: "center"
  },
  removeText: {
    color: "#555556",
    fontFamily: "HelveticaNeue"
  },
  removeButton: {
    alignSelf: "center",
    backgroundColor: "#fafbfc",
    marginBottom: 10,
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "#c2c7cc",
    width: "40%",
    paddingTop: 30,
    paddingBottom: 30,
    elevation: 1,
    justifyContent: "center",
    borderRadius: 5,
    height: 20
  }
};

export default connect(null, {
  onCartRemoveProduct
})(QuantityUpdateModal);
