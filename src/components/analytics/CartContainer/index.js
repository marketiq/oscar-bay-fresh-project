import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
  ListView,
  Picker,
  Dimensions
} from "react-native";
import { Row, Grid } from "react-native-easy-grid";
import Menu, { MenuItem, MenuDivider } from "react-native-material-menu";
import { Button, Icon, Badge, List, ListItem } from "native-base";
import QuantityUpdateModal from "./QuantityUpdateModal";
import Orderwindow from "../OrderWindow/";
import CartListItem from "./CartListItem";
import { onCartRemoveProduct, emptyCart, addTotal } from "../../../actions/";
import { NavigationActions } from "react-navigation";
var { height, width } = Dimensions.get("window");

class CartContainer extends Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      total: 0,
      isOpen: false,
      orderWindow: false,
      item: "",
      currentProduct: {}
    };

    this.onHideModal = this.onHideModal.bind(this);
    this.openQuantityModal = this.openQuantityModal.bind(this);

    this.onHideOrder = this.onHideOrder.bind(this);
    this.openorderWindow = this.openorderWindow.bind(this);
    this.onRemoveProduct = this.onRemoveProduct.bind(this);
    this.onChangePicker = this.onChangePicker.bind(this);
    this.getTotal = this.getTotal.bind(this);
    this.proceedOrder = this.proceedOrder.bind(this);
    this.getProductName = this.getProductName.bind(this);
  }
  getProductName(id) {
    let product = this.props.products.find(item => {
      return id === item.id;
    });
    return product;
  }

  onHideModal() {
    this.setState({
      isOpen: false
    });
  }
  openQuantityModal(item) {
    let name = this.getProductName(item.product_id)
      ? this.getProductName(item.product_id).display_name
      : null;
    let data = {
      ...item,
      display_name:name
    };
    this.setState({
      isOpen: true,
      currentProduct: JSON.parse(JSON.stringify(data))
    });
  }

  onRemoveProduct(id, secId, rowId, rowMap) {
    var { dispatch } = this.props;
    dispatch(onCartRemoveProduct(id, true));
    if (rowMap) {
      rowMap[`${secId}${rowId}`].props.closeRow();
    }
  }
  onHideOrder() {
    this.setState({
      orderWindow: false
    });
  }
  openorderWindow() {
    this.setState({
      orderWindow: true
    });
  }
  onChangePicker() {
    this.props.dispatch(emptyCart());
    this.hideMenu();
  }

  setMenuRef = ref => {
    this.menu = ref;
  };

  menu = null;

  hideMenu = () => {
    this.menu.hide();
  };

  showMenu = () => {
    this.menu.show();
  };

  getTotal() {
    let total = 0;
    this.props.cart
      ? this.props.cart.forEach(item => {
          total += item.price_unit * item.qty;
        })
      : null;
    return total;
  }
  proceedOrder() {
    this.props.dispatch(addTotal(this.getTotal()));
    this.props.dispatch(
      NavigationActions.navigate({ routeName: "PaymentWindow" })
    );
  }

  render() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    console.ignoredYellowBox = ["Remote debugger"];
    var getTotalCount = () => {
      let total = 0;
      this.props.cart
        ? this.props.cart.forEach(item => {
            total += item.qty;
          })
        : null;
      return total;
    };

    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: "row",
            height: "8%",
            maxHeight: "8%",
            minHeight: 50
          }}
        >
          <View
            style={{
              height: "100%",
              width: "85%",
              alignSelf: "flex-start",
              flexDirection: "row",
              borderWidth: 2,
              borderLeftWidth: 0,
              borderColor: "#e7e7ef",
              paddingRight: 15
            }}
          >
            <Menu
              ref={this.setMenuRef}
              style={{ marginTop: 55, width: 324 }}
              button={
                <TouchableOpacity
                  underlayColor="lightgray"
                  activeOpacity={0.3}
                  onPress={this.showMenu}
                >
                  <View style={styles.mainPicker}>
                    <Text style={styles.picketText}>
                      {this.props.cart.length < 1 ? "No Sale" : "Current Sale"}
                    </Text>
                    <Icon style={styles.pickerIcon} name="ios-arrow-down" />
                  </View>
                </TouchableOpacity>
              }
            >
              <MenuItem
                style={{ width: "100%" }}
                onPress={this.onChangePicker}
                disabled={this.props.cart.length < 1 ? true : false}
              >
                Clear Items
              </MenuItem>
            </Menu>
          </View>
          <View
            style={{
              borderWidth: 2,
              borderLeftWidth: 0,
              borderColor: "#e7e7ef",
              width: "15%"
            }}
          >
            <Badge primary style={styles.basketBadge}>
              <Text
                style={{ fontSize: 10, textAlign: "center", color: "#fff" }}
              >
                {getTotalCount()}
              </Text>
            </Badge>
            <Icon
              style={{
                color: "#000",
                marginTop: "30%",
                fontSize: 20,
                marginLeft: "30%"
              }}
              name="ios-basket"
            />
          </View>
        </View>
        <View style={{ height: height - 50 }}>
          <View style={styles.contentContainer}>
            <ScrollView
              ref={ref => (this.scrollView = ref)}
              onContentSizeChange={(contentWidth, contentHeight) => {
                this.scrollView.scrollToEnd({ animated: true });
              }}
            >
              <List
                dataSource={this.ds.cloneWithRows(this.props.cart)}
                renderRow={data => (
                  <TouchableOpacity
                    onPress={() => this.openQuantityModal(data)}
                  >
                    <CartListItem
                      onRemoveProduct={this.onRemoveProduct}
                      item={{
                        ...data,
                        display_name: this.getProductName(data.product_id)
                          ? this.getProductName(data.product_id).display_name
                          : null
                      }}
                    />
                  </TouchableOpacity>
                )}
                renderRightHiddenRow={(data, secId, rowId, rowMap) => (
                  <Button
                    full
                    danger
                    onPress={_ =>
                      this.onRemoveProduct(
                        data.product_id,
                        secId,
                        rowId,
                        rowMap
                      )
                    }
                  >
                    <Icon active name="trash" />
                  </Button>
                )}
                rightOpenValue={-75}
              />
            </ScrollView>
          </View>

          <View style={styles.footer}>
            <View style={{ height: "20%" }}>
              <View style={styles.listItmMain}>
                <Text style={styles.totalleft}>Total:</Text>
                <Text style={styles.totalright}>
                  Rs. {this.getTotal().toFixed(2)}
                </Text>
              </View>
              {/* <View style={styles.taxesItmMain}>
                <Text style={{ ...styles.taxesleft, fontSize: 14 }}>
                  Taxes:
                </Text>
                <Text style={styles.taxesright}>Rs: 0.00</Text>
              </View> */}
            </View>
            <TouchableOpacity
              onPress={this.proceedOrder}
              disabled={this.getTotal() <= 0}
              style={{ height: "26%" }}
            >
              <View
                style={{
                  ...styles.orderButton,
                  backgroundColor:
                    this.getTotal() <= 0 ? "lightgray" : "#dae132"
                }}
              >
                <Text style={styles.orderTxt}>Order</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        {this.state.isOpen ? (
          <QuantityUpdateModal
            onAddProductToCart={this.props.onAddProductToCart}
            onRemoveProduct={this.onRemoveProduct}
            product={this.state.currentProduct}
            onHideModal={this.onHideModal}
            isOpen={this.state.isOpen}
          />
        ) : null}
      </View>
    );
  }
}

const styles = {
  basketBadge: {
    backgroundColor: "#b26eec",
    height: 25,
    marginTop: "6%",
    position: "absolute",
    width: 25,
    borderRadius: 50,
    zIndex: 10,
    justifyContent: "center",
    marginLeft: "40%"
  },
  mainPicker: {
    width: "98%",
    position: "relative",
    // borderWidth:1,
    paddingLeft: 10,
    paddingRight: 50,
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  picketText: {
    width: "95%"
    // borderWidth:1,
  },
  pickerIcon: {
    width: "20%",
    // borderWidth:1,
    textAlign: "center"
  },
  container: {
    width: "100%",
    height: height,
    display: "flex"
  },
  contentContainer: {
    height: "67%" //63% when tax is added
  },
  footer: {
    height: "40%",
    position: "relative",
    bottom: 0
  },
  orderButton: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#dae132"
  },
  orderTxt: {
    color: "#ffffff",
    fontSize: 22,
    fontFamily: "Roboto",
    fontWeight: "600"
  },
  listItmMain: {
    flexDirection: "row",
    borderTopWidth: 1,
    borderTopColor: "#e7e7ef",
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: -10
  },
  totalright: {
    textAlign: "right",
    flex: 1,
    fontSize: 18,
    fontFamily: "Roboto",
    fontWeight: "700",
    color: "#555556"
  },
  totalleft: {
    textAlign: "left",
    flex: 1,
    fontFamily: "Roboto",
    fontWeight: "700",
    color: "#555556"
  },
  taxesItmMain: {
    flexDirection: "row",
    marginRight: 15,
    marginLeft: 15
  },
  taxesright: {
    textAlign: "right",
    flex: 1,
    fontSize: 14,
    fontFamily: "Roboto",
    fontWeight: "400",
    color: "#888787"
  },
  taxesleft: {
    textAlign: "left",
    flex: 1,
    fontFamily: "Roboto",
    fontWeight: "400",
    color: "#888787"
  },

  addIcon: {
    color: "#000",
    fontSize: 20,
    fontWeight: 600,
    backgroundColor: "#ffffff30",
    paddingLeft: 9,
    paddingRight: 9,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 50,
    height: 300,
    width: 300
  }
};

const mapStateToProps = state => {
  return {
    cart: state.cart_reducer
  };
};
export default connect(mapStateToProps)(CartContainer);
