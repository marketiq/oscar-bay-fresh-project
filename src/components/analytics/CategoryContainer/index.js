import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  TouchableNativeFeedback,
  FlatList,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { Card, ListItem } from "react-native-elements";
import { Icon } from "native-base";
import { Grid, Row, Col } from "react-native-easy-grid";
import { productsOnLoad } from "../../../actions/";

const Touchable =
  (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;

class CategoryContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCategory: undefined
    };
  }

  render() {
    let {listProducts} =this.props;
    // var filterProduct= () =>{
    //   for(var i=0 ;i<listProducts.length ;i++){
    //     if(listProducts[i])
    //   }
    // }
    return (
      <Grid style={styles.tabbgColor}>
        <ScrollView style={{ height: "auto" ,marginBottom:200}}>
          {this.state.currentCategory ? (
            <View style={styles.categoryView}>
              <TouchableOpacity
                style={styles.innercategory}
                onPress={() => {
                  this.setState({ currentCategory: undefined });
                }}
              >
                <Icon style={styles.categoryInitials} name="ios-arrow-back" />
                <Text style={styles.currentCategoryName}>
                  {this.state.currentCategory.name}
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
          {this.state.currentCategory ? (
              <FlatList 
              initialNumToRender={10}
              data={this.props.listProducts.filter(prod =>{
                return prod.pos_categ_id[0] === this.state.currentCategory.id
              })}
              keyExtractor={(item, index) => index.toString()}                              
              renderItem = {(item) => {
                item = item.item;
               return (
                    <View
                      key={item.id}
                      style={{ ...styles.categoryView, borderTopWidth: 0 }}
                    >
                      <Touchable
                        style={{ display: "flex" }}
                        key={item.id}
                        activeOpacity={0.7}
                        setOpacityTo={(0.00000001, 0.7)}
                        onPress={this.props.onAddProductToCart(item)}
                      >
                        <View style={{ width: "100%", flexDirection: "row" }}>
                          <Text style={styles.categorychildInitials}>
                            {item.display_name.substring(0, 2).toUpperCase()}
                          </Text>
                          <Text style={styles.productName}>
                            {item.display_name}
                          </Text>
                          <Text style={styles.priceTag}>
                            Rs. {item.list_price}
                          </Text>
                        </View>
                      </Touchable>
                    </View>
                  );
              }}
              />
          ) : (
            <View style={styles.categoryList}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}              
                initialNumToRender={10}
                data={this.props.categories.sort(function(a, b) {
                  var textA = a.name.toUpperCase();
                  var textB = b.name.toUpperCase();
                  return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
              })}
                renderItem={(item ) => {
                  let category=item.item;
                  let index = item.index
                  return (
                  <View
                    style={{
                      ...styles.categoryView,
                      borderTopWidth: index == 0 ? 1 : 0
                    }}
                    key={index}
                  >
                    <TouchableOpacity
                      style={{ display: "flex", flexDirection: "row" }}
                      activeOpacity={0.7}
                      onPress={() => {
                        this.setState({ currentCategory: category });
                      }}
                    >
                      <Text style={styles.categoryInitials}>
                        {category.name.substring(0, 2).toUpperCase()}
                      </Text>
                      <Text style={styles.categoryName}>{category.name}</Text>
                      <Icon
                        style={styles.categoryAction}
                        name="ios-arrow-forward"
                      />
                    </TouchableOpacity>
                  </View>
                )}}
              />
            </View>
          )}
        </ScrollView>
      </Grid>
    );
  }
}

const styles = {
  tabbgColor: {
    backgroundColor: "#fff",
    marginTop: 20,
  },
  innercategory: {
    marginRight: -10,
    display: "flex",
    flexDirection: "row",
    width: "99%"
  },
  curntCategory: {
    color: "#222222"
  },
  curntCategoryicn: {
    color: "#222222"
  },
  categoryine: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingBottom: 15,
    paddingTop: 15,
    borderTopWidth: 1,
    borderTopColor: "#e6e7e8"
  },
  categoryList: {
    borderBottomWidth: 1,
    borderColor: "#e6e7e8",
    // paddingBottom: 19
  },
  categoryView: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#ffffff",
    width: "100%",
    borderWidth: 1,
    borderColor: "#d3d3d3"
  },
  categoryInitials: {
    width: "10%",
    display: "flex",
    flexDirection: "row",
    backgroundColor: "#958cdc",
    color: "#fff",
    paddingTop: 15,
    paddingBottom: 15,
    textAlign: "center",
    fontFamily: "Roboto"
  },
  categorychildInitials: {
    width: "10%",
    display: "flex",
    flexDirection: "row",
    backgroundColor: "#BCBEC0",
    color: "#fff",
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: "center",
    fontFamily: "Roboto"
  },
  categoryName: {
    width: "85%",
    display: "flex",
    flexDirection: "row",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    color: "#595959",
    fontFamily: "Roboto"
  },
  currentCategoryName: {
    width: "85%",
    display: "flex",
    flexDirection: "row",
    paddingTop: 20,
    paddingBottom: 20,
    fontWeight: "900",
    paddingLeft: 20,
    color: "#595959",
    fontFamily: "Roboto"
  },
  categoryAction: {
    width: "5%",
    display: "flex",
    flexDirection: "row",
    color: "#C5C7C9",
    fontSize: 22,
    textAlign: "center",
    paddingTop: 20,
    paddingBottom: 20
  },

  priceTag: {
    fontFamily: "Roboto",
    fontWeight: "300",
    fontSize: 14,
    marginLeft: -10,
    color: "#6d6e71",
    width: "20%",
    flexDirection: "row",
    justifyContent: "flex-start",
    textAlign: "right",
    paddingTop: 20,
    paddingBottom: 20
  },

  productName: {
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    color: "#595959",
    fontFamily: "Roboto",
    fontWeight: "400",
    fontSize: 16,
    width: "70%",
    display: "flex",
    flexDirection: "row"
  }
};

const mapStateToProps = state => {
  return {
    categories: state.prodCategories
  };
};

export default connect(mapStateToProps, {
  productsOnLoad
})(CategoryContainer);
