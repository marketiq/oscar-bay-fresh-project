import React from 'react';
import { Modal, View, Text, TouchableOpacity, Image, ImageBackground } from 'react-native';
import { Button, Icon } from 'native-base';
import { onCartAddProduct, onCartRemoveProduct } from '../../../actions/';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

class Orderwindow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal style={styles.modalBg} transparent={true} animationType="slide" visible={this.props.isOpen} onRequestClose={() => this.props.onHideOrder()}>
                <ImageBackground
                    style={styles.imageBackground}
                    source={require('../../../static/images/background.png')}
                    resizeMode={Image.resizeMode.cover}>
                    <View style={styles.actinButtonmain}>
                        <TouchableOpacity onPress={() => this.props.onHideOrder()}>
                            <Icon style={styles.iconButton} name="md-create" />
                        </TouchableOpacity>
                        {this.props.bill > 0 ?
                            <TouchableOpacity style={styles.forwardBtn} onPress={() => {
                                this.props.dispatch(NavigationActions.navigate({
                                    routeName: 'PaymentWindow', params: {
                                        billAmount: this.props.bill
                                    }
                                }))
                            }}>
                                <Icon style={styles.iconButton} name="ios-arrow-forward" />
                            </TouchableOpacity> : null}
                    </View>
                    <View style={styles.contentContainer}>
                        <Text style={styles.text}>Your total bill is</Text>
                        <Text style={styles.priceText}>Rs. {(this.props.bill).toFixed(2)}</Text>
                        <Image
                            source={require('../../../static/images/waiter_image.png')}
                        />
                        <Text style={styles.textWaiter}>Please hand back tablet to waiter</Text>
                    </View>
                </ImageBackground>
            </Modal>
        );
    }
}

const styles = {
    forwardBtn: {
        height:100,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        bottom:20,
        width:100,
    },
    actinButtonmain: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 40,
        marginRight: 40,
        marginTop: 40,
    },
    iconButton: {
        fontSize: 50,
        textAlign:'right',
        color: '#fff',
        width: '100%'
    },
    priceText: {
        color: '#fff',
        fontSize: 40,
        fontWeight: '700',
        fontFamily: 'Roboto',
        marginTop: 5,
        marginBottom: 40
    },
    text: {
        color: '#fff',
        fontSize: 34,
        fontFamily: 'Roboto',
        fontWeight: '100',
        margin: 5
    },
    textWaiter: {
        color: '#fff',
        fontSize: 34,
        fontFamily: 'Roboto',
        fontWeight: '100',
        marginTop: 30
    },
    imageBackground: {
        height: '100%',
        width: '100%',
    },
    contentContainer: {
        height: '70%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
};

export default connect(null, {
    onCartAddProduct,
    onCartRemoveProduct
})(Orderwindow);