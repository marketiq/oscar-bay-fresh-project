import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Platform,
  TouchableNativeFeedback,
  ActivityIndicator,
  FlatList,
  Dimensions
} from "react-native";
import { Button, Card } from "react-native-elements";
import { Icon, Item, Input, Container } from "native-base";
import { Footer } from "../../../components/common";
import TabsComponent from "../../common/Tabs/Tabs";
import { Grid, Row, Col } from "react-native-easy-grid";
import CartContainer from "../CartContainer/";
import {
  productsOnLoad,
  searchProduct,
  onLoadCategories,
  onCartAddProduct
} from "../../../actions/";

const Touchable =
  (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;
var { height, width } = Dimensions.get("window");

class OverviewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: [],
      query: "",
      productsList: this.props.listProducts,
      data: [],
      searchInput: "",
      error: "",
      loading: false
    };
    this.onClickProduct = this.onClickProduct.bind(this);
    this.onAddProductToCart = this.onAddProductToCart.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
    this.searchProduct = this.searchProduct.bind(this);
  }
  componentWillMount() {
    if (this.props.listProducts.length > 0) {
      return;
    } else {
      this.setState({ loading: true, error: "" });
      this.props.onLoadCategories();
      this.props
        .productsOnLoad()
        .then(res => {
          this.setState({ loading: false });
        })
        .catch(err => {
          console.log("error :", JSON.parse(JSON.stringify(err)));
          this.setState({
            loading: false,
            error: "Fail to load products. Please try again!"
          });
        });
    }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      productsList: nextProps.listProducts
    });
  }
  componentDidUpdate() {
    console.log("in CDU");
  }
  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.props !== nextProps || this.state !== nextState) {
  //     return true;
  //   }
  //   return false;
  // }
  onAddProductToCart = product => () => {
    console.log("in OAPTC");
    let data = {};
    if (product.product_id) {
      data = product;
    } else {
      data = {
        product_id: product.id,
        pack_lot_ids: [],
        note: "",
        discount: 0,
        price_unit: product.list_price,
        qty: 1
      };
    }
    this.props.onCartAddProduct(data);
  };

  onClickProduct() {
    var { cartData } = this.state;
  }

  searchProduct(event) {
    const filteredProducts = inputText => {
      inputText = inputText ? inputText.toLowerCase() : inputText;
      inputText = inputText ? inputText.replace(/\\/g, "\\\\") : inputText;
      var patt = new RegExp("\\b" + inputText);
      const tempProducts = this.props.listProducts.filter(product => {
        return patt.test(product.display_name.toLowerCase());
      });
      this.setState({ productsList: tempProducts });
    };
    if (event) {
      let text = event.nativeEvent.text;
      this.setState({ searchInput: event.nativeEvent.text }, () => {
        filteredProducts(this.state.searchInput);
      });
    } else {
      this.setState({ searchInput: "" }, () => {
        filteredProducts(this.state.searchInput);
      });
    }
  }

  render() {
    var searchView = () => {
      let { productsList } = this.state;
      return (
        <FlatList
          initialNumToRender={10}
          keyExtractor={(item, index) => index.toString()}
          data={productsList}
          renderItem={item => {
            item = item.item;
            let index = item.index;
            return (
              <View
                key={item.id}
                style={{
                  ...styles.categoryView,
                  borderTopWidth: index == 0 ? 1 : 0
                }}
              >
                <Touchable
                  style={{ display: "flex" }}
                  key={item.id}
                  activeOpacity={0.7}
                  setOpacityTo={(0.00000001, 0.7)}
                  onPress={this.onAddProductToCart(item)}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row"
                    }}
                  >
                    <Text style={styles.searchProductInitials}>
                      {item.display_name.substring(0, 2).toUpperCase()}
                    </Text>
                    <Text style={styles.productName}>{item.display_name}</Text>
                    <Text style={styles.priceTag}>Rs. {item.list_price}</Text>
                  </View>
                </Touchable>
              </View>
            );
          }}
        />
      );
    };

    return (
      <View style={{ height, width }}>
        <Grid style={{ zIndex: 0 }}>
          <Row size={100}>
            <Col size={5} style={styles.leftSide}>
              {/* TODO: For Future use  */}
              <Item style={styles.productSearch}>
                <Icon style={styles.productSearchIcon} name="md-search" />
                <Input
                  placeholderTextColor="gray"
                  style={{ color: "black", fontSize: 18 }}
                  placeholder="Search Item"
                  type="text"
                  value={this.state.searchInput}
                  onChange={this.searchProduct}
                />
                {this.state.searchInput ? (
                  <TouchableOpacity onPress={this.searchProduct}>
                    <Icon style={styles.productCloseIcon} name="md-close" />
                  </TouchableOpacity>
                ) : null}
              </Item>
              <View>
                {this.state.loading ? (
                  <View style={styles.loadingContainer}>
                    <ActivityIndicator
                      size={60}
                      color="#B968EE"
                      animating={this.state.loading}
                    />
                    <Text style={styles.loadingText}>Loading products ...</Text>
                  </View>
                ) : this.state.error ? (
                  <TouchableOpacity onPress={this.componentWillMount}>
                    <View style={styles.loadingContainer}>
                      <Icon
                        style={{ color: "#c858f7", fontSize: 50 }}
                        name="md-refresh"
                      />
                      <Text style={styles.loadingText}>{this.state.error}</Text>
                    </View>
                  </TouchableOpacity>
                ) : (
                  <View style={styles.tabMain}>
                    {this.state.searchInput ? (
                      <Container
                        style={{
                          top: "3%"
                        }}
                      >
                        <View
                          style={{ ...styles.categoryList, ...styles.martp }}
                        >
                          <ScrollView
                            keyboardShouldPersistTaps="always"
                            keyboardDismissMode="none"
                            style={{ height: "auto" }}
                          >
                            {searchView()}
                          </ScrollView>
                        </View>
                      </Container>
                    ) : (
                      <TabsComponent
                        onAddProductToCart={this.onAddProductToCart}
                        listProducts={this.props.listProducts}
                      />
                    )}
                  </View>
                )}

                {/* <View style={styles.footerMain}>
                  <Footer />
                </View> */}
              </View>
            </Col>

            <Col size={3} style={styles.rightSide}>
              <Row size={4}>
                <Col size={5}>
                  <CartContainer
                    products={this.props.listProducts}
                    onAddProductToCart={this.onAddProductToCart}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

const styles = {
  loadingContainer: {
    height: height - 250,
    justifyContent: "center",
    alignItems: "center"
  },
  loadingText: {
    color: "gray",
    fontSize: 20,
    paddingTop: 10,
    paddingBottom: 10,
    textAlign: "center",
    fontFamily: "Roboto"
  },
  mainRow: {
    flexDirection: "row",
    flex: 1
  },
  leftSide: {
    backgroundColor: "#fafbfb",
    paddingTop: 15,
    paddingBottom: 15
  },
  rightSide: {
    backgroundColor: "#fff",
    borderLeftWidth: 2,
    borderLeftColor: "#e7e7ef"
  },
  productSearch: {
    height: 50,
    backgroundColor: "#fff",
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 0,
    marginLeft: 15,
    marginRight: 15
  },
  productSearchIcon: {
    color: "#c858f7",
    fontSize: 24,
    fontWeight: "600"
  },
  productCloseIcon: {
    color: "#c858f7",
    fontSize: 24,
    fontWeight: "600"
  },
  tabMain: {
    // height:height-250,
    height: height,
    // height: "70%",
    marginLeft: 15,
    marginRight: 15
  },
  footerMain: {
    height: 91,
    marginLeft: 15,
    marginRight: 15
  },
  searchView: {
    borderWidth: 1,
    backgroundColor: "white"
  },
  categoryList: {
    borderBottomWidth: 1,
    borderColor: "#e6e7e8",
    paddingBottom: 0,
    marginBottom: 200
  },
  categoryView: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#ffffff",
    width: "100%",
    borderWidth: 1,
    borderColor: "lightgray"
    // borderTopColor: "#e6e7e8"
  },
  searchProductInitials: {
    width: "10%",
    display: "flex",
    flexDirection: "row",
    backgroundColor: "#BCBEC0",
    color: "#fff",
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: "center",
    fontFamily: "Roboto"
  },
  priceTag: {
    fontFamily: "Roboto",
    fontWeight: "300",
    fontSize: 14,
    marginLeft: -10,
    color: "#6d6e71",
    width: "20%",
    flexDirection: "row",
    justifyContent: "flex-start",
    textAlign: "right",
    paddingTop: 20,
    paddingBottom: 20
  },
  productName: {
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    color: "#595959",
    fontFamily: "Roboto",
    fontWeight: "400",
    fontSize: 16,
    width: "70%",
    display: "flex",
    flexDirection: "row"
  }
};
const mapStateToProps = state => {
  return {
    listProducts: state.product_reducer
  };
};

export default connect(
  mapStateToProps,
  {
    productsOnLoad,
    searchProduct,
    onCartAddProduct,
    onLoadCategories
  }
)(OverviewPage);
