import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  TouchableNativeFeedback,
  FlatList,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { Card, ListItem, Icon } from "react-native-elements";
import { Container, Content, Header, Title, Tabs, Tab } from "native-base";
import { Grid, Row, Col } from "react-native-easy-grid";
import { productsOnLoad } from "../../../actions/";

const Touchable =
  (Platform.OS === "android" && Platform['Version'] >= 21) ? TouchableNativeFeedback : TouchableOpacity;

  // background={(Platform['Version'] >= 21) ? TouchableNativeFeedback.Ripple(props.backgroundColor) : TouchableNativeFeedback.SelectableBackground()}


class PopularItemsContainer extends Component {
  constructor(props) {
    super(props);
  }
  renderItem = item => {
    item = item.item;
    return (
      <Col style={styles.productCard}>
        <Touchable
          style={styles.TouchableHighlight}
          underlayColor="black"
          // background={TouchableNativeFeedback.Ripple()}
          activeOpacity={0.5}
          onPress={this.props.onAddProductToCart(item)}
        >
          <View style={styles.mainCardbx}>
            <Text style={styles.priceTag}>Rs. {item.list_price}</Text>
            <Text style={styles.productName}>{item.display_name} </Text>
            <Text> </Text>
          </View>
        </Touchable>
      </Col>
    );
  };
  render() {
    const { searchList, searchLoading, searchValue, listProducts } = this.props;

    console.ignoredYellowBox = ["Remote debugger"];
    return (
      <Grid style={styles.tabbgColor}>
        <ScrollView
          style={{ height: "auto", marginBottom: 200 }}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          showsVerticalScrollIndicator={false}
        >
          <Row size={3} style={styles.productMain}>
            <FlatList
              initialNumToRender={4}
              horizontal={false}
              numColumns={4}
              data={this.props.listProducts
                .sort(function(a, b) {
                  var textA = a.display_name.toUpperCase();
                  var textB = b.display_name.toUpperCase();
                  return textA < textB ? -1 : textA > textB ? 1 : 0;
                })
                .filter(item => {
                  return item.popular;
                })}
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderItem}
            />
          </Row>
        </ScrollView>
      </Grid>
    );
  }
}
const styles = {
  productCard: {
    width: '25%',
  },
  TouchableHighlight: {
    marginRight: 8,
    marginLeft: 8,
    marginTop: 10,
    marginBottom: 15
  },
  mainCardbx: {
    width: "90%",
    height: 140,
    margin: 10,
    borderWidth: 0,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    borderColor: "lightgrey",
    backgroundColor: "#fff",
    elevation: 2
  },
  divider: {
    display: "none"
  },
  tabbgColor: {
    backgroundColor: "#fafbfb",
    paddingTop: 20
  },
  productMain: {
    width:'100%',
    display: "flex",
    flexWrap: "wrap"
  },
  priceTag: {
    color: "black",
    alignSelf: "flex-end",
    paddingRight: 0,
    marginRight: 10,
    marginTop: 5,
    fontFamily: "Roboto",
    fontWeight: "400",
    fontSize: 16
  },
  productName: {
    textAlign: "center",
    color: "#555556",
    alignSelf: "center",
    marginRight: 10,
    marginLeft: 10,
    fontFamily: "Roboto",
    fontWeight: "100",
    fontSize: 15
  }
};

export default connect(
  null,
  {
    productsOnLoad
  }
)(PopularItemsContainer);
