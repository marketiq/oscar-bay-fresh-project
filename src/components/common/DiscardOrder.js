import React from 'react';
import {Modal,View,Text,TouchableOpacity} from 'react-native';
import {Button, Icon} from 'native-base';
import {emptyCart} from '../../actions/';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';

class DiscardOrder extends React.Component{
    constructor(props){
        super(props);
        this.state = {
        }
        this.onDiscard = this.onDiscard.bind(this);
    }
    onDiscard(){
        let {dispatch} =this.props;
        dispatch(emptyCart());
        dispatch(NavigationActions.navigate({routeName:'Dashboard'}));
        this.props.onHideModal();
    }
    render(){
        return (
            <Modal transparent={true} animationType="fade" visible={this.props.isOpen} onRequestClose={() => this.props.onHideModal()} onPress= {()=> this.props.onHideModal()}>
                <View style={styles.container}>
                    <View style={styles.modalInner}>
                        <View style={styles.modalHeader}>
                            <TouchableOpacity style={styles.closeButton} onPress={()=>this.props.onHideModal()}>
                                <Icon  style={styles.closeButtonText} name="md-close" />
                            </TouchableOpacity>
                            <Text style={styles.headerText}>Discard Order</Text>
                        </View>
                        <View style={{height:'70%',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                            <Icon style={{fontSize:100,marginBottom:10}} name="md-cart" />
                            <Text style={styles.text}>Are you sure you want to discard this order?</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',height:'20%'}}>
                            <Button style={styles.removeButton} onPress={()=>this.onDiscard()}>
                                <Text style={styles.removeText}>Yes</Text>
                            </Button>
                            <Button style={styles.removeButton} onPress={()=>{this.props.onHideModal()}}>
                                <Text style={styles.removeText}>No</Text>
                            </Button>                        
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = {
    container: {
        paddingLeft: 40,
        paddingRight: 40,
        justifyContent: 'center',
        flexDirection: 'row',
        alignContent: 'space-between',
        backgroundColor: '#00000098',
        width: '100%',
        height: '100%'
    },
    addQuantity:{
        justifyContent:'center',
        flexDirection:'row',
        marginBottom: 10, 
        width: '75%',
        paddingTop: '10%',

    },
    addQuantityinner: {
        justifyContent:'center',
        flexDirection:'row',
        width: '100%',
        paddingLeft: '15%',
        fontFamily: 'Roboto',
        fontWeight: '700',
        fontSize: 18,
        color: '#555556'
    },
    modalInner: {
        backgroundColor: '#fff',
        height: '100%',
        width: '70%'
    },
    modalHeader:{
        height:'10%',
        backgroundColor: '#978add', 
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingTop: 20, 
        paddingBottom: 20, 
        paddingLeft: 10, 
        paddingRight: 10,
        height:70,
        width: '100%'
    },
    modalContent: {
        paddingTop: 10, 
        paddingBottom: 10, 
        paddingLeft: 10, 
        paddingRight: 10,
        justifyContent: 'center',
        flexDirection: 'row',
    },
    image: {
        height: 140
    },
    closeButton:{
        width:'4%'
    },
    closeButtonText: {
        color:'#fff',
        fontSize:25,
        paddingLeft:15,
        // paddingTop: 5,
        marginLeft: -10,
        fontWeight:'900',
    },
    headerText: {
        color:'#fff',
        width:'96%',
        fontSize:20,
        marginLeft: 20,
        fontFamily: 'Roboto',
        fontWeight: '100',
        fontSize: 20,
    },
    headerPriceText:{
        color: '#fff',
        fontSize:20,
        textAlign:'right',
        position: 'relative',
        right: 10,
        width:'30%',
        fontFamily: 'Roboto',
        fontWeight: '100',
        fontSize: 18,
    },
    text: {
        color: '#555556',
        fontSize: 20,
        // textAlign:'center',
        fontWeight: '300',
        backgroundColor: 'transparent',
        // alignItems:'center'
    },
    removeText: {
        color: '#555556',
        fontFamily: 'HelveticaNeue',
    },
    removeButton: {
        alignSelf: 'center',
        backgroundColor: '#fafbfc',
        // marginTop: '50%', 
        // marginBottom: 10, 
        marginRight:5,
        marginLeft:5,
        flexDirection:'row',
        borderWidth: 1,
        borderColor: '#c2c7cc',
        width: '40%',
        height: 60,
        paddingTop: 30,
        paddingBottom: 30,
        elevation: 0,
        justifyContent:'center',
        borderRadius: 5
    }
};

export default connect()(DiscardOrder);