import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text,Image } from 'react-native'
import { Card, ListItem, Icon, } from 'react-native-elements';
import { Container, Content, Header, Title, Tabs, Tab } from 'native-base';

 class Footer extends Component {

    render() {
        return (
            <View style={styles.mainCardbx}>
                <Image
                    source={require('../../static/images/adv.png')}
                    resizeMode={Image.resizeMode.contain}
                    style={{
                        width: '100%',
                        height:91,
                        
                    }} />
            </View>

        );
    }
}

const styles = {
   
    mainCardbx: {
        height:'100%',
        width:'100%',
        backgroundColor: 'transparent',
    },
}
export {Footer};
