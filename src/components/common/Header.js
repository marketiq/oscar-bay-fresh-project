import React from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { Item, Button, Icon, } from 'native-base';
import DiscardOrder from './DiscardOrder';
import { NavigationActions } from 'react-navigation';
import {connect} from 'react-redux';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
        }
        this.navigateMobRecharge = this.navigateMobRecharge.bind(this);                                        
        this.onHideModal = this.onHideModal.bind(this);
        this.toggleDrawer = this.toggleDrawer.bind(this);
    }
    onHideModal() {
        this.setState({ isOpen: false })
    }
    navigateMobRecharge(){
        this.props.dispatch(NavigationActions.navigate({routeName:'PhoneNumber'}))        
    }
    toggleDrawer(){
        const { navigation } = this.props;        
        if (navigation.state.index === 0) {
            navigation.navigate('DrawerOpen')
        } else {
            navigation.navigate('DrawerClose')
        }
    }
    render() {
        const { navigation } = this.props;                
        return (
            <View style={styles.mainContainer}>
                <View style={styles.logoContainer}>
                    <Image
                        source={require('../../static/images/logo.png')}
                        // resizeMode={Image.resizeMode.contain}
                        style={styles.logoMain} />
                </View>
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Item style={styles.iconButtonmain}>
                            {/* <Button transparent style={styles.mobileRechargeIcon} 
                            onPress={this.navigateMobRecharge}
                            >
                                <Icon name="ios-phone-portrait" style={{ color: '#fff' }} />
                                <Text style={{ color: '#fff' }}>Mobile Recharge</Text>
                            </Button>  */}
                            <Button transparent onPress={() => this.setState({ isOpen: true })}>
                                <Icon style={styles.addIcon} name="md-add" />
                            </Button>
                            <Text style={styles.waiterText}>Waiter</Text>
                            <Button
                            transparent
                            style={{left:15}}
                            onPress={this.toggleDrawer}
                            >
                            <Icon style={styles.menuIcon}  color="white" name="menu" underlayColor="transparent" />
                        </Button>
                        </Item>
                    </View>
                </View>
                {
                    this.state.isOpen ?
                        <DiscardOrder navigation={navigation} isOpen={this.state.isOpen} onHideModal={this.onHideModal} />
                        : null
                }
            </View>
        );
    }
}

const styles = {
    mainContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 0,
        width: '100%',
        height:70,
        top: 0,
        position: 'relative',
        backgroundColor: '#b968ee',
        zIndex: 999
    },
    iconButtonmain: {
        flexDirection: 'row',
        borderBottomWidth: 0,
    },
    logoContainer:{
        padding:15,
    },
    logoMain: {
        height:30,
        width: 120,
    },
    addIcon: {
        color: '#ffffff',
        fontSize: 20,
        fontWeight: 600,
        backgroundColor: '#ffffff30',
        paddingLeft: 9,
        paddingRight: 9,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 50,
        height: 30,
        width: 30,
    },
    mobileRechargeIcon: {
        backgroundColor: '#ffffff30',
        paddingLeft: 0,
        paddingRight: 35,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 50,
        marginTop: 2,
        height: 40,
        width: 200,
    },
    waiterText: {
        color: '#ffffff',
        marginLeft: 20,
        marginRight: 5,
        marginTop: -5,
    },
    menuIcon: {
        paddingLeft: 8,
        paddingRight: 15,
        color: '#ffffff',
    },

}

export default connect()(Header);