import React from 'react';
import { View, Image, ImageBackground } from 'react-native';

const ImageBackgroundContainer = ({children, customStyle}) => {

    return (
        <View style={styles.container}>
            <ImageBackground
                style={{...styles.imageBackground, ...customStyle}}
                source={require('../../static/images/background.png')}
                resizeMode={Image.resizeMode.cover}
            >
                {children}
            </ImageBackground>
        </View>
    );

}

const styles = {
    container: {
        flex: 1
    },
    imageBackground: {
        flex: 1,
        paddingLeft: 40,
        paddingRight: 40,
        justifyContent: 'center',
        flexDirection: 'column'
    }
}

export { ImageBackgroundContainer };