import React from "react";
import { View, TextInput, TouchableHighlight } from "react-native";
import {
  FormLabel,
  FormInput,
  FormValidationMessage
} from "react-native-elements";
import { Icon } from "native-base";

const Input = ({
  label,
  value,
  onChangeText,
  placeholder,
  secureTextEntry,
  keyboardType,
  multiline,
  errorMessage,
  customStyle,
  icon,
  emptyField,
}) => {
  const mergedStyle = { ...styles.default, ...customStyle };

  const displayFormLabel = label => label && <FormLabel>{label}</FormLabel>;
  const displayMessageLabel = errorMessage =>
    errorMessage && (
      <FormValidationMessage>{errorMessage}</FormValidationMessage>
    );
  return (
    <View style={styles.container}>
      {displayFormLabel(label)}
      <View style={styles.inputContainer}>
        {/* <Icon
                    color={mergedStyle.color}
                    name={icon}
                    size={20}
                    type='font-awesome'
                /> */}
        <TextInput
          autoCapitalize="none"
          keyboardType={keyboardType}
          multiline={multiline}
          secureTextEntry={secureTextEntry}
          autoCorrect={false}
          placeholder={placeholder}
          value={value}
          onChangeText={onChangeText}
          maxHeight={100}
          placeholderTextColor={mergedStyle.color}
          style={styles.input}
          selectionColor={mergedStyle.color}
          underlineColorAndroid="transparent"
        />
        {icon ? (
          <TouchableHighlight
            style={styles.iconButton}
            underlayColor="gray"
            onPress={emptyField}
          >
            <Icon
              style={{ color: "white" }}
              name={icon}
              size={5}
            />
          </TouchableHighlight>
        ) : null}
      </View>
      <FormValidationMessage style={styles.valiationMessageContainer}>
        {errorMessage}
      </FormValidationMessage>
    </View>
  );
};

const styles = {
  iconButton: {
    width: 50,
    height: '100%',
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  default: {
    color: "white"
  },
  container: {
    width: "50%"
  },
  inputContainer: {
    borderRadius: 0,
    borderBottomWidth: 1,
    maxHeight:50,
    minHeight:50,
    borderColor: "#fff",
    padding: 10,
    display: "flex",
    flexDirection: "row",
    marginBottom: 10,
    paddingLeft: 0,
    paddingRight: 0
  },
  input: {
    color: "white",
    fontSize: 15,
    flex: 1,
    paddingLeft: 0,
    paddingTop: 0,
    paddingBottom: 0
  },
  valiationMessageContainer: {
    backgroundColor: "transparent"
  }
};

export { Input };
