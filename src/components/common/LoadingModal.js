import TimerMixin from "react-timer-mixin";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Modal,
  View,
  Text,
  Image,
  Animated,
  Easing,
  Platform
} from "react-native";
import { WHITE_COLOR } from "../../constants/constants";
import { ImageBackgroundContainer } from "../common";
import { ConfigSelectors } from "../../modules/config";

const { getSelectorConfig } = ConfigSelectors;

const loadingModalTimeout = Platform.OS === "ios" ? 600 : 0;

class LoadingModalComponent extends Component {
  constructor() {
    super();
    this.state = { spinValue: new Animated.Value(0), isLoading: false };
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.loading) {
      TimerMixin.setTimeout(() => {
        this.setState({ isLoading: false });
      }, loadingModalTimeout);
    } else {
      this.setState({ isLoading: true });

      Animated.loop(
        Animated.timing(this.state.spinValue, {
          toValue: 1,
          duration: 3000,
          easing: Easing.linear,
          useNativeDriver: true
        })
      ).start();

      const spin = this.state.spinValue.interpolate({
        inputRange: [0, 1],
        outputRange: ["0deg", "360deg"]
      });
      this.setState({ spin });
    }
  }

  render() {
    return (
      <Modal
        transparent={true}
        animationType="fade"
        visible={this.state.isLoading}
        onRequestClose={() => false}
      >
        <View style={styles.container}>
          <Animated.View style={{ transform: [{ rotate: this.state.spin }] }}>
            <Image
              style={styles.image}
              source={require("../../static/images/loader.png")}
              resizeMode={Image.resizeMode.contain}
            />
          </Animated.View>
          <Text style={styles.text}>loading...</Text>
        </View>
      </Modal>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#00000098"
  },
  image: {
    height: 140
  },
  text: {
    color: WHITE_COLOR,
    fontSize: 25,
    fontWeight: "300",
    backgroundColor: "transparent",
    paddingTop: 20
  }
};

const mapStateToProps = state => {
  return getSelectorConfig(state);
};

const LoadingModal = connect(mapStateToProps, {})(LoadingModalComponent);

export { LoadingModal };
