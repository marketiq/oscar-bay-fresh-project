import React from 'react';
import { Modal, View, Text, TouchableOpacity, Image, ImageBackground,TouchableHighlight, TextInput } from 'react-native';
import { Button, Icon } from 'native-base';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

class PayCardModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal style={styles.modalBg} transparent={true} animationType="slide" visible={this.props.isOpen} onRequestClose={() => this.props.onHideModal('cardPayment')}>
                <ImageBackground
                    style={styles.imageBackground}
                    source={require('../../static/images/background.png')}
                    resizeMode={Image.resizeMode.cover}>
                        <View style={{flexDirection:'row',justifyContent:'flex-end',margin:10,alignItems:'center'}} >
                            <TouchableHighlight 
                            underlayColor='gray'
                            activeOpacity={0.9}
                            onPress={()=>{this.props.onHideModal('cardPayment')}}>                        
                                <View style={{flexDirection:'row',alignItems:'center'}} >
                                    <Icon  name="md-cash" style={styles.text}/>
                                    <Text style={styles.text}>Payment options</Text>
                                </View>
                            </TouchableHighlight>
                          </View>
                    <View style={styles.contentContainer}>
                        <Text style={styles.priceText}>Card Payment</Text>
                        <Text style={styles.text}>Enter your card number</Text>
                       
                       <View style={{flexDirection:'row'}}>
                        <Icon name="md-card" />
                        <TextInput  style={{width:100}} placeholder='123456789'/>
                        <Button>
                            <Text>Charge</Text>
                        </Button>
                       </View>
                    </View>

                </ImageBackground>
            </Modal>
        );
    }
}

const styles = {
    actinButtonmain: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 35,
    },
    iconButton: {
        fontSize: 40,
        color: '#fff'
    },
    priceText: {
        color: '#fff',
        fontSize: 40,
        fontWeight: '700',
        fontFamily: 'Roboto',
        marginTop: 5,
        marginBottom: 40
    },
    text: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'Roboto',
        // fontWeight: '100',
        margin: 5
    },
    textWaiter: {
        color: '#fff',
        fontSize: 34,
        fontFamily: 'Roboto',
        fontWeight: '100',
        marginTop: 30
    },
    imageBackground: {
        height: '100%',
        width: '100%',
    },
    contentContainer: {
        height: '90%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
};

export {PayCardModal};