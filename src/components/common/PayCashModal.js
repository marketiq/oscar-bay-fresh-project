
import React from 'react';
import { Modal, View, Text, TouchableOpacity, Image, ImageBackground } from 'react-native';
import { Button, Icon } from 'native-base';
// import { onCartAddProduct, onCartRemoveProduct } from '../../../actions/';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

class PayCashModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal style={styles.modalBg} transparent={true} animationType="slide" visible={this.props.isOpen} onRequestClose={() => this.props.onHideModal('cashPayment')}>
                <ImageBackground
                    style={styles.imageBackground}
                    source={require('../../static/images/background.png')}
                    resizeMode={Image.resizeMode.cover}>
                    <View style={styles.contentContainer}>
                        <Text style={styles.priceText}>Total Bill: Rs. {this.props.billAmount} </Text>
                        <Text style={styles.priceText}>Rs. {this.props.tenderAmount - this.props.billAmount} Change</Text>
                        <Text style={styles.outofText}>Out of Rs. {this.props.tenderAmount}</Text>
                      
                        {/* <Text style={styles.tagtext}>Would you like to give us your feedback?</Text> */}
                      
                        <Text style={styles.tagtext}>Thank you for shopping with us!</Text>
                        <Text style={styles.tagtext}>Your order # is {this.props.orderNum}!</Text>
                        {/* <Button transparent style={styles.fedbackBtnMain}>
                            <Text style={styles.fedbackBtn}>Feedback</Text>
                        </Button> */}
                        <Button transparent style={styles.fedbackBtnMain}
                            onPress={() => this.props.onHideModal('cashPayment')}
                        >
                            <Text style={styles.fedbackBtn}>New order</Text>
                        </Button>

                    </View>
                </ImageBackground>
            </Modal>
        );
    }
}

const styles = {
    actinButtonmain: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 35,
    },
    iconButton: {
        fontSize: 40,
        color: '#fff'
    },
    priceText: {
        color: '#fff',
        fontSize: 32,
        fontWeight: '700',
        fontFamily: 'Roboto',
        marginTop: 5,
        marginBottom: 5
    },
    outofText: {
        color: '#fff',
        fontSize: 25,
        fontWeight: '300',
        fontFamily: 'Roboto',
        marginTop: 0,
        marginBottom: 40
    },
    text: {
        color: '#fff',
        fontSize: 34,
        fontFamily: 'Roboto',
        fontWeight: '100',
        marginTop: 5,
        marginBottom: 5,
    },
    tagtext: {
         color: '#fff',
        fontSize: 34,
        fontFamily: 'Roboto',
        fontWeight: '100',
        marginTop: 5,
        marginBottom: 5,
    },
    fedbackBtnMain: {
        width: '40%', 
        marginLeft: '30%', 
        backgroundColor: 'white', 
        marginTop: '5%', 
        borderWidth: 0 ,
        paddingTop: '2%',
        paddingBottom: '2%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    fedbackBtn: {
        // backgroundColor: 'white', 
        borderWidth: 0 ,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    textWaiter: {
        color: '#fff',
        fontSize: 34,
        fontFamily: 'Roboto',
        fontWeight: '100',
        marginTop: 30
    },
    imageBackground: {
        height: '100%',
        width: '100%',
    },
    contentContainer: {
        height: '90%',
        width: '100%',
        marginTop: '3%',
        alignItems: 'center',
        justifyContent: 'center'
    }
};

export {PayCashModal};