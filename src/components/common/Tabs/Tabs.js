import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native'
import { Card, ListItem, Icon, } from 'react-native-elements';
import { Container, Content, Header, Title, Tabs, Tab } from 'native-base';
import { AnalyticsActions, AnalyticsSelectors } from '../../../modules/analytics';
import CategoryContainer from '../../analytics/CategoryContainer';
import PopularItemsContainer from '../../analytics/PopularItemsContainer';

const { selectLocations, selectDate } = AnalyticsActions;
const { getSelectorFilters } = AnalyticsSelectors;

class TabsComponent extends Component {
  render() {
    return (
      <Container style={{
        top: '3%',
      }}>

        <Tabs style={{}} tabBarUnderlineStyle={{ backgroundColor: "transparent", borderBottomWidth:5, borderBottomColor: '#ab76e7'}} initialPage={0}>
          <Tab heading="POPULAR ITEMS" tabStyle={{ backgroundColor: '#ffffff',}} textStyle={{ color: '#222222', fontWeight: '600', fontFamily: 'Roboto', }} activeTabStyle={{ backgroundColor: '#ffffff',  }} activeTextStyle={{ color: '#222', }}>
            <PopularItemsContainer onAddProductToCart={this.props.onAddProductToCart} listProducts={this.props.listProducts} />           
          </Tab>
          <Tab heading="CATEGORY" tabStyle={{ backgroundColor: '#ffffff',}} textStyle={{ color: '#222222', fontWeight: '600', fontFamily: 'Roboto', }} activeTabStyle={{ backgroundColor: '#ffffff',  }} activeTextStyle={{ color: '#222', }}>
            <CategoryContainer onAddProductToCart={this.props.onAddProductToCart} listProducts={this.props.listProducts} />
         </Tab>
        </Tabs>
      </Container>

    );
  }
}

const styles = {
  tabHeading: {
    color: '#000'
  },
  tabMain: {
      shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
  },
}

export default TabsComponent;
