import {AsyncStorage} from 'react-native';

export const BASE_URL = "https://hubapi.oscar.pk/";

// export const BASE_URL = "http://192.168.10.17:8000/";

export const DB_URL = "https://qa.oscar.pk/";
// export const DB_URL = 'http://0.0.0.0:8069/';
export const WHITE_COLOR = "white";
export const ACTIVE_TAB_COLOR = "#9d84e0";
export const ACTIVE_TAB_TEXT_COLOR = "#000000";
export const INACTIVE_TAB_COLOR = "#a9b4da";
export const HEADER_COLOR = "#b968ee";
export const ACTIVATE_COLOR = "#59d345";
export const CONFIRM_BUTTON_COLOR = "#9d84e0";
export const APP_BACKGROUND_COLOR = "#f9f9fe";


export var testBaseUrl = url => {
  if (/\d+\.\d+\.\d+\.\d+/.test(url)) {
    return  "http://" + url + ":8000/"
  }else {
    return "https://hubapi.oscar.pk/";
  }
};
export var testDBUrl = url => {
  if (/\d+\.\d+\.\d+\.\d+/.test(url)) {
    return "http://" + url + ":8069/";
  } else {
    return "https://qa.oscar.pk/";
  }
};

export var multiGet = (callback) => {
  try {
    AsyncStorage.multiGet(["base_url", "db"])
      .then(values => {
        callback(values);
      })
      .catch(err => {
      });
  } catch (error) {
  }
}

export var getBaseUrl = (callback) => {
  try {
    AsyncStorage.getItem("base_url")
      .then(value => {
        callback(value);
      })
      .catch(err => {
      });
  } catch (error) {
  }
}
