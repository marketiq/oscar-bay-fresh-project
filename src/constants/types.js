export const PAGE_LOADING = 'page_loading';
export const PAGE_LOADED = 'page_loaded';

export const AUTH_ON_CHANGE = 'auth_on_change';
export const AUTH_ON_LOGIN = 'auth_on_login';
export const AUTH_ON_LOGIN_ERROR = 'auth_on_login_error';
export const AUTH_ON_LOGIN_SUCCESS = 'auth_on_login_success';
export const AUTH_ON_LOGOUT = 'auth_on_logout';

export const PROFILE_POPULATED = 'profile_populated';
export const PROFILE_CHANGED = 'profile_changed';

export const ANALYTICS_POPULATE = 'analytics_populate';
export const SELECT_LOCATIONS = 'select_locations';
export const SELECT_DATES = 'select_dates';

export const PRODUCTS_ON_SEARCH_CHANGE = 'products_on_search_change'
export const PRODUCTS_POPULATE = 'products_populate'

export const CART_ON_ADD_PRODUCT = 'cart_on_add_product'
export const CART_ON_REMOVE_PRODUCT = 'cart_on_remove_product'
