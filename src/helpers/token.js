import { AsyncStorage } from 'react-native'
import axios from 'axios'

export const deleteToken = async () => {
    try {
		await AsyncStorage.removeItem('acces_token')
		axios.defaults.headers.common['Authorization'] = null;
    } catch (error) {
    }
}

export const saveToken = async (acces_token) => {
    try {
    	const token = `Token ${acces_token}`;
		await AsyncStorage.setItem('acces_token', token)
		axios.defaults.headers.common['Authorization'] = token;
    } catch (error) {
    }
}

export const getToken = async () => {
	try {
		var acces_token = await AsyncStorage.getItem('acces_token');
		axios.defaults.headers.common['Authorization'] = acces_token;
		return acces_token;
	} catch (error) {
	}
}