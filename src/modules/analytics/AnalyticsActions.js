import {
    PAGE_LOADING,
    PAGE_LOADED,
    ANALYTICS_POPULATE,
    SELECT_LOCATIONS, SELECT_DATES
} from '../../constants/types';
import axios from 'axios';



export const loadInitialAnalyticsState = () => {
    return (dispatch) => {
        dispatch({
            type: PAGE_LOADING
        })
        axios.get('api/analytics/').then(response => {
            dispatch({
                type: ANALYTICS_POPULATE,
                payload: response.data
            })
        })
    }
}

const requestAnalyticsData = (dispatch, getState) => {
    const { filters } = getState().analytics;
    axios.get('api/analytics/', {params: {locations: filters.selectedLocations, date: filters.selectedDate[0]} }).then(response => {
        dispatch({
            type: ANALYTICS_POPULATE,
            payload: response.data
        })
    })
}

export const selectLocations = (selectedLocations) => {
    return (dispatch, getState) => {
        dispatch({
            type: SELECT_LOCATIONS,
            payload: selectedLocations
        })
        requestAnalyticsData(dispatch, getState);
    }
}


export const selectDate = (selectedDate) => {
    return (dispatch, getState) => {
        dispatch({
            type: SELECT_DATES,
            payload: selectedDate
        })
        requestAnalyticsData(dispatch, getState);
    }
}


