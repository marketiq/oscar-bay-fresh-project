import {
    ANALYTICS_POPULATE, SELECT_LOCATIONS, SELECT_DATES
} from '../../constants/types';

export const INITIAL_STATE = {
    grns_chart: {
        categories: [],
        series: []
    },
    invoice_chart: {
        categories: [],
        series: []
    },
    discounts: {},
    void_number: {},
    top_basket_category: {},
    goods_receiving: {
        data_one: {},
        data_two: {}
    },
    gross_margin: {
        data_one: {},
        data_two: {}
    },
    sales_by_day: {
        categories: [],
        series: []
    },
    sales_by_hour_chart: {
        categories: [],
        series: []
    },
    sales_by_category_chart: {
        series: [],
        title: {
            text: "No Sales"
        }
    },
    voids_chart: { categories: [], series: [] },
    refund_chart: { categories: [], series: [] },
    sales_by_store: {
        categories: [],
        series: []
    },
    gross_sales: {},
    net_sales: {},
    total_orders: {},
    avg_order_size: {},
    inventory: {
        data: {
            headers: [],
            values: []
        }
    },
    overdue_invoices: {
        data_one: {},
        data_two: {}
    },
    payment_method: {
        data: {
            list: [],
        }
    },
    avg_order_value: {},
    sales_by_category: {
        data: {
            headers: [],
            topHeaders: [],
            values: [],
        }
    },
    top_items_by_margin: {
        data: {
            headers: [],
            topHeaders: [],
            values: [],
        }
    },
    top_items_by_sales: {
        data: {
            headers: [],
            topHeaders: [],
            values: [],
        }
    },
    lowest_items_by_sales: {
        data: {
            headers: [],
            topHeaders: [],
            values: [],
        }
    },
    total_cogs: {},
    void: {},
    refunds: {},
    new_taxes: {},
    total_collected: {},
    filters: {
        selectedLocations: [],
        selectedDate: ['today']
    },
    all_locations_insights: []
}

export const Reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ANALYTICS_POPULATE:
            return { ...state, ...action.payload }
        case SELECT_LOCATIONS:
            return { ...state, filters: { ...state.filters, selectedLocations: action.payload } }
        case SELECT_DATES:
            return { ...state, filters: { ...state.filters, selectedDate: action.payload } }
        default:
            return state;
    }
}

