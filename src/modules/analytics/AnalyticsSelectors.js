import { createSelector } from 'reselect'

const getAnalytics = (state) =>  state.analytics;
const getLocations = (state) => state.config.locations;
const getDates = (state) => state.config.dates;

export const getSelectorAnalytics = createSelector(
    [ getAnalytics ],
    (analytics) => analytics
)

export const getSelectorFilters = createSelector(
    [ getAnalytics, getLocations, getDates ],
    (analytics, locations, dates) => {
        return {...analytics.filters, locations, dates}
    }
)
