import {
  AUTH_ON_CHANGE,
  AUTH_ON_LOGIN,
  AUTH_ON_LOGIN_ERROR,
  AUTH_ON_LOGIN_SUCCESS,
  AUTH_ON_LOGOUT,
  PAGE_LOADED
} from "../../constants/types";
import {
  BASE_URL,
  DB_URL,
  testBaseUrl,
  testDBUrl,
  multiGet
} from "../../constants/constants";
import { NavigationActions } from "react-navigation";
import { validateEmail } from "../../helpers/validations";
import { deleteToken, saveToken } from "../../helpers/token";
import axios from "axios";
import { onLoadCategories, productsOnLoad } from "../../actions";

export const authOnChange = (key, value) => {
  return {
    type: AUTH_ON_CHANGE,
    payload: { key, value }
  };
};

export const authOnLogin = ({ email, password, url, db }) => {
  return dispatch => {
    // validation
    if (authOnLoginValidation({ email, password, url, db }, dispatch)) {
      dispatch({
        type: AUTH_ON_LOGIN
      });
      deleteToken().then(() => {
        multiGet(values => {
          if (values[0][1] && values[1][1]) {
            return axios
              .post(`${values[0][1]}api/login/`, {
                email: email,
                password: password,
                url: testDBUrl(url),
                db: values[1][1]
              })
              .then(
                response => {
                  saveToken(response.data.token).then(() => {
                    // dispatch(onLoadCategories());
                    // dispatch(productsOnLoad())
                    //   .then(res => {
                        dispatch({
                          type: AUTH_ON_LOGIN_SUCCESS
                        });
                    //   })
                    dispatch(
                      NavigationActions.navigate({ routeName: "Splash" })
                    );
                  });
                },
                error => {
                  return dispatch({
                    type: AUTH_ON_LOGIN_ERROR,
                    payload: { errors: ["Login Error. Please Try Again!"] }
                  });
                }
              );
          } else {
            dispatch(NavigationActions.navigate({ routeName: "Login" }));
          }
        });
      });
    } else {
      return false;
    }
  };
};

export const authOnLogout = () => {
  return dispatch => {
    deleteToken().then(() => {
      dispatch({
        type: AUTH_ON_LOGOUT
      });
      dispatch(NavigationActions.navigate({ routeName: "Login" }));
    });
  };
};

//Helpers
const authOnLoginValidation = ({ email, password, url, db }, dispatch) => {
  let message = [];
  if (!email) {
    message.push("Email is empty");
  }
  if (!password) {
    message.push("Password is empty");
  }
  if (!url) {
    message.push("URL is empty");
  }
  if (!db) {
    message.push("DB is empty");
  }
  if (email && !validateEmail(email)) {
    message.push("Email is not valid");
  }

  return message.length == 0
    ? true
    : authOnLoginValidationDispatch(message, dispatch);
};

const authOnLoginValidationDispatch = (message, dispatch) => {
  dispatch({
    type: AUTH_ON_LOGIN_ERROR,
    payload: { errors: message }
  });
  return false;
};
