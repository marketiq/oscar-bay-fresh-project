import { 
    AUTH_ON_CHANGE, AUTH_ON_LOGIN, AUTH_ON_LOGIN_ERROR, AUTH_ON_LOGIN_SUCCESS, AUTH_ON_LOGOUT
} from '../../constants/types';

export const INITIAL_STATE = {
    email: '', 
    password: '',
    url:'',
    db:'',
    errors: [],
    loading: false
}

export const Reducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case AUTH_ON_CHANGE:
            return {...state, [action.payload.key]: action.payload.value};
        case AUTH_ON_LOGIN:
            return {...state, errors: INITIAL_STATE.errors}; //loading: true,
        case AUTH_ON_LOGIN_SUCCESS:
            return {...state}; //loading: false
        case AUTH_ON_LOGIN_ERROR:
            return {...state, errors: action.payload.errors, password: ''}; //loading: false, 
        case AUTH_ON_LOGOUT:
            return INITIAL_STATE;
        default: 
            return state;
    }
}