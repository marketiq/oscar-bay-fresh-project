import 'react-native';
import { AuthActions } from '../../auth';
import {
    AUTH_ON_CHANGE, AUTH_ON_LOGIN, AUTH_ON_LOGIN_ERROR, AUTH_ON_LOGIN_SUCCESS, AUTH_ON_LOGOUT
} from '../../../constants/types'
import { 
	password_changed, email_changed, valid_login_credentials, empty_email_login_credentials 
} from '../../../../config/jest/mockData';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
// import MockAsyncStorage from 'mock-async-storage';
// import { AsyncStorage as storage } from 'react-native';
import MockStorage from '../../../../config/jest/mockStorage';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore();

const storageCache = {};
const AsyncStorage = new MockStorage(storageCache);
jest.setMock('AsyncStorage', AsyncStorage);


describe('Auth actions', () => {

	beforeEach(() => {
		store.clearActions();
	})

	afterEach(() => {
		// mockAxios.restore();
		// mockAxios.reset();
	})


	it('should return on change password action on auth', () => {
	    expect(AuthActions.authOnChange(password_changed.key, password_changed.value)).toEqual({
	        type: AUTH_ON_CHANGE,
	        payload: password_changed
	    })
	    expect(AuthActions.authOnChange(password_changed.key, password_changed.value)).toMatchSnapshot();
	})


	it('should return on login action on auth', () => {

		const mockAxios = new MockAdapter(axios);

		mockAxios.onPost().reply(200, {
		  token: '123456'
		});

	    return store.dispatch(AuthActions.authOnLogin(valid_login_credentials)).then(() => {
	    	AsyncStorage.setItem('123456', 'access_token')
		    // return AsyncStorage.setItem('123456', 'access_token').then(item => {	
			    let expectedActions = store.getActions();
			    expect(expectedActions.length).toEqual(2);
			    expect(expectedActions).toContainEqual({
			        type: AUTH_ON_LOGIN
			    });
			    expect(expectedActions).toContainEqual({
			        type: AUTH_ON_LOGIN_SUCCESS
			    });	
		    // })

	    })

	})


	it('should return on login error action on auth', () => {
	    store.dispatch(AuthActions.authOnLogin(empty_email_login_credentials))
	    let expectedActions = store.getActions();
	    expect(expectedActions.length).toEqual(1);
	    expect(expectedActions).toContainEqual({
	        type: AUTH_ON_LOGIN_ERROR,
	        payload: {errors: ['Email is empty.']}
	    });	
	})

})