import 'react-native';
import { AuthReducer } from '../../auth';
import {
    AUTH_ON_CHANGE
} from '../../../constants/types'
import { password_changed, email_changed } from '../../../../config/jest/mockData'



describe('Auth reducers', () => {

	const { Reducer, INITIAL_STATE } = AuthReducer;

	it('should change the password on auth', () => {
	    const expectedState = {...INITIAL_STATE, password: password_changed.value};
	    expect(Reducer(INITIAL_STATE, {type: AUTH_ON_CHANGE, payload: password_changed})).toEqual(expectedState);
	    expect(Reducer(INITIAL_STATE, {type: AUTH_ON_CHANGE, payload: password_changed})).toMatchSnapshot();
	})


	it('should change the email on auth', () => {
	    const expectedState = {...INITIAL_STATE, email: email_changed.value};
	    expect(Reducer(INITIAL_STATE, {type: AUTH_ON_CHANGE, payload: email_changed})).toEqual(expectedState);
	    expect(Reducer(INITIAL_STATE, {type: AUTH_ON_CHANGE, payload: email_changed})).toMatchSnapshot();
	})

});
