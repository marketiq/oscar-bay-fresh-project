import { 
    PAGE_LOADING, PAGE_LOADED
} from '../../constants/types';
import axios from 'axios';

  

export const loadInitialConfigState = () => {
    return (dispatch) => {
		// axios.interceptors.request.use(function (config) {
		// 	if(!getState().config.loading) {
		// 		dispatch({
		// 			type: PAGE_LOADING
		// 		})
		// 	}
		// 	return config;
		// }, function (error) {
		// 	dispatch({
		// 		type: PAGE_LOADED
		// 	})
		// 	return Promise.reject(error);
		// });

		// // Add a response interceptor
		// axios.interceptors.response.use(function (response) {
		// 	dispatch({
		// 		type: PAGE_LOADED
		// 	})
		// 	return response;
		// }, function (error) {
		// 	dispatch({
		// 		type: PAGE_LOADED
		// 	})
		// 	return Promise.reject(error);
		// });
    }
} 

export const seeMoreLoading = () => {
    return (dispatch) => {
		dispatch({
			type: PAGE_LOADING
		})
	}
}
export const seeMoreLoaded = () => {
    return (dispatch) => {
		dispatch({
			type: PAGE_LOADED
		})
	}
}
