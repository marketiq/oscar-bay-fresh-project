import {
  PAGE_LOADING,
  PAGE_LOADED,
  AUTH_ON_LOGIN,
  AUTH_ON_LOGIN_SUCCESS,
  AUTH_ON_LOGIN_ERROR,
  ANALYTICS_POPULATE,
  SELECT_LOCATIONS,
  SELECT_DATES,
  PROFILE_POPULATED
} from "../../constants/types";

export const INITIAL_STATE = {
  loading: false,
  locations: [],
  dates: []
};

export const Reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PAGE_LOADING:
      return { ...state, loading: true };
    case AUTH_ON_LOGIN:
    case SELECT_LOCATIONS:
    case SELECT_DATES:
      return { ...state, loading: true };
    case PAGE_LOADED:
      return { ...state, loading: false };
    case AUTH_ON_LOGIN_SUCCESS:
    case AUTH_ON_LOGIN_ERROR:
    case ANALYTICS_POPULATE:
      return { ...state, loading: false };
    case PROFILE_POPULATED:
      // return {...state, loading: false, locations: action.payload.filters.locations, dates: action.payload.filters.dates};
      return { ...state, loading: true };

    default:
      return state;
  }
};
