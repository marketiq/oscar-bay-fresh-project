import {
  PROFILE_CHANGED,
  PROFILE_POPULATED,
  PAGE_LOADING,
  PAGE_LOADED
} from "../../constants/types";
import { NavigationActions } from "react-navigation";
import { getToken } from "../../helpers/token";
import axios from "axios";
import {getBaseUrl } from "../../constants/constants";

export const loadInitialProfileState = () => {
   return dispatch => {
    dispatch({
      type: PAGE_LOADING
    });
    getToken().then(token => {
      if (!token) {
        dispatch({
          type: PAGE_LOADED
        });
        dispatch(NavigationActions.navigate({ routeName: "Login" }));
      } else {
        getBaseUrl((BASE_URL)=>{
          axios.get(`${BASE_URL}api/me/`).then(
            response => {
              dispatch({
                type: PROFILE_POPULATED,
                payload: response.data
              });
              dispatch({
                type: PAGE_LOADED
              });
              dispatch(NavigationActions.navigate({ routeName: "Main" }));
            },
            error => {
              dispatch({
                type: PAGE_LOADED
              });
              dispatch(NavigationActions.navigate({ routeName: "Login" }));
            }
          );
        })
      }
    });
  };
};

export const onProfileChange = (key, value) => {
  return {
    type: PROFILE_CHANGED,
    payload: { key, value }
  };
};
