import { 
	PROFILE_CHANGED, PROFILE_POPULATED
} from '../../constants/types'


export const INITIAL_STATE = { 
	email: '',
	first_name: '',
	last_name: ''
};


export const Reducer = (state = INITIAL_STATE, action) => {
	switch(action.type) {
		case PROFILE_CHANGED:
			return {...state, [action.payload.key]: action.payload.value}
        case PROFILE_POPULATED:
            return {...state, ...action.payload};
		default: 
			return state;
	}
}