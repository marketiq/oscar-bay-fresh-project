import { createSelector } from 'reselect'


const getProfile = (state) =>  state.profile

export const getSelectorGeneralProfile = createSelector(
	[ getProfile ],
	(profile) => {
		const { email, password } = profile
		return { email, password } 
	}
)