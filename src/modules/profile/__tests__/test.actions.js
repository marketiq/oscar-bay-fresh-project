import 'react-native';
import { ProfileActions } from '../../../modules/profile';
import {
    PROFILE_CHANGED
} from '../../../constants/types'
import { profile_changed } from '../../../../config/jest/mockData'


describe('Profile actions', () => {

	it('creates a PROFILE_CHANGED action', () => {
	  expect(ProfileActions.onProfileChange(profile_changed.key, profile_changed.value)).toEqual(
	    {
	      type: PROFILE_CHANGED,
	      payload: profile_changed
	    }
	  );
	  expect(ProfileActions.onProfileChange(profile_changed.key, profile_changed.value)).toMatchSnapshot();
	});

});