import 'react-native';
import { ProfileReducer } from '../../../modules/profile';
import {
    PROFILE_CHANGED
} from '../../../constants/types';
import { profile_changed } from '../../../../config/jest/mockData';


describe('Profile reducers', () => {

	it('Test PROFILE_CHANGED reducer', () => {
	    expect(ProfileReducer(ProfileReducer.INITIAL_STATE, {type: PROFILE_CHANGED, payload: profile_changed})).toEqual({
	        email: profile_changed.value,
	        password: ''
	    });

	    expect(ProfileReducer(ProfileReducer.INITIAL_STATE, {type: PROFILE_CHANGED, payload: profile_changed})).toMatchSnapshot();
	});

});