import React, { Component } from "react";
import { connect } from "react-redux";
import {
  StackNavigator,
  TabNavigator,
  DrawerNavigator
} from "react-navigation";
import { SearchBar } from "react-native-elements";
import OverviewPage from "../components/analytics/OverViewPage/";
import SettingsPage from "../components/SettingsPage";
import PaymentWindow from "../components/PaymentWindow";
import OrderHistory from "../components/OrderHistory";
import PhoneNumber from "../components/PhoneNumber";
import { View, Image } from "react-native";
import { Item, Input, Button, Text, Icon } from "native-base";
import Header from "../components/common/Header";
import Hidden from "./Hidden";
import {
  ACTIVE_TAB_COLOR,
  INACTIVE_TAB_COLOR,
  ACTIVE_TAB_TEXT_COLOR,
  HEADER_COLOR,
  WHITE_COLOR
} from "../constants/constants";

export const DrawerStack = DrawerNavigator(
  {
    Dashboard: {
      screen: OverviewPage,
      navigationOptions: {
        drawerLabel: "POS Screen"
      }
    },
    PaymentWindow: {
      screen: PaymentWindow,
      navigationOptions: {
        drawerLabel: <Hidden />
      }
    },
    OrderHistory: {
      screen: OrderHistory,
      navigationOptions: {
        drawerLabel: "Order History"
      }
    },
    PhoneNumber: {
      screen: PhoneNumber,
      navigationOptions: {
        drawerLabel: <Hidden />
      }
    },
    Logout: {
      screen: SettingsPage,
      navigationOptions: {
        drawerLabel: "Log out"
      }
    }
  },
  {
    drawerWidth: 300,
    gesturesEnabled: false,
    initialRouteName: "Dashboard",
    contentOptions: {
      activeTintColor: ACTIVE_TAB_COLOR,
      items: ["Dashboard", "Logout"]
    }
  }
);

export const DrawerNavigation = StackNavigator(
  {
    Home: { screen: DrawerStack }
  },
  {
    headerMode: "float",
    initialRouteName: "Home",
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle: { color: WHITE_COLOR },
      headerStyle: { backgroundColor: HEADER_COLOR },
      gesturesEnabled: false,
      header: <Header navigation={navigation} />
    })
  }
);


const styles = {
  searchMain: {
    flexDirection: "row",
    width: 200,
    backgroundColor: "transparent",
    borderRadius: 15,
    marginRight: 10,
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: "#ffffff"
  },
  srchInput: {
    color: "#ffffff",
    borderBottomWidth: 0,
    fontSize: 14
  },
  searchiconStyle: {
    color: "#ffffff",
    fontSize: 18
  },
  iconButtonmain: {
    flexDirection: "row",
    borderBottomWidth: 0
  },
  logoMain: {
    width: 70,
    marginLeft: 15
  },
  addIcon: {
    color: "#ffffff",
    fontSize: 20,
    fontWeight: 600,
    backgroundColor: "#ffffff30",
    paddingLeft: 9,
    paddingRight: 9,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 50,
    height: 30,
    width: 30
  },
  basketIcon: {
    color: "#ffffff",
    fontSize: 22,
    fontWeight: 600
  },
  waiterText: {
    color: "#ffffff",
    marginLeft: 20,
    marginRight: 5,
    marginTop: -5
  },
  meuIcon: {
    paddingLeft: 8,
    paddingRight: 15,
    color: "#ffffff"
  }
};
