import React, { Component } from "react";
import { connect } from "react-redux";
import {AsyncStorage} from 'react-native';
import { StackNavigator, addNavigationHelpers } from "react-navigation";
import Splash from "../components/Splash";
import {LoadingModal} from '../components/common'
import {onLoadCategories,productsOnLoad} from '../actions'
import {ConfigActions} from '../modules/config'
import LoginPage from "../components/LoginPage";
import { DrawerNavigation } from "./MainRoute";
import { Animated, Easing, View, StatusBar } from "react-native";

const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0
  }
});

export const RootRouteParams = StackNavigator(
  {
    Splash: {
      screen: Splash
    },
    Login: {
      screen: LoginPage
    },
    Main: {
      screen: DrawerNavigation
    }
  },
  {
    initialRouteName: "Splash",
    headerMode: "none",
    transitionConfig: noTransitionConfig
  }
);

class RootRoute extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    // this.props.dispatch(ConfigActions.seeMoreLoading())
    // this.getToken().then(tok =>{
    //   if(tok){
    //     this.props.dispatch(onLoadCategories())
    //     this.props.dispatch(productsOnLoad()).then(res =>{
    //       this.props.dispatch(ConfigActions.seeMoreLoaded())
    //     })
    //   }
    //   else {
    //     this.setState({loader:false})
    //   }
    // })
  }
  async getToken(){
    try {
      let token = await AsyncStorage.getItem('acces_token')
      return token
    } catch (error) {
    }
  }

  render() {
    return (
      <View style={{height:'100%',width:'100%'}}>
        <StatusBar backgroundColor="#9d4cd1" barStyle="light-content" />
        <RootRouteParams
          navigation={addNavigationHelpers({
            dispatch: this.props.dispatch,
            state: this.props.nav
          })}
        />
          {/* <LoadingModal/> */}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    nav: state.nav
  };
};

export const RootRouteContainer = connect(mapStateToProps)(RootRoute);
