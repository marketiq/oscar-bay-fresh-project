import { initialState } from './initialState';
import { BILL_AMOUNT, CART } from '../actions/actionTypes';
import {AUTH_ON_LOGOUT} from '../constants/types'
 

const billAmountReducers = (state =initialState.payment,action) => {
    switch(action.type){
        case BILL_AMOUNT.ADD_PAYMENT_FIELD :
            return action.amount ;
        case AUTH_ON_LOGOUT:
            return 0;
        case CART.ON_EMPTY_CART:
            return 0;
        default:
            return state;
    }
} 

export default billAmountReducers;