import { CART } from '../actions/actionTypes';
import initialState from './initialState';
import {AUTH_ON_LOGOUT} from '../constants/types'

const cartReducer = (state = initialState.cart, action) => {
    let cartProduct = state;
    let productIndex = 0;
    switch (action.type) {
        case CART.ON_ADD_PRODUCT:
            if (action.product.qty === 0) {
                action.product.qty++
            }
            return [
                ...state,
                action.product
            ];
        case CART.ON_INC_COUNT:
            productIndex = cartProduct.map(item => {
                return item.product_id;
            }).indexOf(action.product.product_id);
            cartProduct[productIndex].qty++;
            return [
                ...cartProduct
            ];
        case CART.ON_REMOVE_PRODUCT:
            productIndex = cartProduct.map(item => {
                return item.product_id;
            }).indexOf(action.id);
            cartProduct.splice(productIndex, 1);
            return [
                ...cartProduct
            ];
        case CART.ON_DEC_COUNT:
            productIndex = cartProduct.map(item => {
                return item.product_id;
            }).indexOf(action.id);
            cartProduct[productIndex].qty--;
            return [
                ...cartProduct
            ];
        case CART.ON_EMPTY_CART:
            return [];
        case AUTH_ON_LOGOUT:
            return [];
        default:
            return state;
    }
}

export default cartReducer;