
import { CUSTOMERS } from '../actions/actionTypes';
import initialState from './initialState';

const customerReducer = (state = initialState.customers, action) => {
    let customer = state;
    switch (action.type) {
        case CUSTOMERS.CREATE_CUSTOMER:
            return [
                ...state,
                action.data,
            ]
        case CUSTOMERS.SHOW_CUSTOMERS:
            return action.payload
        case CUSTOMERS.UPDATE_CUSTOMER:
            let customerIndex = customer.map(item => {
                return item.id
            }).indexOf(action.data.id);
            customer[customerIndex] = action.data
            return customer;
        case CUSTOMERS.FILTER_CUSTOMERS:
            let inputText = action.text;
            const tempCustomers = initialState.customers.filter((customer) => {
                return (customer.firstName.toLowerCase().indexOf(inputText.toLowerCase()) > -1) || (customer.phone.indexOf(inputText) > -1);
            }
            )
            return tempCustomers;
        default:
            return state;
    }
}

export default customerReducer;