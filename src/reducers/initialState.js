export const initialState = {
    cart: [],
    allRecords: false,
    payment: 0,
    prodCategories:[],
    // products: [
    //     {
    //         "id": 1,
    //         "title": "Blue Band",
    //         "description": "Centralized 24/7 help-desk",
    //         "image": "http://dalefarm.co.uk/wp-content/uploads/2016/10/Pure-Butter-500g-new-packaging.jpg",
    //         "price": 200,
    //         "category": 'dairy',
    //         "count": 1,
    //     },
    //     {
    //         "id": 2,
    //         "title": "Zeera Plus",
    //         "description": "Business-focused global functionalities",
    //         "image": "https://www.continentalbiscuits.com.pk/products/zeera_f.jpg",
    //         "price": 45,
    //         "category": "dairy",
    //         "count": 1,
    //     },
    //     {
    //         "id": 3,
    //         "title": "Lays French Cheese",
    //         "description": "Proactive static open architecture",
    //         "image": "http://dalefarm.co.uk/wp-content/uploads/2016/10/Pure-Butter-500g-new-packaging.jpg",
    //         "category": "confectionary",
    //         "price": 20,
    //         "count": 1,
    //     },
    //     {
    //         "id": 4,
    //         "title": "Nestle Mango Juice",
    //         "description": "Robust dynamic instruction set",
    //         "image": "http://dalefarm.co.uk/wp-content/uploads/2016/10/Pure-Butter-500g-new-packaging.jpg",
    //         "price": 150,
    //         "category": "confectionary",
    //         "count": 1,
    //     },
    //     {
    //         "id": 5,
    //         "title": "KitKat Chocolate Bar",
    //         "description": "Horizontal grid-enabled database",
    //         "image": "http://dalefarm.co.uk/wp-content/uploads/2016/10/Pure-Butter-500g-new-packaging.jpg",
    //         "category": "mobile-cards",
    //         "price": 260,
    //         "count": 1,

    //     },
    //     {
    //         "id": 6,
    //         "title": "Doritos Nacho Cheese",
    //         "description": "Horizontal grid-enabled database",
    //         "image": "http://dalefarm.co.uk/wp-content/uploads/2016/10/Pure-Butter-500g-new-packaging.jpg",
    //         "category": "mobile-cards",
    //         "count": 1,
    //         "price": 240,
    //     },

    //     {
    //         "id": 7,
    //         "title": "Lays Masala Chips",
    //         "description": "Horizontal grid-enabled database",
    //         "image": "http://dalefarm.co.uk/wp-content/uploads/2016/10/Pure-Butter-500g-new-packaging.jpg",
    //         "category": "dairy",
    //         "count": 1,
    //         "price": 50,
    //     },
    //     {
    //         "id": 8,
    //         "title": "Coke Can",
    //         "description": "Horizontal grid-enabled database",
    //         "image": "http://dalefarm.co.uk/wp-content/uploads/2016/10/Pure-Butter-500g-new-packaging.jpg",
    //         "category": "confectionary",
    //         "count": 1,
    //         "price": 150,
    //     },
    // ],
    products: [],
    // categories: [
    //     {
    //         id: 1,
    //         value: 'Dairy Products',
    //         type: 'dairy'
    //     }, {
    //         id: 2,
    //         value: 'Confectionary',
    //         type: 'confectionary'
    //     }, {
    //         id: 3,
    //         value: 'Mobile cards',
    //         type: 'mobile-cards'
    //     }
    // ],
    customers: [],
    // customers: [
    //     {
    //         id:1,
    //         firstName:'Arsalan',
    //         lastName:'',
    //         phone: '42424',
    //         car: 'ABC-1234',
    //     },{
    //         id:2,
    //         firstName:'Muntazir',
    //         lastName:'',
    //         car: 'ABC-5678',
    //         phone: '66645645'
    //     },{
    //         id:3,
    //         firstName:'Ibrar',
    //         lastName:'',
    //         car: 'DEF-1234',
    //         phone: '77745664'
    //     },{
    //         id:4,
    //         firstName:'Waleed',
    //         lastName:'',
    //         car: 'DEF-5678',
    //         phone: '66456546464'
    //     },{
    //         id:5,
    //         firstName:'Zubair',
    //         lastName:'',
    //         car: 'GHI-1234',
    //         phone: '534553543'
    //     }
    // ],
    orders: []
    //     {
    //         id: 1,
    //         customerName: 'Shehzad',
    //         vehicleNum: 'ABC-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 2,
    //         customerName: 'Waleed',
    //         vehicleNum: 'XYZ-123',
    //         orderStatus: 'PENDING'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    //     {
    //         id: 3,
    //         customerName: 'Abdul Qadir',
    //         vehicleNum: 'BAD-123',
    //         orderStatus: 'DELIVERED'
    //     },
    // ]
}

export default initialState;