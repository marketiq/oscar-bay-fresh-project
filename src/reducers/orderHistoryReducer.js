import { ORDERS } from '../actions/actionTypes';
import initialState from './initialState';

const orderHistoryReducer = (state = initialState.allRecords, action) => {
    switch (action.type) {
        case ORDERS.ALL_ORDERS:
            return action.payload
        default:
            return state;
    }
}
export default orderHistoryReducer;