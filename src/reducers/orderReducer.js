import { ORDERS } from '../actions/actionTypes';
import initialState from './initialState';

const orderReducer = (state = initialState.orders, action) => {
    switch (action.type) {
        case ORDERS.GET_ALL_ORDERS:
            return [...action.data]
        default:
            return state;
    }
}
export default orderReducer;