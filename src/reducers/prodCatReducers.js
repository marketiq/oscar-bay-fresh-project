import { initialState } from "./initialState";
import { CATEGORIES } from "../actions/actionTypes";

const prodCatReducers = (state=initialState.prodCategories,action) =>{
    switch (action.type){
        case CATEGORIES.GET_CATEGORIES:
            return [
                ...action.data
            ]
        default:
            return state;
    }
}

export default prodCatReducers;