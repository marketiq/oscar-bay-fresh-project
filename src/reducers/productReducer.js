// import {
//     PRODUCTS_ON_SEARCH_CHANGE, PRODUCTS_POPULATE
// } from '../../constants/types';

import { PRODUCTS } from '../actions/actionTypes';
import initialState from './initialState';
import {AUTH_ON_LOGOUT}  from '../constants/types';

const productReducer = (state = initialState.products, action) => {
    switch (action.type) {
        case PRODUCTS.SHOW_PRODUCTS:
            // return state
            return action.payload

        case PRODUCTS.SHOW_FILTER_PRODUCTS:
            return action.payload
        case AUTH_ON_LOGOUT :
            return [];
        default:
            return state;
    }
}

export default productReducer;