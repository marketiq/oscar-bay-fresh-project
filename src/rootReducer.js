import { combineReducers } from 'redux';
import { ConfigReducer } from './modules/config';
import { NavReducer } from './Router';
import { AuthReducer } from './modules/auth';
import { ProfileReducer } from './modules/profile';
import cart from './reducers/cartReducer';
import product from './reducers/productReducer';
import customer from './reducers/customerReducer';
import order from './reducers/orderReducer';
import billAmount from './reducers/billAmountReducers';
import prodCategories from './reducers/prodCatReducers';

export default combineReducers({
    config: ConfigReducer.Reducer,
    nav: NavReducer,
    auth: AuthReducer.Reducer,
    profile: ProfileReducer.Reducer,
    cart_reducer: cart,
    product_reducer: product,
    billAmount,
    prodCategories,
    customer_reducer: customer,
    order_reducer: order,
})
